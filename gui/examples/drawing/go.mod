module gui/examples/drawing

go 1.15

require (
	github.com/lxn/walk v0.0.0-20200924155701-77185e9c4aec
	github.com/lxn/win v0.0.0-20191128105842-2da648fda5b4 // indirect
	golang.org/x/sys v0.0.0-20201007165808-a893ed343c85 // indirect
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
)
