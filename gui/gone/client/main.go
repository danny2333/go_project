package main

import (
	"gone/client/config"
	"gone/client/process"
	. "gone/client/utils/logger"
)

// rsrc -manifest app.manifest -ico=app.ico -o rsrc.syso
// go build -ldflags="-H windowsgui"

func main() {
	config.LoadConfig("./config/config.json")

	MyLOG.Init("server")
	defer MyLOG.End()

	MyLOG.Log("客户端运行中: %s:%s", config.Config.Client.Host, config.Config.Client.Port)

	process.ClientProc.MainInit()
	process.MyView.HomePage()
}
