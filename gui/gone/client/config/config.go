package config

import (
	"encoding/json"
	"os"
)

type config struct {
	Server serverConfig `json:"server"`
	Client clientConfig `json:"client"`
}

type serverConfig struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

type clientConfig struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

var Config config

func LoadConfig(configFile string) error {
	configData, err := os.ReadFile(configFile)
	if err != nil {
		return err
	}
	return json.Unmarshal(configData, &Config)
}

// 测试用方法
func exportDefaultConfig(configFile string) error {
	defaultConfig := config{}
	configData, err := json.MarshalIndent(&defaultConfig, "", "\t")
	if err != nil {
		return err
	}
	err = os.WriteFile(configFile, configData, 0666)
	if err != nil {
		return err
	}
	return nil
}
