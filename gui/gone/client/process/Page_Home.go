package process

import (
	"gone/client/utils"
	. "gone/client/utils/logger"
	"time"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
)

func (this *View) HomePage() {
	err := MainWindow{
		Visible:  false,
		AssignTo: &this.mwAssign_HomePage,
		Title:    utils.GetSubTitle("HomePage"),
		Icon:     appIcon,
		Layout:   VBox{Alignment: AlignHCenterVCenter}, // 布局
		Children: []Widget{ // 不动态添加控件的话，在此布局或者QT设计器设计UI文件，然后加载。
			LinkLabel{
				MaxSize: Size{Width: 100, Height: 0},
				Text:    `I can contain multiple links like <a id="this" href="https://golang.org">this</a> or <a id="that" href="https://github.com/lxn/walk">that one</a>.`,
				OnLinkActivated: func(link *walk.LinkLabelLink) {
					MyLOG.Log("id: '%s', url: '%s'\n", link.Id(), link.URL())
				},
			},
			Composite{
				Layout: HBox{MarginsZero: true},
				Children: []Widget{
					HSpacer{},
					PushButton{
						Text: "登录",
						OnClicked: func() {
							this.mwAssign_HomePage.SetVisible(false)
							MyView.LoginPage()
							time.Sleep(100 * time.Millisecond)
							this.mwAssign_HomePage.SetVisible(true)
						},
					},
					PushButton{
						Text: "注册",
						OnClicked: func() {
							this.mwAssign_HomePage.SetVisible(false)
							MyView.RegisterPage()
							time.Sleep(100 * time.Millisecond)
							this.mwAssign_HomePage.SetVisible(true)
						},
					},
					PushButton{
						Text: "关闭",
						OnClicked: func() {
							this.mwAssign_HomePage.Close()
						},
					},
				},
			},
		},
	}.Create()
	if err != nil {
		return
	}
	StyleMenuRun(this.mwAssign_HomePage, 420, 120)
}
