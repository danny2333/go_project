package process

import (
	"errors"
	"fmt"
	"gone/client/utils"
	"gone/client/utils/errs"
	. "gone/client/utils/logger"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
)

func (this *View) RegisterPage() {
	MyLOG.Log("RegisterPage ...")
	err := MainWindow{
		Visible:  false,
		AssignTo: &this.mwAssign_RegisterPage,
		Name:     "HomePage",
		Title:    utils.GetSubTitle("RegisterPage"),
		Icon:     appIcon,
		Font: Font{
			Family:    "Microsoft YaHei",
			PointSize: 9,
		},
		Layout: VBox{Alignment: AlignHCenterVCenter}, //布局
		Children: []Widget{ //不动态添加控件的话，在此布局或者QT设计器设计UI文件，然后加载。
			Composite{
				Layout: VBox{},
				Children: []Widget{
					Label{
						Text: "账号:",
					},
					LineEdit{
						AssignTo: &this.RegisterUsername,
					},
					Label{
						Text: "昵称:",
					},
					LineEdit{
						AssignTo: &this.RegisterNickname,
					},
					Label{
						Text: "密码:",
					},
					LineEdit{
						AssignTo:     &this.RegisterPassword,
						PasswordMode: true,
					},
					Label{
						Text: "确认密码:",
					},
					LineEdit{
						AssignTo:     &this.RegisterConPassword,
						PasswordMode: true,
					},
				},
			},
			Composite{
				Layout: HBox{MarginsZero: true},
				Children: []Widget{
					HSpacer{},
					PushButton{
						Text: "注册",
						OnClicked: func() {
							this.registerHandle()
						},
					},
					PushButton{
						Text: "取消",
						OnClicked: func() {
							this.mwAssign_RegisterPage.Close()
						},
					},
				},
			},
		},
	}.Create()
	if err != nil {
		return
	}
	StyleMenuRun(this.mwAssign_RegisterPage, 420, 320)
}

func (this *View) registerHandle() {
	regCheckErr := checkRegisterEmpty(this.RegisterUsername.Text(), this.RegisterNickname.Text(), this.RegisterPassword.Text(), this.RegisterConPassword.Text())
	if regCheckErr != nil {
		errMsgBox(this.mwAssign_RegisterPage, regCheckErr.Error())
		return
	}

	registerErr := ClientProc.Register(this.RegisterUsername.Text(), this.RegisterNickname.Text(), this.RegisterPassword.Text())
	if registerErr != nil {
		walk.MsgBox(this.mwAssign_RegisterPage, UIConfigMsgTitle, registerErr.Error(), walk.MsgBoxIconError)
		return
	}

	msg := fmt.Sprintf("新用户 %s (%s) 注册成功, 请返回登录界面", this.RegisterNickname.Text(), this.RegisterUsername.Text())
	okMsgBox(this.mwAssign_RegisterPage, msg)
}

func checkRegisterEmpty(username, nickname, password, conPassword string) (err error) {
	if username == "" {
		err = errs.CountEmpty
		return
	}
	if nickname == "" {
		err = errors.New("请输入用户名")
		return
	}
	if password == "" {
		err = errs.PwdEmpty
		return
	}
	if conPassword == "" {
		err = errs.PwdEmpty
		return
	}
	if password != conPassword {
		err = errors.New("两次输入密码不相同")
		return
	}
	return
}
