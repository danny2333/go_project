package process

import (
	"gone/client/utils"
	"gone/client/utils/errs"
	. "gone/client/utils/logger"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
)

func (this *View) LoginPage() {
	err := MainWindow{
		Visible:  false,
		AssignTo: &this.mwAssign_LoginPage,
		Title:    utils.GetSubTitle("LoginPage"),
		Icon:     appIcon,
		Layout:   VBox{Alignment: AlignHCenterVCenter},
		Children: []Widget{
			Composite{
				Layout: Grid{Columns: 2},
				// Font:   Font{PointSize: 14},
				Children: []Widget{
					Label{
						Text: "账号:",
					},
					LineEdit{
						AssignTo: &this.LoginUsername,
					},
					Label{
						Text: "密码:",
					},
					LineEdit{
						AssignTo:     &this.LoginPassword,
						PasswordMode: true,
						OnKeyDown: func(key walk.Key) { // 键盘事件
							if key == walk.KeyReturn { // 回车键
								this.loginHandle()
							}
						},
					},
				},
			},
			Composite{
				Layout: HBox{},
				Children: []Widget{
					HSpacer{},
					PushButton{
						Text: "登录",
						OnClicked: func() {
							this.loginHandle()
						},
					},
					PushButton{
						Text: "取消",
						OnClicked: func() {
							this.mwAssign_LoginPage.Close()
						},
					},
				},
			},
		},
	}.Create()
	if err != nil {
		return
	}
	StyleMenuRun(this.mwAssign_LoginPage, 420, 120)
}

func (this *View) loginHandle() {
	MyLOG.Log("登陆账号: %s", this.LoginUsername.Text())

	loginCheckErr := checkLoginEmpty(this.LoginUsername.Text(), this.LoginPassword.Text())
	if loginCheckErr != nil {
		errMsgBox(this.mwAssign_LoginPage, loginCheckErr.Error())
		return
	}

	loginErr := ClientProc.Login(this.LoginUsername.Text(), this.LoginPassword.Text())
	if loginErr != nil {
		errMsgBox(this.mwAssign_LoginPage, loginErr.Error())
		return
	}
	this.mwAssign_LoginPage.Close()
	this.MainPage()
}

func checkLoginEmpty(count string, pwd string) (err error) {
	if count == "" {
		err = errs.CountEmpty
		return
	}
	if pwd == "" {
		err = errs.PwdEmpty
		return
	}
	return
}
