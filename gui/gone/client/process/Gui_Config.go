package process

import (
	"github.com/lxn/walk"
	"github.com/lxn/win"
)

const (
	UIConfigMsgTitle = "提示"
)

var (
	appIcon, _ = walk.NewIconFromResourceId(2)
	hMenu      win.HMENU
	currStyle  int32
	xScreen    int32
	yScreen    int32
	dpiScale   float64

	WindowMap  = make(map[string]*walk.MainWindow)
	MenuConfig *walk.MainWindow
)

func init() {
	xScreen = win.GetSystemMetrics(win.SM_CXSCREEN)
	yScreen = win.GetSystemMetrics(win.SM_CYSCREEN)
}

func StyleMenuRun(w *walk.MainWindow, SizeW int32, SizeH int32) {
	if dpiScale == 0 {
		dpiScale = float64(win.GetDpiForWindow(w.Handle())) / 96.0
	}
	//WindowMap[w.Name()] = w
	currStyle = win.GetWindowLong(w.Handle(), win.GWL_STYLE)
	//removes default styling
	win.SetWindowLong(w.Handle(), win.GWL_STYLE, currStyle&^win.WS_SIZEBOX&^win.WS_MINIMIZEBOX&^win.WS_MAXIMIZEBOX)
	// win.SetWindowLong(w.Handle(), win.GWL_STYLE, currStyle&^win.WS_SIZEBOX&^win.WS_MAXIMIZEBOX)
	hMenu = win.GetSystemMenu(w.Handle(), false)
	// win.RemoveMenu(hMenu, win.SC_CLOSE, win.MF_BYCOMMAND) // 右上角关闭按钮禁用

	SizeW, SizeH = CalcDpiScaledSize(SizeW, SizeH)
	win.SetWindowPos(w.Handle(), 0, (xScreen-SizeW)/2, (yScreen-SizeH)/2, SizeW, SizeH, win.SWP_FRAMECHANGED)
	//win.ShowWindow(w.Handle(), win.SW_SHOW)
	win.ShowWindow(w.Handle(), win.SW_SHOWNORMAL)
	win.SetFocus(w.Handle())
	w.Run()
	//w.Closing().Attach(func(canceled *bool, reason walk.CloseReason) {
	//	w.Dispose()
	//	WindowMap[w.Name()] = nil
	//	//}
	//})
}

func CalcDpiScaledSize(SizeW int32, SizeH int32) (int32, int32) {
	return int32(float64(SizeW) * dpiScale), int32(float64(SizeH) * dpiScale)
}
