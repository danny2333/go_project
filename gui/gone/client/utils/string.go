package utils

import "fmt"

const (
	AppTitle = "GoChatRoom"
)

var ()

func GetSubTitle(subTitle string) string {
	return fmt.Sprintf("%s - %s", subTitle, AppTitle)
}
