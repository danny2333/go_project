package config

import (
	"encoding/json"
	"os"
)

type config struct {
	Server   serverConfig `json:"server"`
	Database mysqlConfig  `json:"database"`
}

type serverConfig struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

type mysqlConfig struct {
	DB_DRIVER         string `json:"type"`
	DB_USER           string `json:"user"`
	DB_PWD            string `json:"pwd"`
	DB_NAME           string `json:"dbname"`
	DB_HOST           string `json:"host"`
	DB_PORT           string `json:"port"`
	DB_PROTOCOL       string `json:"db_protocol"`
	DB_MAX_OPEN_CONNS int    `json:"db_max_open_conns"`
	DB_MAX_IDLE_CONNS int    `json:"db_max_idle_conns"`
}

var Config config

func LoadConfig(configFile string) error {
	configData, err := os.ReadFile(configFile)
	if err != nil {
		return err
	}
	return json.Unmarshal(configData, &Config)
}

// 测试用方法
func exportDefaultConfig(configFile string) error {
	defaultConfig := config{}
	configData, err := json.MarshalIndent(&defaultConfig, "", "\t")
	if err != nil {
		return err
	}
	err = os.WriteFile(configFile, configData, 0666)
	if err != nil {
		return err
	}
	return nil
}
