package main

import (
	"gone/server/config"
	"gone/server/process"
	"gone/server/utils/logger"
	"net"
)

func main() {
	config.LoadConfig("./config/config.json")
	logger.MyLOG.Init("server")

	listen, err := net.Listen("tcp", "0.0.0.0:"+config.Config.Server.Port)
	if err != nil {
		logger.MyLOG.ErrLog("net listen err: %v", err)
		return
	}

	logger.MyLOG.Log("服务端运行中: %s:%s", config.Config.Server.Host, config.Config.Server.Port)

	defer listen.Close()

	process.Connect_DB()
	process.CliMgr.Init()

	for {
		logger.MyLOG.Log("for run ...")
		conn1, err := listen.Accept()
		if err != nil {
			logger.MyLOG.ErrLog("for run Accept err: %v", err)
		} else {
			logger.MyLOG.Log("for run Accept success! client id=%v", conn1.RemoteAddr().String())
		}

		go process.FindProcess(conn1)
	}
}
