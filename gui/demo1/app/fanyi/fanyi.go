package fanyi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
)

// type目前能用的类型：
// ZH_CN2EN 中文　»　英语
// ZH_CN2JA 中文　»　日语
// ZH_CN2KR 中文　»　韩语AUTO 自动识别i：翻译内容*/
// http://fanyi.youdao.com/translate?&doctype=json&type=AUTO&i=你好

var (
	BaiduApi  = "https://fanyi-api.baidu.com/api/trans/vip/translate"
	Appid     = ""
	AppSecret = ""
	from      = "auto"
	to        = "en"
)

type MyMainWindow struct {
	*walk.MainWindow
	showAboutBoxAction *walk.Action
	inTE               *walk.LineEdit
	outTE              *walk.TextEdit
	Yy                 string
}

func Translate(content, yy string) string {
	url := fmt.Sprintf("http://fanyi.youdao.com/translate?&doctype=json&type=%s&i=%s", yy, content)
	log.Println("Translate: ", url)

	client := &http.Client{}
	request, err := http.NewRequest("GET", url, nil)
	//异常捕捉
	if err != nil {
		return ""
	}

	//处理返回结果
	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}
	//关闭流
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return ""
	}
	d := make(map[string]interface{})
	err = json.Unmarshal(body, &d)
	if err != nil {
		return ""
	}

	// {"type":"ZH_CN2EN","errorCode":0,"elapsedTime":0,"translateResult":[[{"src":"你好","tgt":"hello"}]]}
	// return string(body)

	res := d["translateResult"].([]interface{})[0].([]interface{})[0].(map[string]interface{})["tgt"].(string)
	fmt.Printf("【%s】的翻译是【%s】\n", content, res)
	return res
}

func (mw *MyMainWindow) showAboutTriggered() {
	walk.MsgBox(mw, "About", "Powered by golang+lxn/walk", walk.MsgBoxIconInformation)
}

func (mw *MyMainWindow) showNoneMessage(message string) {
	walk.MsgBox(mw, "提示", message, walk.MsgBoxIconInformation)
}

func Run() {
	mw := new(MyMainWindow)
	mw.Yy = "ZH_CN2EN"
	if err := (MainWindow{
		Icon:     "app.ico",
		AssignTo: &mw.MainWindow,
		Title:    "翻译",
		MenuItems: []MenuItem{
			Menu{
				Text: "Action",
				Items: []MenuItem{
					Action{
						Text:        "退出",
						OnTriggered: func() { mw.Close() },
					},
				},
			},
			Menu{
				Text: "帮助",
				Items: []MenuItem{
					Action{
						AssignTo:    &mw.showAboutBoxAction,
						Text:        "关于",
						OnTriggered: mw.showAboutTriggered,
					},
				},
			},
		},
		MinSize: Size{Width: 700, Height: 700},
		Size:    Size{Width: 700, Height: 700},
		Layout:  VBox{},
		DataBinder: DataBinder{
			DataSource:  mw,
			AutoSubmit:  true,
			OnSubmitted: func() {},
		},
		Children: []Widget{
			Composite{
				Layout: HBox{},
				Children: []Widget{
					GroupBox{
						Layout: HBox{},
						Font:   Font{PointSize: 14},
						Children: []Widget{
							LineEdit{AssignTo: &mw.inTE},
							RadioButtonGroup{
								DataMember: "Yy",
								Buttons: []RadioButton{
									{
										Name:    "zhongying",
										Text:    "中英",
										Value:   "ZH_CN2EN",
										MaxSize: Size{Width: 66},
									},
									{
										Name:    "zhongri",
										Text:    "中日",
										Value:   "ZH_CN2JA",
										MaxSize: Size{Width: 66},
									},
									{
										Name:    "zhonghan",
										Text:    "中韩",
										Value:   "ZH_CN2KR",
										MaxSize: Size{Width: 66},
									},
								},
							},
							PushButton{
								Text: "翻译",
								OnClicked: func() {
									content := mw.inTE.Text()
									if content != "" {
										/* tr, err := Translate(content)
										if err != nil {
											mw.showNoneMessage(err.Error())
										} else {
											tf := tr["trans_result"].([]interface{})
											str := ""
											for _, v := range tf {
												v1 := v.(map[string]interface{})
												str = str + v1["dst"].(string)
											}
											outTE.SetText(str)
										} */

										mw.outTE.SetText(Translate(content, mw.Yy))
									} else {
										mw.showNoneMessage("请输入要翻译的内容")
									}
								},
							},
						},
					},
				},
			},
			Composite{
				Layout: HBox{},
				Children: []Widget{
					GroupBox{
						Title:  "翻译结果",
						Layout: HBox{},
						Children: []Widget{
							TextEdit{AssignTo: &mw.outTE, ReadOnly: true, Font: Font{PointSize: 14}},
						},
					},
				},
			},
		},
	}.Create()); err != nil {
		log.Fatal(err)
	}
	mw.Run()
}
