package productcombinationtext

import "fmt"

type Settings struct {
	Str1         []string
	Str2         []string
	Result       []string
	BuildTypeArr []string
	BuildType    string
	// BuildTypeFunc func(string, []string) bool
	// BuildFunc     map[string]func()
}

func New() *Settings {
	return &Settings{
		Str1: []string{"摩尔", "大苏打", "林福地", "十分广泛"},
		Str2: []string{"很不错", "很好看", "值得推荐", "很优秀"},
		BuildTypeArr: []string{
			"1+t+2",
			"2+t+1",
			"1+2",
			"2+1",
		},
		BuildType: "1+t+2",
		/* BuildFunc: make(map[string]func()),
		BuildTypeFunc: func(t string, data []string) bool {
			for _, v := range data {
				if t == v {
					return true
				}
			}
			return false
		}, */
	}
}

func (s *Settings) Build() {
	for k1 := range s.Str1 {
		// fmt.Printf("%d\n", k)
		for k2 := range s.Str2 {
			switch s.BuildType {
			case s.BuildTypeArr[0]:
				tmp := s.Str1[k1] + " " + s.Str2[k2]
				s.Result = append(s.Result, tmp)
			case s.BuildTypeArr[1]:
				tmp := s.Str2[k2] + " " + s.Str1[k1]
				s.Result = append(s.Result, tmp)
			case s.BuildTypeArr[2]:
				tmp := s.Str1[k1] + s.Str2[k2]
				s.Result = append(s.Result, tmp)
			case s.BuildTypeArr[3]:
				tmp := s.Str2[k2] + s.Str1[k1]
				s.Result = append(s.Result, tmp)
			}
		}
	}
	fmt.Printf("%#v", s.Result)
}
