package example

import (
	"strings"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
)

type Cfg struct {
	Title  string
	Width  int
	Height int
	InTE   *walk.TextEdit
	OutTE  *walk.TextEdit
}

func (c *Cfg) G1() {
	MainWindow{
		Icon:    "app.ico",
		Title:   c.Title,
		MinSize: Size{Width: c.Width, Height: c.Height},
		Layout:  VBox{},
		Children: []Widget{
			HSplitter{
				Children: []Widget{
					TextEdit{AssignTo: &c.InTE},
					TextEdit{AssignTo: &c.OutTE, ReadOnly: true},
				},
			},
			PushButton{
				Text: "SCREAM",
				OnClicked: func() {
					c.OutTE.SetText(strings.ToUpper(c.InTE.Text()))
				},
			},
		},
	}.Run()
}

func Run() {
	cfg := &Cfg{
		Title:  "字母转换大写",
		Width:  600,
		Height: 400,
	}
	cfg.G1()
}
