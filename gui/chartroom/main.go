package main

import (
	"chatroom/client/process"
	"chatroom/config"
	serverProcess "chatroom/server/process"
	"chatroom/utils/tools"
	"flag"
	"fmt"
	"log"
	"net"
)

// rsrc -manifest app.exe.manifest -ico=app.ico -o rsrc.syso
// go build -ldflags="-H windowsgui"

var runType = flag.String("t", "server", "run type\nserver and client")

func main() {
	flag.Parse()
	config.LoadConfig("./config/config.json")

	if *runType == "server" {
		serverMain()
	} else if *runType == "client" {
		clientMain()
	} else {
		serverMain()
	}
	return

	// clientMain()
	// serverMain()
}

// clientMain 客户端
// go build -ldflags="-H windowsgui" -o chatroom.exe
// chatroom.exe -t client
func clientMain() {
	tools.MyLOG.Log("客户端运行中: %s:%s", config.Config.Client.Host, config.Config.Client.Port)
	process.ClientProc.MainInit()
	process.MyView.LoginView()

	tools.MyLOG.End()
}

// serverMain 服务端
// go build -ldflags="-H windowsgui" -o chatroom.exe
// chatroom.exe -t server
// 信息显示应该是8080端口被占用，，因此采取如下步骤：
// 在Windows命令行窗口下执行：
// 1、显示占用8080端口的进程：
// netstat -aon|findstr "8080"

// 本例占用8080端口的是进程号为4668的程序。

// 2、找到进程号为4668的程序
// tasklist|findstr "4668"

// 3、杀死程序
// taskkill -F -PID 4668

func serverMain() {
	log.Println("aaa")
	tools.MyLOG.Init(tools.LogServer)

	listen, err := net.Listen("tcp", "0.0.0.0:"+config.Config.Server.Port)
	if err != nil {
		fmt.Printf("serverMain: net listen err: %s", err)
		return
	}
	tools.MyLOG.Log("服务端运行中: %s:%s", config.Config.Server.Host, config.Config.Server.Port)

	defer listen.Close()

	serverProcess.Connect_DB()
	serverProcess.CliMgr.Init()

	for {
		fmt.Println("waitting client1")
		conn1, err := listen.Accept()
		if err != nil {
			fmt.Println("Accept() err=", err)
		} else {
			fmt.Printf("Accept() succ, client ip=%v\n", conn1.RemoteAddr().String())
		}

		go serverProcess.FindProcess(conn1)

	}
}
