package tools

import (
	"bufio"
	"log"
	"os"
	"strings"

	"github.com/lxn/walk"
)

func IsValueInSlice(testint int, testslice []int) bool {
	for _, v := range testslice {
		if v == testint {
			return true
		}
	}
	return false
}

func checkFileIsExist(filename string) bool {
	var exist = true
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		exist = false
	}
	return exist
}

func SetBlue() walk.Color {
	return walk.RGB(0x00, 0x99, 0xff)
}

func SetGrey() walk.Color {
	return walk.RGB(0xcc, 0xcc, 0xcc)
}

func SetGreen() walk.Color {
	return walk.RGB(0x00, 0xcc, 0x66)
}

func LoadConfig(filePath string) {
	// filePath := ".env"

	f, err := os.Open(filePath)
	if err != nil {
		log.Fatalln("Failed to open log file", err)
	}
	defer f.Close()

	lines := make([]string, 0, 100)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatalln("读取每行配置错误", err)
	}

	for _, l := range lines {
		if !strings.HasPrefix(l, "#") && l != "" {
			pair := strings.Split(l, "=")
			os.Setenv(pair[0], pair[1])
		}
	}

}
