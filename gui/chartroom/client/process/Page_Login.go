package process

import (
	"chatroom/utils/errs"
	"chatroom/utils/tools"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
)

func (this *View) LoginView() {
	//var inCount, inPwd *walk.LineEdit
	children := []Widget{
		Composite{
			Layout: Grid{Columns: 2},
			Children: []Widget{
				Label{
					Text: "账号:",
				},
				LineEdit{
					AssignTo: &this.LoginCount,
				},
				Label{
					Text: "密码:",
				},
				LineEdit{
					AssignTo:     &this.LoginPwd,
					PasswordMode: true,
					OnKeyDown: func(key walk.Key) { //键盘事件
						if key == walk.KeyReturn { //回车键
							this.viewLogin()
						}
					},
				},
			},
		},
		Composite{
			Layout: HBox{},
			Children: []Widget{
				HSpacer{},
				PushButton{
					Text:    "登陆",
					MaxSize: Size{Width: 60, Height: 60},
					OnClicked: func() {
						this.viewLogin()
					},
				},
				PushButton{
					Text:    "注册",
					MaxSize: Size{Width: 60, Height: 60},
					OnClicked: func() {
						this.RegisterView()
					},
				},
			},
		},
	}

	// MyLOG.Log("Before: %#v", this.LoginPwd)

	this.mw_LoginPage.Title = "聊天室 - 登陆"
	this.mw_LoginPage.Size = Size{Width: 300, Height: 100}
	this.mw_LoginPage.MinSize = Size{Width: 300, Height: 100}
	this.mw_LoginPage.MaxSize = Size{Width: 300, Height: 100}
	this.mw_LoginPage.Background = SolidColorBrush{Color: walk.RGB(0xFF, 0xFF, 0xFF)}
	this.mw_LoginPage.Layout = VBox{}
	this.mw_LoginPage.Children = children

	this.mw_LoginPage.Run()
}

func (this *View) viewLogin() {
	// MyLOG.Log("After: %#v", this.LoginPwd)
	tools.MyLOG.Log("登陆账号: %s", this.LoginCount.Text())

	loginCheckErr := viewLoginCheckCountPwd(this.LoginCount.Text(), this.LoginPwd.Text())
	if loginCheckErr != nil {
		messageBox("错误", loginCheckErr.Error())
		return
	}

	loginErr := ClientProc.Login(this.LoginCount.Text(), this.LoginPwd.Text())
	if loginErr != nil {
		messageBox("错误", loginErr.Error())
		return
	}
	this.mwAssign_LoginPage.Close()
	this.MainPage()
}

func viewLoginCheckCountPwd(count string, pwd string) (err error) {
	if count == "" {
		err = errs.CountEmpty
		return
	}
	if pwd == "" {
		err = errs.PwdEmpty
		return
	}
	return
}
