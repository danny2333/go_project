//go:generate go-winres make --product-version=git-tag
package main

import (
	"fmt"
	"os"
	"os/signal"
	"sync"

	server "synk/server"

	"github.com/zserge/lorca"
)

func recoverFromError() {
	if r := recover(); r != nil {
		fmt.Println("Recovering from panic:", r)
	}
}
func main() {
	// 声明一个等待组
	var endWaiter sync.WaitGroup

	// 每一个任务开始时, 将等待组增加1
	endWaiter.Add(1)
	start := make(chan int)
	end := make(chan interface{})
	go server.Run(start, end)
	go func(start chan int, quit chan interface{}) {
		port := <-start
		defer recoverFromError()
		ui, _ := lorca.New(fmt.Sprintf("http://127.0.0.1:%d/static/index.html", port), "", 800, 600, "--disable-sync", " --disable-translate")
		defer ui.Close()
		quit <- (<-ui.Done())
	}(start, end)
	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, os.Interrupt)
	select {
	case <-signalChannel:
		endWaiter.Done()
	case <-end:
		endWaiter.Done()
	}

	// 等待所有的任务完成
	endWaiter.Wait()
}
