package controller

import (
	"ctrrom/app/constant"
	"ctrrom/pkg/log"
	"fmt"
	"sync"

	"ctrrom/pkg/browser"

	"github.com/lxn/win"
	"github.com/zserge/lorca"
)

const (
	localUIPattern = `http://%s:%s/?hostname=%s&port=%s&secret=`
)

type counter struct {
	sync.Mutex
	count int
}

func (c *counter) Add(n int) {
	c.Lock()
	defer c.Unlock()
	c.count = c.count + n
}

func (c *counter) Value() int {
	c.Lock()
	defer c.Unlock()
	return c.count
}

func Dashboard() {
	_, controllerPort := CheckConfig()

	if dpiScale == 0 {
		dpiScale = 1
	}

	xScreen := int(win.GetSystemMetrics(win.SM_CXSCREEN))
	yScreen := int(win.GetSystemMetrics(win.SM_CYSCREEN))
	var pageWidth int32 = 800
	var pageHeight int32 = 580
	pageWidth, pageHeight = CalcDpiScaledSize(pageWidth, pageHeight)
	PageInit := lorca.Bounds{
		Left:        (xScreen - int(pageWidth)) / 2,
		Top:         (yScreen - int(pageHeight)) / 2,
		Width:       int(pageWidth),
		Height:      int(pageHeight),
		WindowState: "normal",
	}
	localUIUrl := fmt.Sprintf(localUIPattern, constant.Localhost, constant.DashboardPort,
		constant.Localhost, controllerPort)
	ui, err := lorca.New(localUIUrl, "", 0, 0)
	if err != nil {
		log.Errorln("create dashboard failed %v", err)
		err := browser.Open(localUIUrl)
		if err != nil {
			log.Errorln("open dashboard failed %v", err)
			return
		}
	} else {
		defer func(ui lorca.UI) {
			err := ui.Close()
			if err != nil {
				log.Errorln("close dashboard failed %v", err)
			}
		}(ui)

		ui.Bind("start", func() {
			log.Infoln("UI is ready")
		})

		// Create and bind Go object to the UI
		c := &counter{}
		ui.Bind("counterAdd", c.Add)
		ui.Bind("counterValue", c.Value)

		err := ui.SetBounds(PageInit)
		if err != nil {
			log.Errorln("SetBounds dashboard failed %v", err)
			return
		}
		// Wait until UI window is closed
		select {
		case <-ui.Done():
		}
	}
}
