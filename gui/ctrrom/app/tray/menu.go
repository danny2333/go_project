package tray

import (
	"ctrrom/app/constant"
	"ctrrom/app/controller"
	"ctrrom/common"
	"ctrrom/pkg/log"
	"fmt"
	"io/ioutil"
	"os"

	"ctrrom/pkg/browser"

	"github.com/getlantern/systray"
)

func init() {
	if constant.IsWindows() {
		log.Infoln("Windows系统")
	}

	systray.Run(onReady, onExit)
}

func onReady() {
	log.Infoln("onReady")

	data, err := ioutil.ReadFile(constant.IconPath)
	if err != nil {
		log.Errorln("onReady File reading error: %v", err)
		return
	}
	systray.SetIcon(data)
	systray.SetTitle(common.AppTitle)
	systray.SetTooltip(common.AppTitle + " by Danny")
	mQuitOrig := systray.AddMenuItem(common.AppTitle, "AppTitle")
	go func() {
		<-mQuitOrig.ClickedCh
		fmt.Println("Hi Ctrrom.Mini")
	}()
	systray.AddSeparator()

	// We can manipulate the systray in other goroutines
	go func() {
		mConfig := systray.AddMenuItem("配置管理", "配置管理")
		dashboard := systray.AddMenuItem("控制面板", "控制面板")

		subMenuTop := systray.AddMenuItem("菜单案例", "菜单案例")
		subMenuMiddle := subMenuTop.AddSubMenuItem("系统配置", "系统配置")
		subMenuBottom := subMenuMiddle.AddSubMenuItemCheckbox("用户管理", "用户管理", false)
		subMenuBottom2 := subMenuMiddle.AddSubMenuItem("数据管理", "数据管理")

		mUrl := systray.AddMenuItem("Open UI", "my home")

		systray.AddSeparator()

		mQuit := systray.AddMenuItem("退出", "Quit the whole app")

		for {
			select {
			case <-mConfig.ClickedCh:
				go controller.ShowMenuConfig()
			case <-dashboard.ClickedCh:
				go controller.Dashboard()
			case <-mUrl.ClickedCh:
				browser.Open("https://www.getlantern.org")
			case <-subMenuBottom2.ClickedCh:
				fmt.Println("数据管理")
			case <-subMenuBottom.ClickedCh:
				fmt.Println("用户管理")
			case <-mQuit.ClickedCh:
				systray.Quit()
				return
			}
		}
	}()
}

func onExit() {
	log.Infoln("onExit")
	os.Exit(0)
}
