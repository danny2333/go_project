//go:generate go-bindata -pkg static -ignore .../.DS_Store -o gh-pages.go gh-pages/...

package static

import (
	"fmt"
	"net/http"

	"ctrrom/app/constant"
	"ctrrom/pkg/log"

	assetfs "github.com/elazarl/go-bindata-assetfs"
)

// go install github.com/go-bindata/go-bindata/...
// go install github.com/elazarl/go-bindata-assetfs/...
// go-bindata-assetfs www/...

func init() {
	go func() {
		handler := http.FileServer(&assetfs.AssetFS{
			Asset:     Asset,
			AssetDir:  AssetDir,
			AssetInfo: AssetInfo,
			Prefix:    "www",
		})
		if err := http.ListenAndServe(fmt.Sprintf("%s:%s", constant.Localhost, constant.DashboardPort), handler); err != nil {
			log.Fatalln("ListenAndServe error: %v", err)
		}

	}()
}
