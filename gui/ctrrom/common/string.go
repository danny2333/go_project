package common

import "fmt"

const (
	AppTitle = "CtrRom"
)

var ()

func GetSubTitle(subTitle string) string {
	return fmt.Sprintf("%s - %s", subTitle, AppTitle)
}
