package main

import (
	_ "ctrrom/app/tray"
	_ "ctrrom/static"
	"os"
	"os/signal"
	"syscall"
)

// rsrc -manifest app.exe.manifest -ico=app.ico -o rsrc.syso
// go build -ldflags="-H windowsgui"

func main() {

	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)
	<-sigCh
}
