### 包

像例子里面的 cb 就是后面要引用的包的别名（防止冲突）

- 格式：别名+空格+引用的包名（包名加双引号）

```bash
import (
  "fmt"
  "strings"
  cb "github.com/hyperledger/fabric/protos/common"
  "github.com/golang/protobuf/proto"
)
```

- 前面加下划线，只调用包里面的 init 函数。无法调用包中别的函数。

```bash
import (
  "fmt"
  "strings"
  _ "github.com/hyperledger/fabric/protos/common"
"github.com/golang/protobuf/proto"
)
```

fmt 前面加点，调用函数时可以省略包的名字。即：fmt.println()可以写成 println()

```bash
import (
  . "fmt"
  "strings"
  "github.com/hyperledger/fabric/protos/common"
  "github.com/golang/protobuf/proto"
  )
```
