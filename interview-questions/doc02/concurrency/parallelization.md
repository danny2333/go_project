# 多核并行化与同步锁

## 1. 多核并行化

```go
//多核并行化
runtime.GOMAXPROCS(16) //设置环境变量GOMAXPROCS的值来控制使用多少个CPU核心
runtime.NumCPU() //来获取核心数
//出让时间片
runtime.Gosched() //在每个goroutine中控制何时出让时间片给其他goroutine
```

## 2. 同步锁

```go
//同步锁
sync.Mutex //单读单写：占用Mutex后，其他goroutine只能等到其释放该Mutex
sync.RWMutex //单写多读：会阻止写，不会阻止读
RLock() //读锁
Lock() //写锁
RUnlock() //解锁（读锁）
Unlock() //解锁（写锁）
//全局唯一性操作
//once的Do方法保证全局只调用指定函数(setup)一次，其他goroutine在调用到此函数是会阻塞，直到once调用结束才继续
/*
- 应用场景：高并发的场景下，有些操作只需要执行一次，例如只加载一次配置文件、只关闭一次通道等；
- sync.Once只有一个Do方法：func (o *Once) Do(f func())
- sync.Once内部包含一个互斥锁和一个布尔值，互斥锁保证布尔值和数据的安全；布尔值用来记录初始化是否完成；这样设计就能保证初始化操作的时候是并发安全的并且初始化操作也不会被执行多次
*/
once.Do(setup)
```
