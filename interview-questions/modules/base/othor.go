package base

import (
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"
)

// 注意点 方法名如果首字小写 代表私有 无法再外部导入，且会提示方法未引用

// Chengfa99
func Chengfa99() {
	for i := 1; i <= 9; i++ {
		for j := 1; j <= i; j++ {
			fmt.Printf("%d*%d=%d ", i, j, i*j)
		}
		fmt.Println()
	}
}

// Bubble_sort ...
func Bubble_sort(nums []int) []int {
	length := len(nums)

	for i := 0; i < length; i++ {
		for j := i + 1; j < length; j++ {
			if nums[i] < nums[j] {
				temp := nums[i]
				nums[i] = nums[j]
				nums[j] = temp
			}
		}
	}

	return nums
}

// JsonData ...
func JsonData() {
	type AutoGen struct {
		Age   int    `json:"age"`
		Name  string `json:"name"`
		Child []int  `json:"child"`
	}

	jsonStr1 := `{"age":22, "name":"peter", "child": [1,2,3]}`

	a := AutoGen{}
	json.Unmarshal([]byte(jsonStr1), &a)
	fmt.Println(a)

	b, err := json.Marshal(a)
	if err != nil {
		fmt.Println("JSON ERR:", err)
	}
	// fmt.Println(string(b))
	fmt.Printf("%s\n\n", b)
}

// LockTest 加锁和不加锁的区别
func LockTest() {
	// 测试sync.Mutex

	// 不加锁数据不正确 造成该问题的原因是协程在读count值时，假设有a、b两个携程，a读取了count值为2，还未增加，
	// 这时b也读取了count值为2，a、b进行了加1，count=3.这种情况会在多个协程中发生，
	// 导致了最终count累加不到10000，相当于脏读。
	// 这里我们可以使用Mutex来解决这个问题

	// Sync.Mutex 可以进行加锁。在每个协程累加前读取Mutex，如果被锁则等待，这样就可以防止count在同一时间被重复读取导致最终数据出错
	var m sync.Mutex
	count := 0
	for i := 0; i < 10000; i++ {
		go func() {
			m.Lock()
			defer m.Unlock()
			count++
		}()
	}

	time.Sleep(time.Second * 2)

	fmt.Println(count)
}

// SliceArrMerQues ...
func SliceArrMerQues() {
	// a := [5]int{0, 0, 0, 0, 2}

	// ...长度自动 4 代表几位,后面2 最后的值
	a := [...]int{4: 2}

	// 指向数组的指针
	var p *[5]int = &a
	fmt.Println(p)

	numss := []int{5, 9, 6, 8, 12, 42}
	// result := bubble_sort(numss...)
	result := Bubble_sort(numss)
	fmt.Println(result)

	var arrayData = [4]string{"a", "b", "c", "d"}

	fmt.Println("arrayData: ", arrayData)

	var slice1 = arrayData[0:4]
	fmt.Println("slice1: ", slice1)

	var slice2 = arrayData[0:4]
	fmt.Println("slice2: ", slice2)

	slice1[0] = "w"
	slice1[1] = "x"
	slice1[2] = "y"
	slice1[3] = "z"
	fmt.Println("修改了slice1")

	// slice1和slice2共用了同一个底层array 会覆盖
	fmt.Println("slice1 is: ", slice1)
	fmt.Println("slice2 is: ", slice2)
}

// Book ...
type Book struct {
	title  string
	author string
	price  int
}

// BookStore ...
type BookStore struct {
	BookList []*Book
}

// TestStruct ...
func TestStruct() {
	book1 := Book{title: "Vue学习", author: "尤大婶", price: 20}

	bookStore := BookStore{}
	bookStore.BookList = append(bookStore.BookList, &book1)

	for i, book := range bookStore.BookList {
		fmt.Println(i, book)
	}
}

// Jindu 进度条
func Jindu() {
	for i := 0; i < 50; i++ {
		time.Sleep(100 * time.Millisecond)
		h := strings.Repeat("=", i) + strings.Repeat(" ", 49-i)
		fmt.Printf("\r%.0f%%[%s]", float64(i)/49*100, h)
	}
}
