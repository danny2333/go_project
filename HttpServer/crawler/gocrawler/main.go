package main

import (
	"strconv"

	"gocrawler/engine"
	"gocrawler/parser/lianjia"
	"gocrawler/persist"
	"gocrawler/scheduler"
)

func main() {
	itemChan, err := persist.ItemSaver("lianjia")
	if err != nil {
		panic(nil)
	}

	e := engine.Engine{
		WorkerCount: 10,
		Scheduler:   &scheduler.QueueScheduler{},
		ItemChan:    itemChan,
	}

	var reqList []engine.Request

	// 链家的租房无明显入口，每页内容也无较好方法提取的下一页url，所以先手动提供120个分页入口来爬取内容
	for i := 1; i < 10; i++ {
		req := engine.Request{
			URL:       "https://sz.lianjia.com/zufang/pg" + strconv.Itoa(i),
			ParseFunc: lianjia.RentListParser,
		}
		reqList = append(reqList, req)
	}

	e.Run(reqList...)
}
