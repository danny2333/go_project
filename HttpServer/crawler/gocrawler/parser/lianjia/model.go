package lianjia

// House 结构体
type House struct {
	Code  string // 编码
	Img   string // 图片
	Name  string // 名称
	Price string // 价格
}
