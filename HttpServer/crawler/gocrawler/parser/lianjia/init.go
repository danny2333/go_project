package lianjia

import (
	"context"
	"gocrawler/pkg/esorm"
	"log"
)

const (
	INDEX = "lianjia"
)

func init() {
	esorm.Init()
	ctx := context.Background()
	if ok, _ := esorm.GetElasticDefault().IndexExists(ctx, INDEX); !ok {
		if err := esorm.GetElasticDefault().CreateTable(ctx, INDEX); err != nil {
			log.Fatalf("CreateTable Error: %v", err)
		} else {
			log.Println("CreateTable Successify.")
		}
	}
	log.Printf("索引%v已存在\n", INDEX)
}
