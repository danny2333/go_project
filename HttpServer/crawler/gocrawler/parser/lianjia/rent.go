package lianjia

import (
	"strings"

	"gocrawler/engine"

	"github.com/PuerkitoBio/goquery"
)

// RentParser 详情
func RentParser(doc *goquery.Document) (engine.ParseResult, error) {
	var result engine.ParseResult
	var ID, URL string

	// h, _ := doc.Html()
	// log.Printf("%v", h)
	// os.Exit(0)
	var house House

	content := doc.Find(".wrapper .content")

	house.Code = content.Find(".watch-btn").AttrOr("data-id", "")
	house.Img = content.Find(".content__article__slide__wrapper .content__article__slide__item img").Eq(0).AttrOr("src", "")
	house.Name = strings.TrimSpace(content.Find(".content__title").Text())
	house.Price = content.Find(".content__aside--title span").Eq(0).Text()

	// 匹配到的ID内容,房源验真编号：105101392982,处理后得到实际ID
	ID = doc.Find(".gov_title").Text()
	ID = strings.Replace(ID, "房源验真编号：", "", -1)

	// URL = "https://sz.lianjia.com/zufang/" + house.Code + ".html"
	// log.Printf("%#v\n", house)
	// fmt.Println("ID=", strings.TrimSpace(ID))
	// fmt.Println("URL=", URL)
	// os.Exit(0)
	result.Items = append(result.Items, engine.Item{
		ID:      ID,
		URL:     URL,
		Type:    "rent",
		Payload: house,
	})

	return result, nil
}

// RentListParser 列表
func RentListParser(doc *goquery.Document) (engine.ParseResult, error) {
	var result engine.ParseResult
	var URL string

	doc.Find(".content__list--item").Each(func(i int, s *goquery.Selection) {
		URL = s.Find("a.content__list--item--aside").AttrOr("href", "")
		if URL != "" {
			result.Requests = append(result.Requests, engine.Request{
				URL:       "https://sz.lianjia.com" + URL,
				ParseFunc: RentParser,
			})
		}

	})
	return result, nil
}
