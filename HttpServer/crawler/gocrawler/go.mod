module gocrawler

go 1.18

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/olivere/elastic v6.2.37+incompatible
	golang.org/x/net v0.0.0-20210916014120-12bc252f5db8
	golang.org/x/text v0.3.6
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/mailru/easyjson v0.7.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
