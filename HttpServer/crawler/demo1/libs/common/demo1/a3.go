package demo1

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

func HttpGet2(url string) (result string, err error) {
	log.Println(url)
	resp, err := http.Get(url)
	if err != nil {
		return result, err
	}

	defer resp.Body.Close()

	time.Sleep(time.Second * 1000)
	// 循环读取 网页数据， 传出给调用者
	buf := make([]byte, 4096)
	for {
		n, err := resp.Body.Read(buf)
		if n == 0 {
			fmt.Println("读取网页完成")
			break
		}
		if err != nil && err != io.EOF {
			return result, err
		}
		// 累加每一次循环读到的 buf 数据，存入result 一次性返回。
		result += string(buf[:n])
	}
	return
}

func SubMain() {
	v := "http://www.pufei8.com/manhua/419/373107.html?page=1"
	str, err := HttpGet2(v)
	log.Println(err)
	log.Println(str)
}
