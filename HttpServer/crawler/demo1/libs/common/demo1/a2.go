package demo1

import (
	"log"
	"net/http"

	"github.com/PuerkitoBio/goquery"
)

// ExampleScrape ...
func ExampleScrape() {
	res, err := http.Get("https://stackoverflow.com/questions/tagged/javascript")
	if err != nil {
		log.Fatal("http Get err", err)
	}

	defer res.Body.Close()

	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal("goquery err", err)
	}

	doc.Find(".question-summary .summary").Each(func(i int, s *goquery.Selection) {
		title := s.Find("H3").Text()
		log.Println(i, title)
	})
}
