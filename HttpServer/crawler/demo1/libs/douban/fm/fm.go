package fm

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// FmRoot 响应信息结构体
type FmRoot struct {
	R                int
	IsShowQuickStart int
	Song             []FmSong
}

// FmSong ...
type FmSong struct {
	Album       string
	Picture     string
	Ssid        string
	Artist      string
	Url         string
	Company     string
	Title       string
	Rating_avg  int
	Length      int64
	Subtype     string
	Public_time string
	Sid         string
	Aid         string
	Sha256      string
	Kbps        string
	Albumtitle  string
	Like        int
}

// Sinfo 统计信息
type Sinfo struct {
	Etime     float32
	TotalCot  int
	TotalMer  int
	SucceeCot int
}

const (
	// dir = "H:\\life story\\music\\"
	dir = "./music"
)

// Smain 抓取音乐列表
func Smain() []byte {
	client := &http.Client{
		Timeout: time.Duration(5 * time.Second), //超时时间5s
	}
	req, _ := http.NewRequest("GET", "https://fm.douban.com/j/mine/playlist?type=n&sid=347955&pt=2.9&channel=-3&pb=64&from=mainsite&r=1d815d7ebf", nil)
	// req.Header.Add("Accept:", "*/*") // 这个有问题
	//req.Header.Add("Accept-Encoding", "gzip,deflate,sdch")
	req.Header.Add("Connection", "keep-alive")
	req.Header.Add("Accept-Language", "zh-CN,zh;q=0.8")
	req.Header.Add("Cookie", "openExpPan=Y; bid=\"8YicOUp+Kx0\"; dbcl2=\"56345753:wqSu+cIqVy8\"; fmNlogin=\"y\"; __utma=58778424.1673168777.1384438525.1385773681.1385773681.15; __utmb=58778424.1.9.1385790386347; __utmc=58778424; __utmz=58778424.1384438525.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)")
	req.Header.Add("Host", "douban.fm")
	req.Header.Add("Referer", "http://douban.fm/")
	req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36")

	resp, err := client.Do(req)
	if resp.StatusCode == 200 {
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)
		// log.Println(string(body))
		return body
	} else {
		log.Fatal(err)
		os.Exit(1)
	}
	return nil
}

// Smain2 抓取音乐列表
func Smain2() {

	client := &http.Client{}

	req, err := http.NewRequest("POST", "http://www.01happy.com/demo/accept.php", strings.NewReader("name=cjb"))
	if err != nil {
		log.Println("NewRequest Error: ", err.Error())
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Cookie", "name=anny")

	resp, err := client.Do(req)
	if err != nil {
		log.Println("client.Do Error: ", err.Error())
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	log.Println("body", string(body))
	log.Println("resp.StatusCode", resp.StatusCode)
}

/* func main() {
	fmt.Println("Spilder start................!")
	startTime := time.Now().UnixNano()
	sinfo := Sinfo{}
	sum := 1

	for sum < 10 {
		data := Smain()
		if data != nil {
			var root FmRoot
			err := json.Unmarshal(data, &root)
			if err != nil {
				fmt.Println("Start Down File....")
				CrawMusic(root.Song, &sinfo)
			} else {
				log.Fatal(err)
			}
		}
	}

	sinfo.Etime = float32(time.Now().UnixNano()-startTime) / 1e9
	fmt.Printf("success cot %d  memory %d(M) total cot %d \n", sinfo.TotalCot, sinfo.TotalMer, sinfo.TotalCot)
	fmt.Printf("exceute time %.3fs\n", sinfo.Etime)
} */

// CrawMusic 抓取
func CrawMusic(song []FmSong, sinfo *Sinfo) {
	sinfo.TotalCot = sinfo.TotalCot + len(song)
	for _, v := range song {
		if !FileExist(dir+v.Title+".mp3") && v.Url != "" {
			log.Println(v.Url + " " + v.Title + "  " + strconv.FormatInt(v.Length, 10))
			DownMusic(v, sinfo)
		}
	}
}

// DownFile ...
func DownFile(url string) []byte {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Accept-Encoding", "gzip,deflate,sdch")
	req.Header.Add("Accept-Language", "zh-CN,zh;q=0.8")
	req.Header.Add("Cookie", "openExpPan=Y; bid=\"8YicOUp+Kx0\"; dbcl2=\"56345753:wqSu+cIqVy8\"; fmNlogin=\"y\"; __utma=58778424.1673168777.1384438525.1385773681.1385773681.15; __utmb=58778424.1.9.1385790386347; __utmc=58778424; __utmz=58778424.1384438525.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)")
	req.Header.Add("Host", "mr3.douban.com")
	req.Header.Add("Referer", "http://douban.fm/")
	req.Header.Add("Connection", "keep-alive")
	req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36")
	resp, _ := client.Do(req)

	if resp.StatusCode == 200 {
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)
		return body
	} else {
		log.Println("error -->" + url)
	}
	return nil
}

// DownMusic 下载文件
func DownMusic(song FmSong, sinfo *Sinfo) {

	file := dir + song.Title + ".mp3"
	dts := DownFile(song.Url)
	if dts != nil {
		defer func() {
			f, _ := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_APPEND, os.ModePerm)
			defer f.Close()
			ioutil.WriteFile(file, dts, os.ModePerm)
			sinfo.SucceeCot++
		}()
	} else {
		log.Println("save file error")
	}
}

// FileInfo 保存歌词详细信息
func FileInfo() {
	//TODO
}

//  FileExist 文件是否存在
func FileExist(path string) bool {
	_, err := os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		return false
	}
	return true
}
