package ngabbs

import (
	"encoding/csv"
	"fmt"
	"httpserver/crawler/utils"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"time"
)

// GetPageStr ...
func GetPageStr(page string) (content string) {
	var pageStr string
	if page == "" {
		pageStr = "1"
	} else {
		pageStr = page
	}
	url := "https://ngabbs.com/thread.php?fid=-7955747&page=" + pageStr

	client := &http.Client{
		Timeout: time.Duration(5 * time.Second), //超时时间5s
	}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Connection", "keep-alive")
	req.Header.Add("Accept-Language", "zh-CN,zh-TW;q=0.9,zh;q=0.8,en;q=0.7")
	req.Header.Add("Cookie", "UM_distinctid=17ae2168de91c6-04700b492ae294-2343360-1fa400-17ae2168deb950; ngacn0comUserInfo=danny2333%09danny2333%0939%0939%09%0910%090%090%090%090%09; ngaPassportUid=63013128; ngaPassportUrlencodedUname=danny2333; ngaPassportCid=X93vjvp175l7jdfkatiqenhne5vc8fovmtivl1u3; ngacn0comUserInfoCheck=017681ff1b6e8608bc10377b22efa755; ngacn0comInfoCheckTime=1627297620; CNZZDATA30043604=cnzz_eid%3D57813405-1627286657-https%253A%252F%252Fngabbs.com%252F%26ntime%3D1627297457; lastvisit=1627299085; lastpath=/read.php?tid=27646434&page=14; bbsmisccookies=%7B%22pv_count_for_insad%22%3A%7B0%3A-191%2C1%3A1627317382%7D%2C%22insad_views%22%3A%7B0%3A2%2C1%3A1627317382%7D%2C%22uisetting%22%3A%7B0%3A%22a%22%2C1%3A1627297919%7D%7D; _cnzz_CV30043604=forum%7Cfid-7955747%7C0")
	req.Header.Add("Host", "ngabbs.com")
	req.Header.Add("Referer", "https://ngabbs.com/thread.php?fid=-7955747")
	req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36")

	resp, err := client.Do(req)

	if err != nil {
		return ""
	}
	defer resp.Body.Close()
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ""
	}

	utf8, err := utils.GbkToUtf8(respBytes)
	if err != nil {
		return ""
	} else {
		return string(utf8)
	}
}

// Item ...
type Item struct {
	URL   string `json:"url"`
	Title string `json:"title"`
}

// GetListsTotal 获取最大页数
func GetListsTotal(html string) int {

	reg := regexp.MustCompile(`<a.*?href="(.*?)".*?class='pager_spacer'.*?>`)

	res := reg.FindAllStringSubmatch(html, -1)
	// fmt.Println("res: ", html)
	if len(res) == 0 {
		return 0
	}

	total, _ := strconv.Atoi(res[0][1])

	return total
}

// GetAllList ...
func GetAllList() {
	// firstHtml := GetPageStr("")
	// println(firstHtml)
	// total := GetListsTotal(firstHtml)
	total := 100
	fmt.Println("总页数: ", total)

	// var items []Item

	var items [][]string

	for i := 1; i <= total; i++ {
		html := GetPageStr(fmt.Sprintf("%d", i))
		fmt.Printf("正在抓取第%d页\n", i)
		// reg := regexp.MustCompile(`<div.*?class="title">(?s).*?<font.*?>(?s).*?<a.*?href="(?P<url>.*?)".*?title="(?P<title>.*?)".*?target.*?>(?s).*?</font>(?s).*?</div>`)

		// reg := regexp.MustCompile(`<a.*?href='(?P<url>.*?)'.*?id='.*?'.*?class='topic'>(?P<title>.*?)</a>`)

		reg := regexp.MustCompile(`<a.*?class='replies'>(?P<replies>.*?)</a>(?s).*?<a.*?href='(?P<url>.*?)'.*?id='.*?'.*?class='topic'>(?P<title>.*?)</a>`)

		result := reg.FindAllStringSubmatch(html, -1)

		if len(result) == 0 {
			fmt.Println("null")
		}

		for _, match := range result {
			/* var item = Item{
				URL:   match[1],
				Title: match[2],
			}
			items = append(items, item) */

			items = append(items, []string{
				"https://ngabbs.com" + match[2], match[3], match[1],
			})
		}
	}
	// fmt.Println(items)

	if err := csvExport(items); err != nil {
		fmt.Println(err.Error())
	}
}

// GetLists ...
func GetLists() {
	html := GetPageStr("")
	println(html)
	// total := GetListsTotal(html)
	// fmt.Println("总页数: ", total)
	// 应该是使用了 . 点操作符，按正则表达式规则应该匹配所有字符，但实际上却没有匹配换行符。
	// 这是因为在 golang 里 . 默认不匹配换行符.. 需要在前面加上(?s)

	// reg := regexp.MustCompile(`<div.*?class="title">(?s).*?<font.*?>(?s).*?<a(?s:(.*?))>(?s).*?</font>(?s).*?</div>`)
	// reg := regexp.MustCompile(`<div.*?class="title">(?s).*?<font.*?>(?s:(.*?))</font>(?s).*?</div>`)
	// reg := regexp.MustCompile(`<font.*?>(?s:(.*?))</font>`)

	// reg := regexp.MustCompile(`<div.*?class="title">(?s).*?<font.*?>(?s).*?<a.*?href="(?P<url>.*?)".*?title="(?P<title>.*?)".*?target.*?>(?s).*?</font>(?s).*?</div>`)

	// reg := regexp.MustCompile(`<a.*?href='(?P<url>.*?)'.*?id='.*?'.*?class='topic'>(?P<title>.*?)</a>`)

	reg := regexp.MustCompile(`<a.*?class='replies'>(?P<replies>.*?)</a>(?s).*?<a.*?href='(?P<url>.*?)'.*?id='.*?'.*?class='topic'>(?P<title>.*?)</a>`)

	result := reg.FindAllStringSubmatch(html, -1)

	fmt.Println(len(result))
	if len(result) == 0 {
		fmt.Println("null")
	}

	// var items []Item // 1 结构体存储

	// items := make(map[int]Item) // 2 map存储

	var items [][]string // 3 数组存储

	for _, match := range result {
		/* var item = Item{
			URL:   match[1],
			Title: match[2],
		}
		items = append(items, item) */

		// r := Item{URL: match[1], Title: match[2]}
		// items[i] = r

		// items[i] = []string{
		// 	match[1], match[2],
		// }
		items = append(items, []string{
			"https://ngabbs.com" + match[2], match[3], match[1],
		})
	}

	fmt.Println(items)

	if err := csvExport(items); err != nil {
		fmt.Println(err.Error())
	}
}

// csvExport 导出csv
func csvExport(data [][]string) error {
	file, err := os.Create(fmt.Sprintf("data/ngabbs-%v.csv", time.Now().Format("20060102150405")))
	if err != nil {
		return err
	}
	defer file.Close()

	file.WriteString("\xEF\xBB\xBF") // 写入UTF-8 BOM，防止中文乱码

	writer := csv.NewWriter(file)
	defer writer.Flush()

	headers := []string{"链接", "标题", "评论数"}
	writer.Write(headers)

	for _, value := range data {
		if err := writer.Write(value); err != nil {
			return err // let's return errors if necessary, rather than having a one-size-fits-all error handler
		}
	}

	return nil
}
