package ngabbs

type Ngabbs struct {
	Host      string
	Fid       string
	Page      int
	TotalPage int
	Post      Post
}

type Post struct {
	Title    string
	Link     string
	Reviews  int
	PostDate string
}

func (n *Ngabbs) SetHost(host string) {
	n.Host = host
}

func (n *Ngabbs) ParsePage() {
	// todo
}
