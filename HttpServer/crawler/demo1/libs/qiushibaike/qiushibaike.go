package qiushibaike

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"
)

const PageNum = 13

type Joke struct {
	Author  string `json:"author"`
	Link    string `json:"link"`
	Content string `json:"content"`
	Type    string `json:"type"`
}

// 失效 已暂停网页浏览
func SubMain() {
	// 每一页对应的url链接
	var url = "https://www.qiushibaike.com/text/page/%d/"
	var jokes []Joke
	var divPattern = regexp.MustCompile(`(?s)<div class="article.*?typs_(.*?)".*?>.*?<div class="author.*?>.*?<a onclick.*?>.*?<h2>(.*?)</h2>.*?<a href="(.*?)".*? class="contentHerf".*?>.*?<div class="content">.*?<span>(.*?)</span>.*?</div>`)

	for i := 1; i < PageNum; i++ {
		resp, err := http.Get(fmt.Sprintf(url, i))
		if err != nil {
			fmt.Println("Get error: ", err)
			continue
		}

		data, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			fmt.Println("ReadAll Err: ", err)
			continue
		}

		for _, div := range divPattern.FindAllSubmatch(data, -1) {
			jokes = append(jokes, Joke{
				Author:  strings.TrimSpace(string(div[2])),
				Link:    "https://www.qiushibaike.com" + string(div[3]),
				Content: strings.TrimSpace(string(div[4])),
				Type:    string(div[1]),
			})
		}
	}
	if len(jokes) < 1 {
		fmt.Println("没有数据...")
		return
	}
	file, err := os.Create(fmt.Sprintf("data/jokes-%v.json", time.Now().Format("20060102150405")))
	if err != nil {
		fmt.Println("Create file Error")
		return
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	encoder.SetIndent("  ", "    ")
	if err := encoder.Encode(jokes); err != nil {
		fmt.Println("Encode Err: ", err)
	}
}
