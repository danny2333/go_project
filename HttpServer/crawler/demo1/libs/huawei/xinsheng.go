package huawei

import (
	"encoding/csv"
	"fmt"
	"httpserver/crawler/utils"
	"os"
	"regexp"
	"strconv"
)

// GetPageStr ...
func GetPageStr(page string) (content string) {
	// http://xinsheng.huawei.com/cn/index.php?app=forum&mod=List&act=index&class=462&order=replycount&type=&sign=&special=&search=&cate=241&p=1
	var pageStr string
	if page == "" {
		pageStr = "1"
	} else {
		pageStr = page
	}
	url := "http://xinsheng.huawei.com/cn/index.php?app=forum&mod=List&act=index&class=462&order=replycount&type=&sign=&special=&search=&cate=241&p=" + pageStr
	content = utils.Get(url)
	return
}

// Item ...
type Item struct {
	URL   string `json:"url"`
	Title string `json:"title"`
}

// GetListsTotal 获取最大页数
func GetListsTotal(html string) int {

	reg := regexp.MustCompile(`<li><input.*?id="changeToPage".*?/>(.*?)</li>`)

	res := reg.FindAllStringSubmatch(html, -1)
	if len(res) == 0 {
		return 0
	}
	reg2 := regexp.MustCompile(`[0-9]+`)

	res2 := reg2.FindAllStringSubmatch(res[0][1], -1)

	if len(res2) == 0 {
		return 0
	}

	total, _ := strconv.Atoi(res2[0][0])

	return total
}

// GetAllList ...
func GetAllList() {
	firstHtml := GetPageStr("")
	total := GetListsTotal(firstHtml)
	fmt.Println("总页数: ", total)

	// var items []Item

	var items [][]string

	for i := 1; i <= total; i++ {
		html := GetPageStr(fmt.Sprintf("%d", i))
		fmt.Printf("正在抓取第%d页\n", i)
		reg := regexp.MustCompile(`<div.*?class="title">(?s).*?<font.*?>(?s).*?<a.*?href="(?P<url>.*?)".*?title="(?P<title>.*?)".*?target.*?>(?s).*?</font>(?s).*?</div>`)

		result := reg.FindAllStringSubmatch(html, -1)

		if len(result) == 0 {
			fmt.Println("null")
		}

		for _, match := range result {
			/* var item = Item{
				URL:   match[1],
				Title: match[2],
			}
			items = append(items, item) */

			items = append(items, []string{
				match[1], match[2],
			})
		}
	}
	// fmt.Println(items)

	if err := csvExport(items); err != nil {
		fmt.Println(err.Error())
	}
}

// GetLists ...
func GetLists() {
	html := GetPageStr("")

	total := GetListsTotal(html)
	fmt.Println("总页数: ", total)
	// 应该是使用了 . 点操作符，按正则表达式规则应该匹配所有字符，但实际上却没有匹配换行符。
	// 这是因为在 golang 里 . 默认不匹配换行符.. 需要在前面加上(?s)

	// reg := regexp.MustCompile(`<div.*?class="title">(?s).*?<font.*?>(?s).*?<a(?s:(.*?))>(?s).*?</font>(?s).*?</div>`)
	// reg := regexp.MustCompile(`<div.*?class="title">(?s).*?<font.*?>(?s:(.*?))</font>(?s).*?</div>`)
	// reg := regexp.MustCompile(`<font.*?>(?s:(.*?))</font>`)
	reg := regexp.MustCompile(`<div.*?class="title">(?s).*?<font.*?>(?s).*?<a.*?href="(?P<url>.*?)".*?title="(?P<title>.*?)".*?target.*?>(?s).*?</font>(?s).*?</div>`)

	result := reg.FindAllStringSubmatch(html, -1)

	fmt.Println(len(result))
	if len(result) == 0 {
		fmt.Println("null")
	}

	// var items []Item // 1 结构体存储

	// items := make(map[int]Item) // 2 map存储

	var items [][]string // 3 数组存储

	for _, match := range result {
		/* var item = Item{
			URL:   match[1],
			Title: match[2],
		}
		items = append(items, item) */

		// r := Item{URL: match[1], Title: match[2]}
		// items[i] = r

		// items[i] = []string{
		// 	match[1], match[2],
		// }
		items = append(items, []string{
			match[1], match[2],
		})
	}

	fmt.Println(items)

	if err := csvExport(items); err != nil {
		fmt.Println(err.Error())
	}
}

// csvExport 导出csv
func csvExport(data [][]string) error {
	file, err := os.Create("result.csv")
	if err != nil {
		return err
	}
	defer file.Close()

	file.WriteString("\xEF\xBB\xBF") // 写入UTF-8 BOM，防止中文乱码

	writer := csv.NewWriter(file)
	defer writer.Flush()

	headers := []string{"链接", "标题"}
	writer.Write(headers)

	for _, value := range data {
		if err := writer.Write(value); err != nil {
			return err // let's return errors if necessary, rather than having a one-size-fits-all error handler
		}
	}

	return nil
}
