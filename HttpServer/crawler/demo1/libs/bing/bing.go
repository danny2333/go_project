package bing

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
)

var baseUrl = "https://www.bing.com"

// FetchBinBgImg 抓取cn.bing.com壁纸
func FetchBinBgImg() {
	var reg = regexp.MustCompile(`(?Us)<link\s*rel="preload"\s*href="(.*)".*id="preloadBg"`)
	var imgPath string = ""
	resp, _ := http.Get(baseUrl + "/?FORM=BEHPTB&ensearch=1")
	body, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if matchResult := reg.FindAllStringSubmatch(string(body[:]), -1); matchResult != nil {
		for _, match := range matchResult {
			fmt.Printf("%#v\n\n", match)
			imgPath = baseUrl + match[1]
		}
	}
	fmt.Println(imgPath)
}
