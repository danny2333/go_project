package baidu

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// https://tieba.baidu.com/f?kw=广东体育&ie=utf-8
// pid 7127767327

// https://tieba.baidu.com/f?ie=utf-8&kw=妹子&fr=search

var (
	fullPath = "https://tieba.baidu.com/p/%s/?ph=%s"
)

// GetTiebaPageStr 抓取页面
func GetTiebaPageStr(pid, page string) string {
	var pageStr string
	if page == "" {
		pageStr = "1"
	} else {
		pageStr = page
	}

	url := fmt.Sprintf(fullPath, pid, pageStr)
	fmt.Println("url: ", url)

	client := &http.Client{
		Timeout: time.Duration(5 * time.Second), //超时时间5s
	}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Connection", "keep-alive")
	req.Header.Add("Accept-Language", "zh-CN,zh-TW;q=0.9,zh;q=0.8,en;q=0.7")
	req.Header.Add("Cookie", "BIDUPSID=B5C249E86282C53EFAFFBE56604FDBC4; PSTM=1629335044; BAIDUID=B5C249E86282C53EF709428DA13EC1D2:FG=1; __yjs_duid=1_4a9dfd4e55c1bf8790d90ed271fd5c5b1629337435521; delPer=0; wise_device=0; Hm_lvt_98b9d8c2fd6608d564bf2ac2ae642948=1629343035; BAIDU_WISE_UID=wapp_1629343036462_221; USER_JUMP=-1; st_key_id=17; bdshare_firstime=1629343285189; video_bubble0=1; BDRCVFR[f83LjLo_kJ6]=mk3SLVN4HKm; BDRCVFR[dG2JNJb_ajR]=mk3SLVN4HKm; BDRCVFR[-pGxjrCMryR]=mk3SLVN4HKm; PSINO=6; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; H_PS_PSSID=34433_34380_34370_31253_34405_34004_34383_34072_34092_34106_34094_26350_34418_34323_34289_34390; BA_HECTOR=a10005010h8k81a5ll1ghrsrm0q; Hm_lpvt_98b9d8c2fd6608d564bf2ac2ae642948=1629353913; st_data=bd53b0cb966ff23424a8980c69eb671dd96a8b086cbdf91459576a874c439192efba73dfcdd61a569d22bf6252b7ede468801ae4014f64d671b50022ef2e00bae8a42593c02982b4981be7bd3ba08aeeda714fe1e7191093654e67cfb3304eeec8cb3d4583582380fca4ec5d819dd81de82873079b52de45704bfedfc59b1d6e; st_sign=1019070f; ab_sr=1.0.1_NGJiZmMwYmU0Y2I1ZGQwZjZmNzU3ZTZlMzI0NDczZmE3ZGU1ZGRlMmYzYTkzN2EzNWJjMmRkNDg5NzNhYWFjNTVjOTJlNmRjMWRkYzA2NGNmODA4OTExM2Q5NGZhMjU1OTg1MGEyMTEwMjk4N2UyOGYyYjI0MGI0YzQxN2NiZjdjMGU5ZGM5ODNhOTI4ODhlNDdlZDIyNTlhOGVmOWRiNw==")
	req.Header.Add("Host", "tieba.baidu.com")
	req.Header.Add("Referer", "https://tieba.baidu.com/p/7501135504?pid=140891844156&cid=0")
	req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36")

	resp, err := client.Do(req)

	if err != nil {
		return ""
	}

	defer resp.Body.Close()
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ""
	}

	return string(respBytes)
}

// GetTiebaArticlePageTotal 获取最大页数
func GetTiebaArticlePageTotal(html string) int {
	reg := regexp.MustCompile(`共<span.*?class="red">(.*?)</span>页`)

	res := reg.FindAllStringSubmatch(html, -1)
	if len(res) == 0 {
		return 0
	}

	total, _ := strconv.Atoi(res[0][1])

	return total
}

// GetTiebaArticle 获取贴吧指定id单个文章
func GetTiebaArticle(pid string) {
	firstHtml := GetTiebaPageStr(pid, "")
	fmt.Println(firstHtml)
	total := GetTiebaArticlePageTotal(firstHtml)

	fmt.Println("总页数: ", total)

	// 判断文件夹是否存在
	ok, err := IsExists("data/" + pid)
	if err != nil {
		panic("文件检查失败")
	}
	if !ok {
		err := os.Mkdir("data/"+pid, os.ModePerm)
		if err != nil {
			panic("创建文件夹失败")
		}
	}

	// var items []string

	for i := 1; i <= total; i++ {
		html := GetTiebaPageStr(pid, fmt.Sprintf("%d", i))
		fmt.Printf("正在抓取第%d页\n", i)

		// 应该是使用了 . 点操作符，按正则表达式规则应该匹配所有字符，但实际上却没有匹配换行符。
		// 这是因为在 golang 里 . 默认不匹配换行符.. 需要在前面加上(?s)
		// reg := regexp.MustCompile(`<a.*?class='replies'>(?P<replies>.*?)</a>(?s).*?<a.*?href='(?P<url>.*?)'.*?id='.*?'.*?class='topic'>(?P<title>.*?)</a>`)

		reg := regexp.MustCompile(`<img.*?class="BDE_Image".*?src="(.*?)".*?`)

		// 大图
		// http://tiebapic.baidu.com/forum/pic/item/a1a9f603738da9772fb94368a751f8198718e39d.jpg
		// http://tiebapic.baidu.com/forum/pic/item/d8c3828ba61ea8d33cb5b147800a304e241f589c.jpg

		// 小图
		// http://tiebapic.baidu.com/forum/w%3D580/sign=5d500b9ebc64034f0fcdc20e9fc27980/a1a9f603738da9772fb94368a751f8198718e39d.jpg
		// http://tiebapic.baidu.com/forum/w%3D580/sign=9f757dc2dc1349547e1ee86c664f92dd/d8c3828ba61ea8d33cb5b147800a304e241f589c.jpg

		// http://tiebapic.baidu.com/forum/pic/item/16c3af51f3deb48f2348769de71f3a292df5780e.jpg
		// http://tiebapic.baidu.com/forum/w%3D580/sign=9f757dc2dc1349547e1ee86c664f92dd/16c3af51f3deb48f2348769de71f3a292df5780e.jpg

		result := reg.FindAllStringSubmatch(html, -1)

		if len(result) == 0 {
			fmt.Println("null")
		}

		for _, match := range result {
			//检测文件是否存在,如果存在则跳过,不存在则写入
			path := "data/" + pid + "/" + match[1][strings.LastIndex(match[1], "/")+1:]
			ok, err := IsExists(path)
			if err != nil {
				panic("文件检测错误 >_<!")
			}
			if !ok {
				SaveImg(path, "http://tiebapic.baidu.com/forum/pic/item/"+match[1][strings.LastIndex(match[1], "/")+1:])
				// items = append(items, "http://tiebapic.baidu.com/forum/pic/item/"+match[1][strings.LastIndex(match[1], "/")+1:])
			}
		}
	}

	fmt.Println("Successify.")

}

func SaveImg(path, imgSrc string) {

	//读取图片原始二进制数据
	//延迟防止封ip
	time.Sleep(time.Second * 1)
	bin, err := http.Get(imgSrc)
	if err != nil {
		fmt.Println("访问图片失败: ", err)
	}
	defer bin.Body.Close()
	imgBin, err := ioutil.ReadAll(bin.Body)
	if err != nil {
		fmt.Println("读取图片数据失败: ", path)
	}

	fmt.Println("path: ", path)
	f, err := os.OpenFile(path, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	if err != nil {
		fmt.Println("文件读取失败: ", err)
	}
	defer f.Close()
	_, err = f.Write([]byte(imgBin))
	if err != nil {
		fmt.Println("文件写入失败: ", err)
	}

}

// IsExists 检测文件是否存在
func IsExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}

	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}
