package baidu

import (
	"fmt"
	"httpserver/crawler/pkg/reporter"
	"httpserver/crawler/utils"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	// regHref       = `((ht|f)tps?)://[w]{0,3}.baidu.com/link\?[a-zA-z=0-9-\s]*`
	regTitle      = `<title[\sa-zA-z="-]*>([^x00-xff]|[\sa-zA-Z=-：|，？"])*</title>`
	regCheckTitle = `(为什么|怎么)*.*([G|g][O|o][L|l][A|a][N|n][G|g]).*(怎么|实现|如何|为什么).*`
	reQQEmail     = `(\d+)@qq.com`
	fname         = fmt.Sprintf("data/url-%v.txt", time.Now().Format("20060102150405"))
)

// GetEmail 爬邮箱  函数
func GetEmail() {
	// 1. 去网站拿数据
	resp, err := http.Get("https://tieba.baidu.com/p/6051076813?red_tag=1573533731")
	if err != nil {
		fmt.Println(err, "http.Get url")
	}
	defer resp.Body.Close()
	// 2. 读取页面内容
	pageBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err, "ioutil.ReadAll")
	}
	//fmt.Printf("%T",pageBytes) pageBytes  8位无符号整型 byte 字节型 表示单个字符， uint8 长度是一个字节
	// 将字节转为字符串
	pagestr := string(pageBytes) //
	//fmt.Println(pagestr)
	// 过滤数据
	re := regexp.MustCompile(reQQEmail) // 解析并返回一个正则表达式。如果成功返回，该Regexp就可用于匹配文本。
	// -1 代表取全部记录
	results := re.FindAllStringSubmatch(pagestr, -1)
	fmt.Println(results)

}

// SearchKeyword ...
func SearchKeyword() {
	var num int
	url := "http://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&tn=39042058_20_oem_dg&wd=golang实现&oq=golang 删除数组&rsv_pq=d9be28ec0002df1b&rsv_t=8017GWpSLPhDmKilZQ1StC04EVpUAeLEP90NIm k5pRh5R9o57NHMO8Gaxm1TtSOo/vtJj 98/sc&rqlang=cn&rsv_enter=1&inputT=3474&rsv_sug3=16&rsv_sug1=11&rsv_sug7=100&rsv_sug2=0&rsv_sug4=4230"
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	reg := regexp.MustCompile(`((ht|f)tps?)://[w]{0,3}.baidu.com/link\?[a-zA-z=0-9-\s]*`)
	f, _ := os.OpenFile(fname, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	defer f.Close()
	for _, d := range reg.FindAllString(string(body), -1) {
		ff, _ := os.OpenFile(fname, os.O_RDWR, 0666)
		file, _ := ioutil.ReadAll(ff)
		dd := strings.Split(d, "")
		dddd := ""
		for _, ddd := range dd {
			if ddd == "?" {
				ddd = `\?`
			}
			dddd += ddd
		}
		if utils.CheckRegexp(string(file), dddd, 0).(string) == "" {
			io.WriteString(f, d+"\n")
			fmt.Print("\n收集地址：" + d + "\n")
			num++
		}
		// fmt.Print(string(file))
		ff.Close()
	}
	fmt.Print("\n首次收集网络地址：" + strconv.Itoa(len(reg.FindAllString(string(body), -1))) + "\n")
	reporter.ReportMessage("\n首次收集网络地址：" + strconv.Itoa(len(reg.FindAllString(string(body), -1))) + "\n\n首次储存成功！\n")
	fmt.Print("\n去重后网络地址数：" + strconv.Itoa(num))
	fmt.Print("\n\n首次储存成功！\n")
}
