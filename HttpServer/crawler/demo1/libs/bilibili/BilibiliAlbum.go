package bilibili

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// Resp 定义响应信息结构体
type Resp struct {
	Code    int
	Msg     string
	Message string
	Data    Data
}

// Data 数据
type Data struct {
	Items []Item
}

// Item Data子集
type Item struct {
	DocID       int
	PosterUID   int
	Title       string
	Description string
	Pictures    []Picture
	Count       int
	Ctime       int
	View        int
	Like        int
}

// Picture Info
type Picture struct {
	ImgSrc    string `json:"img_src"`
	ImgWidth  int    `json:"img_width"`
	ImgHeight int    `json:"img_height"`
	ImgSize   int    `json:"img_size"`
}

// Info uid url参数
type Info struct {
	UID      int
	PageNum  int
	PageSize int
	Biz      string
}

// Num 图片数量
type Num struct {
	Code    int
	Msg     string
	Message string
	Data    NumData
}

// NumData 图片信息
type NumData struct {
	AllCount   int `json:"all_count"`
	DrawCount  int `json:"draw_count"`
	PhotoCount int `json:"photo_count"`
	DailyCount int `json:"daily_count"`
}

// GetSrc 获取图片链接
func GetSrc(uid, num int) {
	index := 1
	pageNum := "0"
	biz := "all"

	// 判断文件夹是否存在
	ok, err := IsExists(strconv.Itoa(uid))
	if err != nil {
		panic("文件检查失败")
	}
	if !ok {
		err := os.Mkdir("data/"+strconv.Itoa(uid), os.ModePerm)
		if err != nil {
			panic("创建文件夹失败")
		}
	}

	// https://api.vc.bilibili.com/link_draw/v1/doc/doc_list?uid=1774758&page_num=0&page_size=142&biz=all
	url := "https://api.vc.bilibili.com/link_draw/v1/doc/doc_list?uid=" + strconv.Itoa(uid) + "&page_num=" + pageNum + "&page_size=" + strconv.Itoa(num) + "&biz=" + biz
	// resp, err := http.Get(url)
	// if err != nil {
	// 	panic("解析错误")
	// }

	// defer resp.Body.Close()

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}

	// 反爬措施，加上cookie
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36")
	req.Header.Set("cookie", "l=v; buvid3=F99999FE-F2D2-342A-1A5D-5CAEC0871B6C34926infoc; CURRENT_FNVAL=80; _uuid=18500941-01D3-CBB6-571D-5E7AD4C284B235624infoc; blackside_state=1; DedeUserID=32445825; DedeUserID__ckMd5=8558c901b465367d; SESSDATA=06b2678b%2C1622454099%2C6ac1d*c1; bili_jct=d07fb9c65e93f9463aca8ac53bf0410b; PVID=1; rpdid=|(um|ullkl|k0J'uY|Ym|JR)u; LIVE_BUVID=AUTO5416094039101842")

	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic("数据读取失败")
	}
	fmt.Printf("%s\n", body)

	res := new(Resp)
	if err := json.Unmarshal(body, &res); err == nil {
		// 获取所有items
		var items []Item
		items = append(items, res.Data.Items...)

		fmt.Println("num: ", len(items))

		for _, v := range items {
			//清除上次输出
			// clear.ClearCmd()
			//获取每一个picture
			fmt.Println("正在读取第" + strconv.Itoa(index) + "个相册,共" + strconv.Itoa(num) + "个相册")
			for k, v1 := range v.Pictures {
				//开始写入图片
				//获取图片的后缀,并更改名字重新保存
				dot := strings.LastIndex(v1.ImgSrc, ".")
				ext := v1.ImgSrc[dot:]
				//检测文件是否存在,如果存在则跳过,不存在则写入
				path := "data/" + strconv.Itoa(uid) + "/" + strconv.Itoa(index) + "-" + strconv.Itoa(k+1) + ext
				ok, err := IsExists(path)
				if err != nil {
					panic("文件检测错误 >_<!")
				}
				if !ok {
					//读取图片原始二进制数据
					//延迟防止封ip
					time.Sleep(time.Second * 1)
					bin, err := http.Get(v1.ImgSrc)
					if err != nil {
						panic("访问图片失败 >_<!")
					}
					defer bin.Body.Close()
					imgBin, err := ioutil.ReadAll(bin.Body)
					if err != nil {
						panic("读取图片数据失败 >_<!")
					}
					// f, err := os.OpenFile(strconv.Itoa(uid) + "/" + strconv.Itoa(index) + "-" + strconv.Itoa(k+1) + ext, os.O_CREATE, 0666)
					fmt.Println("path: ", path)
					f, err := os.OpenFile(path, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
					if err != nil {
						// panic("文件读取失败 >_<!")
						panic(err.Error())
					}
					defer f.Close()
					_, err = f.Write([]byte(imgBin))
					if err != nil {
						panic("文件写入失败 >_<!")
					}
					//清除前面的输出
					ClearCmd()

					fmt.Println("正在下载第" + strconv.Itoa(index) + "个相册的第" + strconv.Itoa(k+1) + "个图片,共" + strconv.Itoa(len(v.Pictures)) + "个图片")
				}
			}
			index++
		}
		fmt.Println("下载完成,马上退出 >_<~~")
		time.Sleep(time.Second * 1)
	} else {
		panic("相册详情解析失败了呢 >_<!")
	}

}

// GetImgNum 获取相册数量
func GetImgNum(uid int) int {
	url := "https://api.vc.bilibili.com/link_draw/v1/doc/upload_count?uid=" + strconv.Itoa(uid)
	num := new(Num)
	res, err := http.Get(url)
	if err != nil {
		fmt.Println("链接解析错误")
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("数据映射失败")
	}

	err = json.Unmarshal(body, &num)
	if err != nil {
		fmt.Println("相册数量解析错误")
	}

	imgNum := num.Data.AllCount
	if imgNum > 300 {
		fmt.Println("相册数量多")
	} else {
		fmt.Printf("共获取到该up主 %v 个相册 >_<~~\n", imgNum)
	}

	return imgNum
}

// IsExists 检测文件是否存在
func IsExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}

	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

// RunMain ...
func RunMain() {
	user := new(Info)

	for {
		fmt.Println("请输入b站up主的uid : ")
		_, err := fmt.Scanln(&user.UID)
		if err != nil {
			fmt.Println(err)
		}
		if user.UID == 0 || strconv.Itoa(user.UID) == " " {
			fmt.Println("uid不能为空或者为0")
		} else {
			num := GetImgNum(user.UID)
			if num != 0 {
				GetSrc(user.UID, num)
				break
			}
			fmt.Println("--------------------------")
			fmt.Println("该up主还没有上传相册哦 >_<~~")
			time.Sleep(time.Second * 1)
		}

	}
}
