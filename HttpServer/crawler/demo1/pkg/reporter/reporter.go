package reporter

import (
	"net/http"
)

const ftUrl = "https://sc.ftqq.com/SCU167763T37dfe625b1b97bfdb7a4365e2351d51f6066bb8fb7c0e.send"

func ReportError(title string, err error) {
	go func() {
		http.Get(ftUrl + "?text=" + title + "&desp=" + err.Error())
	}()
}

func ReportMessage(title string) {
	go func() {
		http.Get(ftUrl + "?text=" + title)
	}()
}
