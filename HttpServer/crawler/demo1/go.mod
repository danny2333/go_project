module httpserver/crawler

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/Unknwon/goconfig v1.0.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110
	golang.org/x/text v0.3.3
)
