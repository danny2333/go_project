package module

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"webget/schema"
	"webget/util"

	"github.com/PuerkitoBio/goquery"
)

// NewXLFM 荔枝FM下载链接生成器
func NewLZFM(client *util.Client) schema.Worker {
	return &LZFM{
		client: client,
		option: &schema.Option{Cli: true, Web: false, Task: false, Increment: false},
	}
}

// LZFM 荔枝 FM 专辑声音下载链接生成器
type LZFM struct {
	client *util.Client
	option *schema.Option
}

// Intro 显示抓取器帮助
func (s *LZFM) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "荔枝 FM"
	case "cliHelp":
		tip = "荔枝FM的音频抓取"
	}

	return tip
}

// Options 抓取选项
func (s *LZFM) Options() *schema.Option {
	return s.option
}

// Task 后台任务
func (s *LZFM) Task() error {
	return nil
}

// List 列出已经缓存的资源
func (s *LZFM) List() []map[string]string {
	return nil
}

// Search 缓存搜索
func (s *LZFM) Search(keyword string) []map[string]string {
	return nil
}

// Web 模块 web 入口, 返回 true 表示已经准备就绪
func (s *LZFM) Web(w http.ResponseWriter, req *http.Request, buf *bytes.Buffer) bool {
	return false
}

// Do 提取内容
func (s *LZFM) Do(tryModel bool, entry string, rule string, fp *os.File) error {
	var err error
	if "" == entry {
		if tryModel {
			entry = "http://www.lizhi.fm/user/2544758401649219116"
		} else {
			return errors.New("请输入要抓取的声音专辑入口网址")
		}
	}

	if strings.Index(entry, "?") > 0 || !strings.HasPrefix(entry, "http://www.lizhi.fm/user/") {
		return errors.New("声音专辑网址格式不对，正确的格式如：http://www.lizhi.fm/user/2544758401649219116")
	}

	// 直接下载
	/* var cnt int
	var trackID = s.getItemID(entry)
	if len(trackID) > 0 {
		for _, item := range trackID {
			if err = s.client.Download(item[1], item[0], true); nil != err {
				cnt++
			}
		}
	}

	if cnt > 0 && nil == err {
		err = errors.New("下载失败了 " + strconv.FormatInt(int64(cnt), 10) + " 个文件")
	} */

	// 保存到json
	var trackID = s.getItemID(entry)
	if len(trackID) > 0 {
		var data = map[string]interface{}{
			"ts":   time.Now().Unix(),
			"data": trackID,
		}

		var bj, _ = json.Marshal(data)
		util.FilePutContents("data/lzfm.json", bj, false)
	}

	return err
}

// getItemID 读取专辑声音列表
func (s *LZFM) getItemID(entry string) [][]string {
	var idx int64 = 1
	var url string
	var ret = make([][]string, 0)

	for {
		if 1 == idx {
			url = entry
		} else {
			// url = entry + "/p/" + strconv.FormatInt(idx, 10) + ".html"
			url = fmt.Sprintf(entry+"/p/%d.html", idx)
		}

		// 太多了 就50页了
		if idx == 50 {
			break
		}

		doc, err := s.client.GetDoc(url, nil)
		if err != nil {
			continue
		}

		// 判断下一页内容是否存在
		_, exists := doc.Find("div.wrap div.frame ul.audioList li").Eq(0).Find("a.js-play-data.audio-list-item").Attr("href")
		if !exists {
			break
		}

		// 提取提取声音标题与ID
		doc.Find("div.wrap div.frame ul.audioList li a.js-play-data.audio-list-item").Each(func(i int, s *goquery.Selection) {
			title, _ := s.Attr("title")
			href, _ := s.Attr("href")
			href = fmt.Sprintf("%s%s", "https://www.lizhi.fm", href)
			if "" != title && "" != href {
				log.Println("成功提取链接：", href)
				ret = append(ret, []string{title, href})
			}
		})

		log.Printf("抓取%s\n", url)
		idx++
	}

	return ret
}
