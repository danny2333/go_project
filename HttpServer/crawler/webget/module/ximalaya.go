package module

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"webget/schema"
	"webget/util"
)

// NewXimalayaAlbum 喜马拉雅 FM 专辑声音下载链接生成器
func NewXimalayaAlbum(client *util.Client) schema.Worker {
	return &XimalayaAlbum{
		client: client,
		option: &schema.Option{Cli: true, Web: false, Task: false, Increment: true},
	}
}

// XimalayaAlbum 喜马拉雅 FM 专辑声音下载链接生成器
type XimalayaAlbum struct {
	client *util.Client
	option *schema.Option
}

// Intro 显示抓取器帮助
func (s *XimalayaAlbum) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "喜马拉雅 FM"
	}

	return tip
}

// Options 抓取选项
func (s *XimalayaAlbum) Options() *schema.Option {
	return s.option
}

// Task 后台任务
func (s *XimalayaAlbum) Task() error {
	return nil
}

// List 列出已经缓存的资源
func (s *XimalayaAlbum) List() []map[string]string {
	return nil
}

// Search 缓存搜索
func (s *XimalayaAlbum) Search(keyword string) []map[string]string {
	return nil
}

// Web 模块 web 入口, 返回 true 表示已经准备就绪
func (s *XimalayaAlbum) Web(w http.ResponseWriter, req *http.Request, buf *bytes.Buffer) bool {
	return false
}

// Do 提取内容
func (s *XimalayaAlbum) Do(tryModel bool, entry string, rule string, fp *os.File) error {
	if "" == entry {
		if tryModel {
			entry = "http://www.ximalaya.com/1000202/album/2667276/"
		} else {
			return errors.New("请输入要抓取的声音专辑入口网址")
		}
	}

	if strings.Index(entry, "?") > 0 || !strings.HasSuffix(entry, "/") {
		return errors.New("声音专辑网址格式不对，正确的格式如：http://www.ximalaya.com/1000202/album/2667276/")
	}

	var cnt int
	var url string
	var flag bool
	var files = util.GetDirFiles("./", true)
	var trackID, err = s.getItemID(entry)

	if nil == err && len(trackID) > 0 {
		for _, item := range trackID {
			flag = false

			for _, file := range files {
				if file == item[0] {
					flag = true
				}
			}

			if !flag {
				if url, err = s.getItemURL(item[1]); nil == err && "" != url {
					if err = s.client.Download(url, item[0], true); nil != err {
						cnt++
					}
					fmt.Printf("正在下载 %s\n", item[0])
				}
			}

		}
	}

	if cnt > 0 && nil == err {
		err = errors.New("下载失败了 " + strconv.FormatInt(int64(cnt), 10) + " 个文件")
	}

	return err
}

// getItemURL 获取声音 ID 对应的 URL
func (s *XimalayaAlbum) getItemURL(id string) (string, error) {
	var out = make(map[string]interface{})
	var url = "http://www.ximalaya.com/tracks/" + id + ".json"

	var err = s.client.GetCodec(url, nil, "json", &out)
	if nil == err && nil != out {
		if uri, ok := out["play_path"].(string); ok && "" != uri {
			return uri, nil
		}

		return "", errors.New("声音 " + url + " 的下载网址为空，是收费内容？")
	}

	return "", err
}

// getItemID 读取专辑声音列表
func (s *XimalayaAlbum) getItemID(entry string) ([][]string, error) {
	var idx int64 = 1
	var aurl string = "https://www.ximalaya.com/revision/album/v1/getTracksList?albumId=2667276&pageNum=%d&sort=1"
	var burl string
	var data []byte
	var ret [][]string
	var err error

	for {
		if 1 == idx {
			burl = fmt.Sprintf(aurl, 1)
		} else {
			burl = fmt.Sprintf(aurl, idx)
		}

		if idx == 2 {
			break
		}

		payload := &util.ClientPayload{}
		payload.Header = &http.Header{
			"User-Agent": []string{"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0"},
			"Referer":    []string{"https://www.ximalaya.com/album/2667276?page=" + strconv.FormatInt(idx-1, 10)},
		}

		if data, _, err = s.client.GetByte(burl, payload); nil == err && nil != data {
			var root = make(map[string]interface{})
			if err = json.Unmarshal(data, &root); nil == err {
				if tracks, ok := root["data"].(map[string]interface{})["tracks"].([]interface{}); ok && len(tracks) > 0 {
					ret = make([][]string, len(tracks))

					for k, v := range tracks {
						if track, ok := v.(map[string]interface{}); ok {
							if title, ok := track["title"].(string); ok && "" != title {
								if id, ok := track["trackId"]; ok {
									ret[k] = []string{title, s.toString(id), track["url"].(string)}
								}
							}
						}
					}
				} else {
					break
				}
			} else {
				continue
			}
		} else {
			continue
		}

		log.Printf("抓取%s\n", burl)
		idx++
	}

	return ret, err
}

func (s *XimalayaAlbum) toString(in interface{}) string {
	var ret string

	if v, ok := in.(string); ok {
		ret = v
	} else if v, ok := in.(uint64); ok {
		ret = strconv.FormatUint(v, 10)
	} else if v, ok := in.(int64); ok {
		ret = strconv.FormatInt(v, 10)
	} else if v, ok := in.(float64); ok {
		ret = strconv.FormatFloat(v, 'f', 0, 64)
	} else if v, ok := in.(float32); ok {
		ret = strconv.FormatFloat(float64(v), 'f', 0, 32)
	}

	return ret
}
