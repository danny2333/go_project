package main

import (
	"china_repos/github"
	"china_repos/worker"
	"flag"
	"fmt"
	"os"
)

var (
	cookieFile, language string
	minStars             int
)

func main() {
	// default flag== -help
	flag.StringVar(&cookieFile, "cookie", "./cookie", "github cookie file path.")
	flag.StringVar(&language, "l", "Go", "the language you want to search.")
	flag.IntVar(&minStars, "stars", 100, "minimum stars of the repos")
	flag.Parse()

	client := github.NewClient()

	client.Msg("王尼玛")
	fmt.Printf("%#v", client)
	os.Exit(0)
	client.LoadCookie(cookieFile)

	worker.New(
		language,
		minStars,
		client,
	).Run()
}
