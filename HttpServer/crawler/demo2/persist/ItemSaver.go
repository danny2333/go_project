package persist

import (
	"context"
	"log"
	"xiaoshuo/engine"

	"github.com/olivere/elastic"
)

func ItemSaver(index string) (chan engine.Profile, error) {
	// client, err := elastic.NewClient()
	client, err := elastic.NewClient(elastic.SetURL("http://127.0.0.1:9200"), elastic.SetSniff(false))
	if err != nil {
		return nil, err
	}
	out := make(chan engine.Profile)
	go func() {
		count := 0
		for {
			item := <-out
			log.Printf("Item Data %v", item)
			_, err := save(client, item, index)
			if err != nil {
				log.Printf("Item Saver: error saving item %v: %v", item, err)
			}
			count++
		}
	}()

	return out, nil
}

func save(client *elastic.Client, item engine.Profile, index string) (id string, err error) {
	resp, err := client.Index().Index(index).Type(item.Type).Id(item.Id).BodyJson(item).Do(context.Background())
	if err != nil {
		return "", err
	}
	return resp.Id, nil
}
