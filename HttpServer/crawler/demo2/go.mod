module xiaoshuo

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.7.1
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/olivere/elastic v6.2.37+incompatible
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/net v0.0.0-20210726213435-c6fcb2dbf985
	golang.org/x/text v0.3.6
)
