package main

import (
	"encoding/json"
	"log"
	"os"
	"runtime"

	"gowebapp/app/route"
	"gowebapp/app/shared/database"
	"gowebapp/app/shared/email"
	"gowebapp/app/shared/jsonconfig"
	"gowebapp/app/shared/recaptcha"
	"gowebapp/app/shared/server"
	"gowebapp/app/shared/session"
	"gowebapp/app/shared/view"
	"gowebapp/app/shared/view/plugin"
)

// *****************************************************************************
// 应用逻辑
// *****************************************************************************

func init() {
	// 带有文件名和行号的粗略日志记录
	log.SetFlags(log.Lshortfile)

	// 使用所有的CPU核心
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	// 加载配置文件
	jsonconfig.Load("config"+string(os.PathSeparator)+"config.json", config)

	// 配置会话cookie存储
	session.Configure(config.Session)

	// 连接数据库
	database.Connect(config.Database)

	// 在加载视图插件之前配置谷歌reCAPTCHA
	recaptcha.Configure(config.Recaptcha)

	// 设置视图
	view.Configure(config.View)
	view.LoadTemplates(config.Template.Root, config.Template.Children)
	view.LoadPlugins(
		plugin.TagHelper(config.View),
		plugin.NoEscape(),
		plugin.PrettyTime(),
		recaptcha.Plugin(),
	)

	// 启动服务
	server.Run(route.LoadHTTP(), route.LoadHTTPS(), config.Server)
}

// *****************************************************************************
// 应用设置
// *****************************************************************************

// config 配置设置变量
var config = &configuration{}

// configuration 配置包含应用程序的设置
type configuration struct {
	Database  database.Info   `json:"Database"`
	Email     email.SMTPInfo  `json:"Email"`
	Recaptcha recaptcha.Info  `json:"Recaptcha"`
	Server    server.Server   `json:"Server"`
	Session   session.Session `json:"Session"`
	Template  view.Template   `json:"Template"`
	View      view.View       `json:"View"`
}

// ParseJSON 将字节解读为结构体
func (c *configuration) ParseJSON(b []byte) error {
	return json.Unmarshal(b, &c)
}
