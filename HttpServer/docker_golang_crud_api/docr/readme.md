## Golang Docker 

### 构建镜像
```
docker build -t server .
```

### 运行镜像
```
docker run -p 9900:9900 server
```