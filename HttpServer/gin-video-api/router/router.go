package router

import (
	"gin-video-api/controller"
	"gin-video-api/service"
	"gin-video-api/middleware"
	"net/http"

	"github.com/gin-gonic/gin"
	"log"
)

var (
	videoService    service.VideoService       = service.New()
	videoController controller.VideoController = controller.New(videoService)
)

// InitRouter init
func InitRouter(r *gin.Engine) {
	r.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Welcome Gin Server")
	})

	r.Use(middleware.StatCost())

	r.GET("/videos", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, videoController.FindAll())
	})

	r.POST("/video", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, videoController.Create(ctx))
	})

	// PATCH 只更新某个字段 PUT全部
	r.PATCH("/video", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, videoController.Update(ctx))
	})

	v1 := r.Group("/v1")
	{
		v1.GET("/test1", func(ctx *gin.Context) {
			example := ctx.MustGet("example").(string)
			log.Println("gin set example val: ", example)
			ctx.JSON(http.StatusOK, gin.H{
				"message": "success",
			})
		})
	}

}
