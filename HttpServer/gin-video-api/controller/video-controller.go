package controller

import (
	"gin-video-api/entity"
	"gin-video-api/service"

	"github.com/gin-gonic/gin"
)

// VideoController interdace
type VideoController interface {
	FindAll() []entity.Video
	Create(ctx *gin.Context) entity.Video
	Update(ctx *gin.Context) entity.Video
}

// controller .
type controller struct {
	service service.VideoService
}

// New .
func New(service service.VideoService) VideoController {
	return &controller{
		service: service,
	}
}

// FindAll .
func (c *controller) FindAll() []entity.Video {
	return c.service.FindAll()
}

// Create .
func (c *controller) Create(ctx *gin.Context) entity.Video {
	var video entity.Video
	ctx.BindJSON(&video)

	return c.service.Create(video)
}

// Update .
func (c *controller) Update(ctx *gin.Context) entity.Video {
	var video entity.Video
	ctx.BindJSON(&video)

	return c.service.Update(video)
}
