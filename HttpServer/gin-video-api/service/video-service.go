package service

import (
	"gin-video-api/entity"
	"gin-video-api/utils"
)

// VideoService interface
type VideoService interface {
	Create(entity.Video) entity.Video
	Update(entity.Video) entity.Video
	FindAll() []entity.Video
}

// videoService struct 所有数据组
type videoService struct {
	videos []entity.Video
}

// New .
func New() VideoService {
	return &videoService{
		videos: []entity.Video{},
	}
}

// Create .
func (service *videoService) Create(video entity.Video) entity.Video {
	// 判断title是否存在，有就返回
	for _, v := range service.videos {
		if v.Title == video.Title {
			return v
		}
	}
	tmpID := utils.UniqueID()
	if tmpID == "" {
		return entity.Video{}
	}

	video.ID = tmpID
	service.videos = append(service.videos, video)

	return video
}

// Update .
func (service *videoService) Update(video entity.Video) entity.Video {
	// 判断title是否存在，有就返回
	// 采用range  item.value方式不能修改数组结构体中的值
	// for _, v := range service.videos {
	for i := 0; i < len(service.videos); i++ {
		if service.videos[i].ID == video.ID {
			service.videos[i].Title = video.Title
			service.videos[i].Description = video.Description
			service.videos[i].URL = video.URL
			return service.videos[i]
		}
	}

	return entity.Video{}
}

// FindAll .
func (service *videoService) FindAll() []entity.Video {
	return service.videos
}
