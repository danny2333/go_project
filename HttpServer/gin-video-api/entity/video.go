package entity

// Video .
type Video struct {
	// ID          string `json:"id,omitempty"`
	ID          string `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	URL         string `json:"url"`
}
