module httpserver/cron/demo2

go 1.15

require (
	github.com/gin-gonic/gin v1.7.1
	github.com/gorilla/websocket v1.4.2
	gopkg.in/robfig/cron.v2 v2.0.0-20150107220207-be2e0b0deed5
)
