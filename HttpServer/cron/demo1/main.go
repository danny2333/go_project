package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	// "github.com/robfig/cron"
	"github.com/tidwall/gjson"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
)

var (
	APPID          = "wx94464261ae67004a"
	APPSECRET      = "a40ff611d145fa88f288e873523a9cff"
	SentTemplateID = "C1ehCWvnl_zDoxZz3Sesv_VRTv0lDUMtWgQdGrWCStc" //每日一句的模板ID
	WeatTemplateID = "DhDqFMfVKIiw8hyCrX3KN94x_tgaobOl8h5b6hyDYQU" //天气模板ID
	CityID         *string
	UserName       *string
	Password       *string
)

type token struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int    `json:"expires_in"`
}

// sentence 每日一句
type sentence struct {
	Content     string `json:"content"`
	Note        string `json:"note"`
	Translation string `json:"translation"`
}

func main() {
	CityID = flag.String("CityID", "101280601", "请输入城市代码，如北京：101010100")
	UserName = flag.String("u", "015899", "请输入登录名")
	Password = flag.String("p", "Dc@015899", "请输入密码")
	// 所有标志都声明完成以后，调用 `flag.Parse()` 来执行命令行解析。
	flag.Parse()
	// CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -i -tags netgo -v -ldflags="-s -w" -o gotest01
	// gitbash
	// env GOOS=linux go build -o gotest01
	// cmd
	// set GOARCH=amd64
	// set GOOS=linux
	// go build -o gotest01 main.go
	// chmod -R 777 gotest01
	// nohup ./gotest01 &
	// ps -ef|grep gotest01
	// kill -9 324
	// 秒 分 时 日 月 周

	// 每日一句
	/* spec := "0 24 10 * * *" // 每天10:24
	// 每日天气
	spec1 := "0 0 7 * * *" // 每天早晨7:00
	c := cron.New()
	c.AddFunc(spec, everydaysen)
	c.AddFunc(spec1, weather)
	c.Start()
	fmt.Println("开启定时任务")
	select {} */

	everydaysen()
	// weather()
	// bbsLogin(UserName, Password)
}

func GbkToUtf8(s []byte) ([]byte, error) {
	reader := transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewDecoder())
	d, e := ioutil.ReadAll(reader)
	if e != nil {
		return nil, e
	}
	return d, nil
}

func Utf8ToGbk(s []byte) ([]byte, error) {
	reader := transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewEncoder())
	d, e := ioutil.ReadAll(reader)
	if e != nil {
		return nil, e
	}
	return d, nil
}

// bbsLogin 论坛登录
func bbsLogin(username, password *string) {
	LOGIN_URL := "http://202.105.146.234:10220/discuz/member.php?mod=logging&action=login&loginsubmit=yes&loginhash=LDoj3"
	if *username == "" || *password == "" {
		return
	}

	userData := make(map[string]interface{})
	userData["loginfield"] = "username"
	userData["username"] = *username
	userData["password"] = *password
	userData["referer"] = "http://202.105.146.234:10220/discuz/./"
	userData["loginsubmit"] = true
	userData["formhash"] = "141ec9d2"
	bytesData, err := json.Marshal(userData)
	if err != nil {
		fmt.Println(err.Error())
	}
	reader := bytes.NewReader(bytesData)
	time.Sleep(time.Second * 1)
	request, err := http.NewRequest("POST", LOGIN_URL, reader)
	if err != nil {
		log.Println(err.Error())
	}
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Set("Referer", "http://202.105.146.234:10220/discuz/erpmember.php?mod=logging&&action=login")
	request.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36")
	client := http.Client{}
	// 超时时长 30s
	client.Timeout = 30 * 1000 * 1000 * 1000
	resp, err := client.Do(request)
	if err != nil {
		log.Println(err.Error())
	}
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err.Error())
	}

	utf8, err := GbkToUtf8(respBytes)
	if err != nil {
		log.Println(err)
	} else {
		WriterStr(string(utf8))
	}

}

// WriterStr 写入文件
func WriterStr(str string) {
	filePath := "./output.html"
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("打开文件错误", err.Error())
		return
	}

	defer file.Close()

	writer := bufio.NewWriter(file)
	writer.WriteString(str)

	writer.Flush()
}

// everydaysen 发送每日一句
func everydaysen() {
	bbsLogin(UserName, Password)
	req, fxurl := getsen()
	if req.Content == "" {
		return
	}
	access_token := getaccesstoken()
	if access_token == "" {
		return
	}

	flist := getflist(access_token)
	if flist == nil {
		return
	}

	reqdata := "{\"content\":{\"value\":\"" + req.Content + "\", \"color\":\"#0000CD\"}, \"note\":{\"value\":\"" + req.Note + "\"}}"
	for _, v := range flist {
		templatepost(access_token, reqdata, fxurl, SentTemplateID, v.Str)
	}
}

// weather 发送天气预报
func weather() {
	access_token := getaccesstoken()
	if access_token == "" {
		return
	}

	flist := getflist(access_token)
	if flist == nil {
		return
	}

	for _, v := range flist {
		sendweather(access_token, v.Str)
		// switch v.Str {
		// case "oeZ6P5kyGsLKn3sIGRVfpb8oT4mg":
		// 	city = "青岛"
		// 	go sendweather(access_token, city, v.Str)
		// case "oeZ6P5jvFNh2y_h_2UcaoTXBaC2o":
		// 	city = "西安"
		// 	go sendweather(access_token, city, v.Str)
		// default:

		// }
	}
	fmt.Println("weather is ok")
}

// getweather 获取天气
func getweather() (string, string, string, string) {
	url := fmt.Sprintf("http://www.weather.com.cn/data/cityinfo/%s.html", *CityID)
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("获取天气失败", err)
		return "", "", "", ""
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("读取内容失败", err)
		return "", "", "", ""
	}

	day := Lunar(time.Now().Format("20060102"))
	wea := gjson.Get(string(body), "weatherinfo.weather").Str
	// tem := gjson.Get(string(body), "weatherinfo.ptime").Str
	tem := fmt.Sprintf("最低温度：%s，最高温度：%s", gjson.Get(string(body), "weatherinfo.temp1").Str, gjson.Get(string(body), "weatherinfo.temp2").Str)
	city := gjson.Get(string(body), "weatherinfo.city").Str
	return day, wea, tem, city
}

// sendweather 发送天气
func sendweather(access_token, openid string) {
	day, wea, tem, city := getweather()
	if day == "" || wea == "" || tem == "" || city == "" {
		return
	}
	reqdata := "{\"city\":{\"value\":\"城市：" + city + "\", \"color\":\"#0000CD\"}, \"day\":{\"value\":\"" + day + "\"}, \"wea\":{\"value\":\"天气：" + wea + "\"}, \"tem1\":{\"value\":\"" + tem + "\"}}"
	//fmt.Println(reqdata)
	templatepost(access_token, reqdata, "http://wufazhuce.com", WeatTemplateID, openid)
}

// getaccesstoken 获取微信accesstoken
func getaccesstoken() string {
	url := fmt.Sprintf("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%v&secret=%v", APPID, APPSECRET)
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("获取微信token失败", err)
		return ""
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("微信token读取失败", err)
		return ""
	}

	token := token{}
	err = json.Unmarshal(body, &token)
	if err != nil {
		fmt.Println("微信token解析json失败", err)
		return ""
	}

	return token.AccessToken
}

// getsen 获取每日一句
func getsen() (sentence, string) {
	resp, err := http.Get("http://open.iciba.com/dsapi/?date")
	sent := sentence{}
	if err != nil {
		fmt.Println("获取每日一句失败", err)
		return sent, ""
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("读取内容失败", err)
		return sent, ""
	}

	err = json.Unmarshal(body, &sent)
	if err != nil {
		fmt.Println("每日一句解析json失败")
		return sent, ""
	}
	fenxiangurl := gjson.Get(string(body), "fenxiang_img").String()
	fmt.Println(sent)
	return sent, fenxiangurl
}

// getflist 获取关注者列表
func getflist(access_token string) []gjson.Result {
	url := "https://api.weixin.qq.com/cgi-bin/user/get?access_token=" + access_token + "&next_openid="
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("获取关注列表失败", err)
		return nil
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("读取内容失败", err)
		return nil
	}
	flist := gjson.Get(string(body), "data.openid").Array()
	return flist
}

// templatepost 发送模板消息
func templatepost(access_token string, reqdata string, fxurl string, templateid string, openid string) {
	url := "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token

	reqbody := "{\"touser\":\"" + openid + "\", \"template_id\":\"" + templateid + "\", \"url\":\"" + fxurl + "\", \"data\": " + reqdata + "}"

	resp, err := http.Post(url,
		"application/x-www-form-urlencoded",
		strings.NewReader(string(reqbody)))
	if err != nil {
		fmt.Println(err)
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(body))
}
