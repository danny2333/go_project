module httpserver/cron

go 1.15

require (
	github.com/robfig/cron v1.2.0
	github.com/tidwall/gjson v1.7.4
	golang.org/x/text v0.3.6
)
