package danny

import (
	"bytes"
	"danny/app/pkg/common"
	"danny/app/pkg/logg"
	"danny/app/pkg/weixin"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/tidwall/gjson"
)

type Dconfig struct {
	UserName  string
	Password  string
	SuperName string
	SuperPass string
	CityID    string
	Data      []string
	client    http.Client
	cookie    []*http.Cookie
}

func Init() *Dconfig {
	return &Dconfig{
		CityID:    os.Getenv("CityID"),
		UserName:  os.Getenv("UserName"),
		Password:  os.Getenv("Password"),
		SuperName: os.Getenv("SuperName"),
		SuperPass: os.Getenv("SuperPass"),
	}
}

func (d *Dconfig) GetDconfig() {
	fmt.Printf("%#v", d)
}

func (d *Dconfig) ClearData() {
	d.Data = []string{}
}

// LoginBBS 论坛登录 new
func (d *Dconfig) LoginBBS() {
	if d.UserName == "" {
		logg.Danger("LoginBBS:用户名空")
		return
	}
	// time := time.Now().Format("2006-01-02 15:04:05")
	sEnc := b64.StdEncoding.EncodeToString([]byte(d.UserName + time.Now().Format("20060102")))
	// sDec, _ := b64.StdEncoding.DecodeString(sEnc)

	url := "http://bderp.ser.ltd:10220/discuz/member.php?mod=logging&action=login&loginsubmit=yes&loginhash=LQuP4&type=oalogin&areaid=35&username=" + sEnc
	// resp, err := http.Get(url)
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	resp, err := d.client.Do(req)
	if err != nil {
		logg.Danger("LoginBBS: HTTP Get Error", err)
		return
	}
	d.cookie = resp.Cookies()
	defer resp.Body.Close()
	logg.Info("LoginBBS:Login Success")

	time.Sleep(time.Second * 6)

	req, _ = http.NewRequest(http.MethodGet, "http://bderp.ser.ltd:10220/discuz/plugin.php?id=knowledge_sharing:hot", nil)
	for i := range d.cookie {
		req.AddCookie(d.cookie[i])
	}
	resp, err = d.client.Do(req)
	if err != nil {
		logg.Danger("LoginBBS: HTTP Get View Error", err)
		return
	}
	// bufBody := bufio.NewReader(resp.Body)
	// utf8Reader := transform.NewReader(bufBody, fetcher.DetermineEncoding(bufBody).NewDecoder())
	// body, err := ioutil.ReadAll(utf8Reader)
	// common.WriterStr(string(body), fmt.Sprintf("%v", time.Now().Unix()))
	logg.Info("LoginBBS:View Success")
}

// BbsLogin 论坛登录 old
func (d *Dconfig) BbsLogin() {
	LOGIN_URL := "http://bderp.ser.ltd:10220/discuz/member.php?mod=logging&action=login&loginsubmit=yes&loginhash=LDoj3"
	if d.UserName == "" || d.Password == "" {
		logg.Danger("BbsLogin:username or password is empty")
		return
	}

	form := url.Values{}
	form.Set("formhash", "141ec9d2")
	form.Set("referer", "http://bderp.ser.ltd:10220/discuz/./")
	form.Set("loginfield", "username")
	form.Set("username", d.UserName)
	form.Set("password", d.Password)
	form.Set("loginsubmit", "true")

	time.Sleep(time.Second * 1)

	request, err := http.NewRequest("POST", LOGIN_URL, strings.NewReader(form.Encode()))
	if err != nil {
		logg.Danger("BbsLogin:NewRequest", err)
	}
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Add("Referer", "http://bderp.ser.ltd:10220/discuz/erpmember.php?mod=logging&&action=login")
	request.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36")
	request.Header.Add("Cookie", "td_cookie=487908204; vH1Z_2132_saltkey=SIGgHBif; vH1Z_2132_lastvisit=1625024239; vH1Z_2132_home_diymode=1; vH1Z_2132_sid=Hqfczh; vH1Z_2132_sendmail=1; vH1Z_2132_lastact=1625031525%09erpmember.php%09logging")

	client := http.Client{}
	// 超时时长 30s
	client.Timeout = 30 * 1000 * 1000 * 1000
	resp, err := client.Do(request)
	if err != nil {
		logg.Danger("BbsLogin:client.Do", err)
	}

	defer resp.Body.Close()
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logg.Danger("BbsLogin:ReadAll", err)
	}

	// utf8, err := GbkToUtf8(respBytes)
	_, err = common.GbkToUtf8(respBytes)
	if err != nil {
		logg.Danger("BbsLogin:GbkToUtf8", err)
	} else {
		// WriterStr(string(utf8))
		logg.Info("BbsLogin:Forum Login Success!", err)
	}

}

// OaSsoSwitch 切换账号
func (d *Dconfig) OaSsoSwitch(username, userid string) {
	LOGIN_URL := "https://api.ser.ltd:51443/sso/switch"

	userData := make(map[string]interface{})
	userData["username"] = "004532"
	userData["userId"] = 15673
	bytesData, err := json.Marshal(userData)
	if err != nil {
		logg.Danger("OaSsoSwitch:Marshal", err)
		return
	}
	reader := bytes.NewReader(bytesData)
	time.Sleep(time.Second * 1)
	request, err := http.NewRequest("POST", LOGIN_URL, reader)
	if err != nil {
		logg.Danger("OaSsoSwitch:NewRequest", err)
		return
	}
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("userName", username)
	request.Header.Set("userId", userid)
	request.Header.Set("Referer", "https://oa.ser.ltd/")
	request.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36")
	client := http.Client{}
	// 超时时长 30s
	client.Timeout = 30 * 1000 * 1000 * 1000
	resp, err := client.Do(request)
	if err != nil {
		logg.Danger("OaSsoSwitch:client.Do", err)
		return
	}
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logg.Danger("OaSsoSwitch:ReadAll", err)
		return
	}

	result := make(map[string]interface{})
	err = json.Unmarshal([]byte(respBytes), &result)
	if err != nil {
		logg.Danger("OaSsoSwitch:Unmarshal", err)
		return
	}

	if result["code"].(float64) == 200 {
		// 登录成功 调用其它方法
		logg.Info("OaSsoSwitch: Swtict Success", result["data"].(map[string]interface{})["username"])
		// 如果按多工号搜索 在此处循环关注数组逐一获取
		d.GetOverTime(result["data"].(map[string]interface{})["username"].(string), result["data"].(map[string]interface{})["userId"].(string))
		return
	} else {
		logg.Danger("OaLogin:result.code!==200")
		return
	}
}

// OaLogin 登录
func (d *Dconfig) OaLogin() {
	username := d.SuperName
	password := d.SuperPass
	LOGIN_URL := "https://api.ser.ltd:51443/login/"
	if username == "" || password == "" {
		logg.Danger("OaLogin:用户名和密码空")
		return
	}

	userData := make(map[string]interface{})
	userData["username"] = username
	userData["password"] = password
	userData["verifycode"] = "-1111111111"
	userData["loginChecked"] = true
	userData["loginType"] = 1
	userData["type"] = -4
	userData["mac"] = "E0-D5-5E-5E-8A-96"
	userData["disk"] = "23AA9C9DB4BD71345C8B9F167637277D"
	bytesData, err := json.Marshal(userData)
	if err != nil {
		logg.Danger("OaLogin:Marshal", err)
		return
	}
	reader := bytes.NewReader(bytesData)
	time.Sleep(time.Second * 1)
	request, err := http.NewRequest("POST", LOGIN_URL, reader)
	if err != nil {
		logg.Danger("OaLogin:NewRequest", err)
		return
	}
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Referer", "https://oa.ser.ltd/")
	request.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36")
	client := http.Client{}
	// 超时时长 30s
	client.Timeout = 30 * 1000 * 1000 * 1000
	resp, err := client.Do(request)
	if err != nil {
		logg.Danger("OaLogin:client.Do", err)
		return
	}
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logg.Danger("OaLogin:ReadAll", err)
		return
	}

	result := make(map[string]interface{})
	err = json.Unmarshal([]byte(respBytes), &result)
	if err != nil {
		logg.Danger("OaLogin:Unmarshal", err)
		return
	}

	if result["code"].(float64) == 200 {
		// 登录成功 调用其它方法
		logg.Info("Login OA Success", result["data"].(map[string]interface{})["username"])
		// d.GetOverTime(result["data"].(map[string]interface{})["username"].(string), result["data"].(map[string]interface{})["userId"].(string))
		d.OaSsoSwitch(result["data"].(map[string]interface{})["username"].(string), result["data"].(map[string]interface{})["userId"].(string))
		return
	} else {
		logg.Danger("OaLogin:result.code!==200")
		return
	}
}

// GetOverTime 获取值班时长
func (d *Dconfig) GetOverTime(userName, userID string) {
	url := "https://api.ser.ltd:51443/dutytime/summary/list"

	yesterday := time.Now().Add(-24 * time.Hour)
	workingDate := common.GetFirstDateOfMonth(yesterday).Format("2006-01-02")
	endTime := common.GetLastDateOfMonth(yesterday).Format("2006-01-02")
	formData := map[string]interface{}{
		"query": map[string]string{
			"jobNumber":   d.UserName,
			"workingDate": workingDate,
			"endTime":     endTime,
		},
		"page": map[string]int{
			"pageNo":   1,
			"pageSize": 10,
		},
	}
	logg.Info("GetOverTime:查询时间段: ", workingDate, "-", endTime)

	bytesData, err := json.Marshal(formData)
	if err != nil {
		logg.Danger("GetOverTime:Marshal", err)
		return
	}
	reader := bytes.NewReader(bytesData)
	time.Sleep(time.Second * 1)
	request, err := http.NewRequest("POST", url, reader)
	if err != nil {
		logg.Danger("GetOverTime:http.NewRequest", err)
		return
	}
	// header.Set与header.Add区别
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("userName", userName)
	request.Header.Set("userId", userID)
	request.Header.Set("Host", "api.ser.ltd:51443")
	request.Header.Set("Origin", "https://oa.ser.ltd")
	request.Header.Set("Referer", "https://oa.ser.ltd/")
	request.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36")

	// request.Header.Add("Content-Type", "application/json;charset=UTF-8")
	client := http.Client{}
	// 超时时长 30s
	client.Timeout = 30 * 1000 * 1000 * 1000
	resp, err := client.Do(request)

	if err != nil {
		logg.Danger("GetOverTime:client.Do", err)
		return
	}
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logg.Danger("GetOverTime:ReadAll", err)
		return
	}

	result := make(map[string]interface{})
	err = json.Unmarshal([]byte(respBytes), &result)
	if err != nil {
		logg.Danger("GetOverTime:Unmarshal", err)
		return
	}

	if result["code"].(float64) == 200 {
		// 获取成功 判断是否没数据
		dutyTimes := result["data"].(map[string]interface{})["dutyTimes"].([]interface{})
		if len(dutyTimes) == 0 {
			logg.Danger("GetOverTime:get dutyTimes is 0", err)
			return
		}
		var total float64 = 40
		nowOverTime := dutyTimes[0].(map[string]interface{})["overtime"].(float64)
		otime := fmt.Sprintf("本月值班时长: %.2f", nowOverTime)
		d.Data = append(d.Data, otime)
		if nowOverTime < total {
			d.Data = append(d.Data, fmt.Sprintf("本月还需%.2f小时，顽张って！", total-nowOverTime))
		}
		logg.Info("GetOverTime:Success! 值班时长/时: ", otime)
	} else {
		logg.Danger("GetOverTime:result!==200", err)
		return
	}
}

// GetWeatherData ...
func (d *Dconfig) GetWeatherData() {
	url := fmt.Sprintf("http://t.weather.itboy.net/api/weather/city/%s", d.CityID)
	resp, err := http.Get(url)
	if err != nil {
		logg.Danger("GetWeatherData:Get", err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logg.Danger("GetWeatherData:ReadAll", err)
		return
	}

	getItem := func(item string) string {
		return gjson.Get(string(body), item).Str
	}

	d.Data = append(d.Data, getItem("cityInfo.city")+" "+getItem("data.forecast.0.ymd")+" "+getItem("data.forecast.0.week")+" "+common.Lunar(time.Now().Format("20060102")))

	d.Data = append(d.Data, fmt.Sprintf("今日天气: %s|%s|%s|%s", getItem(`data.forecast.0.high`), getItem(`data.forecast.0.low`), getItem(`data.forecast.0.type`), getItem(`data.forecast.0.notice`)))

	d.Data = append(d.Data, fmt.Sprintf("明日天气: %s|%s|%s|%s", getItem(`data.forecast.1.high`), getItem(`data.forecast.1.low`), getItem(`data.forecast.1.type`), getItem(`data.forecast.1.notice`)))
}

func (d *Dconfig) Run() {
	// 每次执行先清空d.Data
	d.ClearData()
	d.LoginBBS()
	d.GetWeatherData()
	d.OaLogin()
	// d.GetOverTime("003464", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxNTE0MCIsImV4cCI6MTY0MjYwMzk5MSwiaWF0IjoxNjQyNTgyMzkxfQ.iV3AieIX9lR3DglJtz1eIkmbH8itepFKDQ7J_9aVnc0")
	var arr []string
	jsonStr := `{`

	// 此循环 顺序不对
	/* for i := 0; i < len(d.Data); i++ {
		temp := `"content` + strconv.Itoa(i) + `": {"value": "` + d.Data[i] + `"}`
		// arr = append(arr, temp) // 插入后面
		arr = append([]string{temp}, arr...) // 插入前面
	} */

	for k, v := range d.Data {
		temp := `"content` + strconv.Itoa(k) + `": {"value": "` + v + `"}`
		arr = append(arr, temp)
	}

	jsonStr += strings.Join(arr, ",")
	jsonStr += `}`

	access_token := weixin.GetAccessToken()
	if access_token == "" {
		return
	}

	flist := weixin.GetFlist(access_token)
	if flist == nil {
		return
	}

	// 这里循环每条数据 扫码关注的用户 v是用户微信号
	for _, v := range flist {
		// 这里可以针对每个用户处理不同事务
		logg.Info("Run:给" + v.Str + "发送数据")
		weixin.TemplatePost(access_token, jsonStr, "http://wufazhuce.com", weixin.WeatTemplateID, v.Str)
	}

}
