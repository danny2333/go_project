package weixin

import (
	"danny/app/pkg/logg"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/tidwall/gjson"
)

// 测试号信息
var (
	APPID          = "wx94464261ae67004a"
	APPSECRET      = "a40ff611d145fa88f288e873523a9cff"
	WeatTemplateID = "sI-D2m2qW1gEDag8LR480zXEFDoZExKjPZt9aJWIJoM" //天气模板ID
)

// Token 获取Access token结构体
type Token struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int    `json:"expires_in"`
}

// GetAccessToken 获取微信accesstoken
func GetAccessToken() string {
	url := fmt.Sprintf("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%v&secret=%v", APPID, APPSECRET)
	resp, err := http.Get(url)
	if err != nil {
		logg.Danger("GetAccessToken:Get", err)
		return ""
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logg.Danger("GetAccessToken:ReadAll", err)
		return ""
	}

	token := Token{}
	err = json.Unmarshal(body, &token)
	if err != nil {
		logg.Danger("GetAccessToken:Unmarshal", err)
		return ""
	}

	return token.AccessToken
}

// GetFlist 获取关注者列表
func GetFlist(access_token string) []gjson.Result {
	url := "https://api.weixin.qq.com/cgi-bin/user/get?access_token=" + access_token + "&next_openid="
	resp, err := http.Get(url)
	if err != nil {
		logg.Danger("GetFlist:Get", err)
		return nil
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logg.Danger("GetFlist:ReadAll", err)
		return nil
	}
	flist := gjson.Get(string(body), "data.openid").Array()
	return flist
}

// TemplatePost 发送模板消息
func TemplatePost(access_token string, reqdata string, fxurl string, templateid string, openid string) {
	url := "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token

	reqbody := "{\"touser\":\"" + openid + "\", \"template_id\":\"" + templateid + "\", \"url\":\"" + fxurl + "\", \"data\": " + reqdata + "}"

	resp, err := http.Post(url,
		"application/x-www-form-urlencoded",
		strings.NewReader(string(reqbody)))
	if err != nil {
		logg.Danger("GetFlist:Post", err)
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logg.Danger("GetFlist:ReadAll", err)
		return
	}

	logg.Info("TemplatePost: Send Success", string(body))
}
