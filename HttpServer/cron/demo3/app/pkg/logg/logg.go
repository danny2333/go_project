package logg

import (
	"log"
	"os"
	"path/filepath"
	"runtime"
)

var logger *log.Logger

func init() {
	file, err := os.OpenFile("log.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalln("Failed to open log file", err)
	}
	// logger = log.New(file, "INFO ", log.Ldate|log.Ltime|log.Lshortfile)
	logger = log.New(file, "INFO ", log.Ldate|log.Ltime)
}

// Info ...
func Info(args ...interface{}) {
	logger.SetPrefix("INFO ")

	_, file, line, ok := runtime.Caller(1)
	if !ok {
		file = "unknow"
		line = 0
	} else {
		file = filepath.Base(file)
	}

	// logger.Println(args...)
	logger.Printf("%s:%d:%s", file, line, args)
}

// Danger ...
func Danger(args ...interface{}) {
	logger.SetPrefix("ERROR ")

	_, file, line, ok := runtime.Caller(1)
	if !ok {
		file = "unknow"
		line = 0
	} else {
		file = filepath.Base(file)
	}

	logger.Printf("%s:%d:%s", file, line, args)
}

// Warning ...
func Warning(args ...interface{}) {
	logger.SetPrefix("WARNING ")
	_, file, line, ok := runtime.Caller(1)
	if !ok {
		file = "unknow"
		line = 0
	} else {
		file = filepath.Base(file)
	}

	logger.Printf("%s:%d:%s", file, line, args)
}
