package main

import (
	"danny/app/modules/danny"
	"danny/app/pkg/logg"
	"flag"
	"os"

	"github.com/robfig/cron"
)

func main() {
	// 注意指令后要空格
	// go run main.go -u 015899 -p 密码 -su 003471 -sp 密码
	CityID := flag.String("CityID", "101280601", "请输入城市代码，如北京: 101010100")
	UserName := flag.String("u", "015899", "请输入登录名")
	Password := flag.String("p", "Dc@015899", "请输入密码")
	SuperName := flag.String("su", "003471", "请输入su登录名")
	SuperPass := flag.String("sp", "luo123321!", "请输入sp密码")
	// 所有标志都声明完成以后，调用 `flag.Parse()` 来执行命令行解析。
	flag.Parse()

	os.Setenv("CityID", *CityID)
	os.Setenv("UserName", *UserName)
	os.Setenv("Password", *Password)
	os.Setenv("SuperName", *SuperName)
	os.Setenv("SuperPass", *SuperPass)

	Danny := danny.Init()
	// Danny.Run()

	// CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -i -tags netgo -v -ldflags="-s -w" -o gotest01
	// gitbash
	// env GOOS=linux go build -o gotest01
	// cmd
	// set GOARCH=amd64
	// set GOOS=linux
	// go build -o gotest01 main.go
	// chmod -R 777 gotest01
	// nohup ./gotest01 -u 015899 -p 密码 -su 003471 -sp 密码 &
	// nohup ./gotest01 &
	// ps -ef|grep gotest01
	// kill -9 324
	// history -d 2038
	// Windows方法 start /min danny.exe

	// // 秒 分 时 日 月 周
	// spec := "0 24 10 * * *" // 每天10:24
	// spec := "0 19 9 * * *" // 每天9.19
	spec := "0 0 7 * * *" // 每天早晨7:00
	c := cron.New()
	c.AddFunc(spec, Danny.Run)
	c.Start()
	logg.Info("main:程序执行成功...")
	// 让main函数不退出，让它在后台一直执
	select {}
}
