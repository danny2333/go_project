module danny

go 1.18

require (
	github.com/robfig/cron v1.2.0
	github.com/tidwall/gjson v1.13.0
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd
	golang.org/x/text v0.3.7
)

require (
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
)
