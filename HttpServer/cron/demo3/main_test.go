package main

import (
	"danny/app/pkg/common"
	"encoding/json"
	"fmt"
	"log"
	"testing"
	"time"
)

// TestInterface
// go test -run ^TestDemo$
func TestInterface(t *testing.T) {
	var jsonStr = `
    {
        "code": 200,
        "data": {
            "checkDutyTimeEntities": [],
            "dutyTimes": [
                {
                    "areaName": "安锦恒",
                    "cellStyleMap": {},
                    "department": {
                        "children": [],
                        "departmentId": 1416,
                        "departmentName": "杨祥组",
                        "level": 3,
                        "levelSearch": "1-004-001-007"
                    },
                    "departmentName": "杨祥组",
                    "departmentThree": "BITA",
                    "departmentTwo": "罗平组",
                    "empName": "丁聪",
                    "employee": {
                        "attachments": [],
                        "departments": [],
                        "empName": "丁聪",
                        "familyRelations": [],
                        "foreignTabCn": "",
                        "hireDate": "2020-09-14",
                        "isDimission": 1,
                        "isPositive": 1,
                        "learningAndTrainings": [],
                        "officeAreaCn": "安锦恒",
                        "position": "Web工程师",
                        "positiveTime": "2020-12-01",
                        "shenzhenContacts": [],
                        "termDate": "",
                        "viewWorkAge": "",
                        "workExperiences": []
                    },
                    "fit": 96.81,
                    "id": 21436846,
                    "isRevise": "0",
                    "isSchedule": "0",
                    "jobNumber": "015899",
                    "lastUpdatedDate": 1623567713000,
                    "overtime": 14.12,
                    "position": "Web工程师",
                    "recordTime": 13.67,
                    "workingDate": "2021-06-01"
                }
            ],
            "errorMaps": {},
            "page": {
                "bottomPage": 1,
                "nextPage": 1,
                "pageNo": 1,
                "pageSize": 10,
                "prePage": 1,
                "queryCount": true,
                "realCount": 0,
                "startRow": 0,
                "topPage": 1,
                "totalCount": 1,
                "totalPage": 1
            },
            "query": {
                "areasView": [
                    {
                        "areaId": 1,
                        "areaName": "安锦恒",
                        "areaNameCode": "AJH",
                        "createdBy": "besskyHR",
                        "creationDate": 1613813047000
                    }
                ],
                "cellStyleMap": {},
                "dataResourceJobNum": "015899",
                "deptIds": [],
                "deptsView": [
                    {
                        "children": [],
                        "departmentId": 1416,
                        "departmentName": "杨祥组",
                        "level": 3,
                        "levelSearch": "1-004-001-007",
                        "parentId": 1124
                    }
                ],
                "employee": {
                    "attachments": [],
                    "departments": [],
                    "familyRelations": [],
                    "foreignTabCn": "",
                    "learningAndTrainings": [],
                    "shenzhenContacts": [],
                    "viewWorkAge": "",
                    "workExperiences": []
                },
                "endTime": "2021-07-01",
                "jobNumber": "015899",
                "jobNumberList": [],
                "officeAreaList": [],
                "workingDate": "2021-06-01"
            }
        },
        "success": true
    }
`
	result := make(map[string]interface{})
	err := json.Unmarshal([]byte(jsonStr), &result)
	if err != nil {
		log.Println(err)
	}
	// log.Println(result["data"].(map[string]interface{})["dutyTimes"])
	// log.Printf("%v", result["data"].(map[string]interface{})["dutyTimes"].([]interface{})[2])
	fmt.Printf("值班时长: %v\n", result["data"].(map[string]interface{})["dutyTimes"].([]interface{})[0].(map[string]interface{})["overtime"])
}

// TestDemo
// go test -run ^TestDemo$
func TestDemo(t *testing.T) {
	yesterday := time.Now().Add(-24 * time.Hour)
	fmt.Printf("昨天: %v\n", yesterday.Format("2006-01-02"))
	fmt.Printf("农历: %v\n", common.Lunar(time.Now().Format("20060102")))
	fmt.Printf("该月的第一天: %v\n", common.GetFirstDateOfMonth(yesterday).Format("2006-01-02"))
	fmt.Printf("该月的最后一天: %v\n", common.GetLastDateOfMonth(yesterday).Format("2006-01-02"))

}
