package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// IndexHome 首页
func IndexHome(ctx *gin.Context) {

	//// query string
	queryVal1 := ctx.Query("val1")

	// 如果没有val2参数，返回默认的，如果有val2但值是空的则不会返回默认
	queryVal2 := ctx.DefaultQuery("val2", "val2_default")
	ctx.String(http.StatusOK, "val1=%s\nval2=%s", queryVal1, queryVal2)

	// post form data
	// formVal3 := ctx.PostForm("val3")
	// formVal4 := ctx.DefaultPostForm("val4", "val4_default")

	// path info
	// pathVal5 := ctx.Param("val5")

	// ctx.String(http.StatusOK, "val1 = %s \nval2 = %s \nval3 = %s \nval4 = %s \nval5 = %s", queryVal1, queryVal2, formVal3, formVal4, pathVal5)
	ctx.HTML(http.StatusOK, "index/index.html", gin.H{
		"msg": "easy gin",
	})
}
