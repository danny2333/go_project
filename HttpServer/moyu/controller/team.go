package controller

import (
	// model init
	_ "go-gin-cms/model"
	"go-gin-cms/service"
	"net/http"

	"github.com/gin-gonic/gin"
	// mysql derver
	_ "github.com/jinzhu/gorm"
)

// Intro .
func Intro(c *gin.Context) {
	topList, _ := service.GetBannerList(2)
	botList, _ := service.GetBannerList(3)

	c.HTML(http.StatusOK, "team/intro.html", gin.H{
		"topList":    topList,
		"botList":    botList,
		"config":     service.GetConfig(),
		"configteam": service.GetTeamConfig(),
		"controller": "Team",
	})
}

// Team .
func Team(c *gin.Context) {
	list, _ := service.GetTeamLimitList(4)
	c.HTML(http.StatusOK, "team/team.html", gin.H{
		"list":       list,
		"config":     service.GetConfig(),
		"configteam": service.GetTeamConfig(),
		"controller": "Index",
	})
}
