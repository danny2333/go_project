package controller

import (
	// model init
	_ "go-gin-cms/model"
	"go-gin-cms/service"
	"net/http"

	"github.com/gin-gonic/gin"
	// mysql derver
	_ "github.com/jinzhu/gorm"
)

// CategoryList .
func CategoryList(c *gin.Context) {
	list, err := service.GetCategoryList()
	if err != nil {
		c.JSON(200, gin.H{
			"Status":  0,
			"Message": "请求失败",
			"Data":    "",
		})
	}
	c.JSON(200, gin.H{
		"Status":  1,
		"Message": "请求成功",
		"Data":    list,
	})
}

// CategoryPost .
func CategoryPost(c *gin.Context) {
	err := service.CreateCategory()
	if err != nil {
		c.JSON(200, gin.H{
			"Status":  0,
			"Message": "添加失败",
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"Status":  0,
		"Message": "添加失败",
	})
}
