package controller

import (
	// model init
	_ "go-gin-cms/model"
	"go-gin-cms/service"
	"net/http"

	"github.com/gin-gonic/gin"
	// mysql derver
	_ "github.com/jinzhu/gorm"
)

// Works .
func Works(c *gin.Context) {
	types := c.Query("type")
	where := make(map[string]interface{})
	where["status"] = 1
	if types != "" {
		where["type"] = types
	}
	list, _ := service.GetWorksList(where)
	c.HTML(http.StatusOK, "works/works.html", gin.H{
		"list":       list,
		"types":      types,
		"config":     service.GetConfig(),
		"configteam": service.GetTeamConfig(),
		"controller": "Works",
	})
}
