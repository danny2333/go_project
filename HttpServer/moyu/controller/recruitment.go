package controller

import (
	// model init
	_ "go-gin-cms/model"
	"go-gin-cms/service"
	"html/template"
	"net/http"

	"github.com/gin-gonic/gin"
	// mysql derver
	_ "github.com/jinzhu/gorm"
)

// RecruitmentList .
func RecruitmentList(c *gin.Context) {
	list, _ := service.GetRecruitmentList()
	for key, value := range list {
		list[key].HTML = template.HTML(value.Desc)
	}
	c.HTML(http.StatusOK, "recruitment/lists.html", gin.H{
		"list":       list,
		"desc":       "",
		"config":     service.GetConfig(),
		"configteam": service.GetTeamConfig(),
		"controller": "Recruitment",
	})
}
