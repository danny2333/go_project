package controller

import (
	// model init
	_ "go-gin-cms/model"
	"go-gin-cms/service"
	"net/http"

	"github.com/gin-gonic/gin"
	// mysql derver
	_ "github.com/jinzhu/gorm"
)

// Contact .
func Contact(c *gin.Context) {
	c.HTML(http.StatusOK, "contact/contact.html", gin.H{
		"config":     service.GetConfig(),
		"configteam": service.GetTeamConfig(),
		"controller": "Contact",
	})
}
