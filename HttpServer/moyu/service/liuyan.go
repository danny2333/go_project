package service

import (
	"go-gin-cms/model"
)

// CreateLiuyan .
func CreateLiuyan(liuyan model.Liuyan) interface{} {
	return model.DB.Create(&liuyan).Error
}
