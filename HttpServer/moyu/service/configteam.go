package service

import (
	"go-gin-cms/model"
)

// GetTeamConfig .
func GetTeamConfig() model.ConfigTeam {
	var configteam model.ConfigTeam
	model.DB.First(&configteam)
	return configteam
}
