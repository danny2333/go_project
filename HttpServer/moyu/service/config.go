package service

import (
	"go-gin-cms/model"
)

// GetConfig .
func GetConfig() model.Config {
	var config model.Config
	model.DB.First(&config)
	return config
}
