package model

import (
	"errors"
	"fmt"
	"sync"

	orm "github.com/jinzhu/gorm"
	// mysql init
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/spf13/viper"
)

// MySQLPool .
type MySQLPool struct{}

var instance *MySQLPool
var once sync.Once

// DB .
var DB *orm.DB

// GetInstance 单例模式
func GetInstance() *MySQLPool {
	once.Do(func() {
		instance = &MySQLPool{}
	})

	return instance
}

// InitPool .
func (pool *MySQLPool) InitPool() (db *orm.DB) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=%s", viper.GetString("db.username"), viper.GetString("db.password"), viper.GetString("db.host"), viper.GetString("db.name"), viper.GetString("db.charset"))
	db, err := orm.Open("mysql", dsn)
	if err != nil {
		panic(errors.New("mysql连接失败"))
	}

	// 连接数配置也可以写入配置，在此读取
	db.DB().SetMaxIdleConns(50)
	db.DB().SetMaxOpenConns(50)
	// db.LogMode(true)
	DB = db
	return db
}
