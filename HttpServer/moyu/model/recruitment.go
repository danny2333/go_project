package model

import "html/template"

// TableName .
func (Recruitment) TableName() string {
	return "td_recruitment"
}

// Recruitment .
type Recruitment struct {
	ID     int    `gorm:"column:id"`
	Job    string `gorm:"column:job"`
	Desc   string `gorm:"column:desc"`
	Sort   int    `gorm:"column:sort"`
	Status int8   `gorm:"column:status"`
	HTML   template.HTML
	Ctime  int `gorm:"column:ctime"`
}
