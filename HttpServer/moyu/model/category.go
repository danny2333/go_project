package model

// TableName .
func (Category) TableName() string {
	return "category"
}

// Category .
type Category struct {
	ID   int64  `gorm:"column:id"`
	Name string `gorm:"column:name"`
}
