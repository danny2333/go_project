package model

// TableName .
func (Liuyan) TableName() string {
	return "td_liuyan"
}

// Liuyan .
type Liuyan struct {
	ID     int    `gorm:"column:id"`
	Name   string `gorm:"column:name" validate:"required"`
	Mobile string `gorm:"column:mobile" validate:"required"`
	Nr     string `gorm:"column:nr" validate:"required"`
	IP     string `gorm:"column:ip"`
	Ctime  int    `gorm:"column:ctime"`
}
