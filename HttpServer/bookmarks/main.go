package main

import (
	"crypto/rand"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"
)

// Book ...
type Book struct {
	ID          string `form:"ID"`
	Title       string `form:"Title"`
	URL         string `form:"URL"`
	Description string `form:"Description"`
	CreatedAt   string `form:"CreatedAt,omitempty"`
	UpdatedAt   string `form:"UpdatedAt,omitempty"`
	IsDel       bool   `form:"IsDel,omitempty"`
}

// WebTitle ...
const (
	WebTitle = "My Bookmarks"
)

type allBooks []Book

var books = allBooks{
	{
		ID:          "1",
		Title:       "BAIDU",
		URL:         "http://BAIDU.com",
		Description: "众里寻他千百度",
		CreatedAt:   "2010-12-12 05:12:25",
		UpdatedAt:   "2020-12-22 22:12:33",
		IsDel:       false,
	},
	{
		ID:          "2",
		Title:       "GOOGLE",
		URL:         "http://GOOGLE.com",
		Description: "Google 啥也不说",
		CreatedAt:   "2019-05-06 15:32:22",
		UpdatedAt:   "2019-09-18 12:42:33",
		IsDel:       false,
	},
	{
		ID:          "3",
		Title:       "APPLE",
		URL:         "http://APPLE.com",
		Description: "Apple 给我apple给我apple",
		CreatedAt:   "2019-07-22 15:32:22",
		UpdatedAt:   "2020-09-13 19:25:33",
		IsDel:       false,
	},
}

// homeHandle ...
func homeHandle(w http.ResponseWriter, r *http.Request) {
	const tmpl = `
	<div>
		<button onClick="window.location.href='/add/'">Add</button>
		{{range .}}
			{{if not .IsDel}}
				<div>
					<p><b>Title</b>：<a href="{{.URL}}" target="_blank">{{.Title}}</a></p>
					<p><b>URL</b>：{{.URL}}</p>
					<p><b>Description</b>：{{.Description}}</p>
					<p><b>CreatedAt</b>：{{.CreatedAt}}</p>
					<p><b>UpdatedAt</b>：{{.UpdatedAt}}</p>
					<p><a href="/edit/?id={{.ID}}">Edit</a> - <a href="/delete/?id={{.ID}}">Delete</a></p>
				</div>
				<hr />
			{{end}}
		{{end}}
	</div>`

	nowTmpl := BaseHTML(WebTitle, tmpl)

	t, err := template.New("webpage").Parse(nowTmpl)
	if err != nil {
		log.Fatal(err)
	}

	t.Execute(w, books)
}

// updateBook ...
func updateBook(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		bookID := r.URL.Query().Get("id")
		const tmpl = `
		<div>
			<form action="/edit/" method="post">
				<input type="hidden" name="ID" value="{{.ID}}" />
				Title:<br>
				<input type="text" name="Title" value="{{.Title}}"><br><br>
				URL:<br>
				<input type="url" name="URL" value="{{.URL}}"><br><br>
				Description:<br>
				<textarea name="Description">{{.Description}}</textarea><br><br>
				<input type="submit" value="Submit">
			</form>
		</div>`

		nowTmpl := BaseHTML("Edit - "+WebTitle, tmpl)

		t, err := template.New("webpage").Parse(nowTmpl)
		if err != nil {
			log.Fatal(err)
		}

		var book Book
		for _, singleBook := range books {
			if singleBook.ID == bookID {
				book = singleBook
			}
		}

		t.Execute(w, book)

	} else if r.Method == "POST" {
		r.ParseForm()

		bookID := r.FormValue("ID")
		title := r.FormValue("Title")
		url := r.FormValue("URL")
		description := r.FormValue("Description")

		for i, singleBook := range books {
			if singleBook.ID == bookID {
				books[i].Title = title
				books[i].URL = url
				books[i].Description = description
				books[i].UpdatedAt = time.Now().Format("2006-01-02 15:04:05")
				http.Redirect(w, r, "/", 302)
			}
		}
		log.Printf("更新成功！数据ID = %v\n", bookID)
	}
}

// deleteBook ...
func deleteBook(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	bookID := r.FormValue("id")
	log.Println("delete book id: ", bookID)
	log.Println("delete book id2: ", r.URL.Query().Get("id"))

	for i, singleBook := range books {
		if singleBook.ID == bookID {
			// books = append(books[:i], books[i+1:]...)
			books[i].IsDel = true
			books[i].UpdatedAt = time.Now().Format("2006-01-02 15:04:05")

			http.Redirect(w, r, "/", 302)
		}
	}
}

// addBook ...
func addBook(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		const tmpl = `
		<div>
			<form action="/add/" method="post">
				Title:<br>
				<input type="text" name="Title" value=""><br><br>
				URL:<br>
				<input type="url" name="URL" value=""><br><br>
				Description:<br>
				<textarea name="Description"></textarea><br><br>
				<input type="submit" value="Submit">
			</form>
		</div>`

		nowTmpl := BaseHTML("Edit - "+WebTitle, tmpl)

		t, err := template.New("webpage").Parse(nowTmpl)
		if err != nil {
			log.Fatal(err)
		}

		t.Execute(w, nil)

	} else if r.Method == "POST" {
		r.ParseForm()

		var newBook = Book{
			ID:          createUUID(),
			Title:       r.FormValue("Title"),
			URL:         r.FormValue("URL"),
			Description: r.FormValue("Description"),
			CreatedAt:   time.Now().Format("2006-01-02 15:04:05"),
			UpdatedAt:   time.Now().Format("2006-01-02 15:04:05"),
		}

		books = append(books, newBook)
		http.Redirect(w, r, "/", 302)
		log.Printf("新增成功！数据ID = %v\n", newBook.ID)
	}
}

// BaseHTML ...
func BaseHTML(title, body string) string {
	var tmpl = `
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>` + title + `</title>
	</head>
	<body>
		<header>
			<h1>My BookMarks list</h1>
		</header>
		<main>
		` + body + `
		</main>
		<footer>

		</footer>
	</body>
	</html>`
	return tmpl
}

// testMiddleware ...
func testMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Got a %s request for: %v", r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

// testJSONP ...
func testJSONP(w http.ResponseWriter, r *http.Request) {
	callbackName := r.URL.Query().Get("callback")
	if callbackName == "" {
		w.Write([]byte("Please give callback name in query string"))
	}

	b, err := json.Marshal(r.Header)
	if err != nil {
		w.Write([]byte("json encode error"))
	}

	w.Header().Set("Content-Type", "application/javascript")
	fmt.Fprintf(w, "%s(%s);", callbackName, b)
}

func main() {
	// nohup ./bookmarks -port 9091 > /dev/null 2> /dev/null  &
	port := flag.String("port", "8081", "端口地址，默认8081")
	flag.Parse()
	fmt.Println("程序已启动，访问地址：http://127.0.0.1:" + *port)
	fmt.Println("Name: 书签小程序\nVersion：v0.0.1\nAuthor：Danny")

	mux := http.NewServeMux()

	mux.HandleFunc("/", homeHandle)
	mux.HandleFunc("/delete/", deleteBook)
	mux.HandleFunc("/edit/", updateBook)
	mux.HandleFunc("/add/", addBook)

	mux.HandleFunc("/jsonp", testJSONP)
	mux.HandleFunc("/jsonp-server", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, `<!DOCTYPE html>
			<html>
			<head><title>Go JSONP Server</title></head>
			<body>
			<button id="btn">Click to get HTTP header via JSONP</button>
			<pre id="result"></pre>
			<script>
			'use strict';
			
			var btn = document.getElementById("btn");
			var result = document.getElementById("result");
			
			function myCallback(acptlang) {
			  result.innerHTML = JSON.stringify(acptlang, null, 2);
			}
			
			function jsonp() {
			  result.innerHTML = "Loading ...";
			  var tag = document.createElement("script");
			  tag.src = "/jsonp?callback=myCallback";
			  document.querySelector("head").appendChild(tag);
			}
			
			btn.addEventListener("click", jsonp);
			</script>
			</body>
			</html>`)
	})

	WrappedMux := testMiddleware(mux)
	err := http.ListenAndServe(":"+*port, WrappedMux)

	if err != nil {
		log.Println("http error", err)
	}
}

// create a random UUID with from RFC 4122
// adapted from http://github.com/nu7hatch/gouuid
func createUUID() (uuid string) {
	u := new([16]byte)
	_, err := rand.Read(u[:])
	if err != nil {
		log.Fatalln("Cannot generate UUID", err)
	}

	// 0x40 is reserved variant from RFC 4122
	u[8] = (u[8] | 0x40) & 0x7F
	// Set the four most significant bits (bits 12 through 15) of the
	// time_hi_and_version field to the 4-bit version number.
	u[6] = (u[6] & 0xF) | (0x4 << 4)
	uuid = fmt.Sprintf("%x-%x-%x-%x-%x", u[0:4], u[4:6], u[6:8], u[8:10], u[10:])
	return
}
