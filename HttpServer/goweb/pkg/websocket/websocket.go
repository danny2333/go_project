package websocket

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

// upgrader ws配置
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,

	CheckOrigin: func(r *http.Request) bool { return true },
}

// Upgrade .
func Upgrade(w http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return conn, nil
}

// // Reader .
// func Reader(conn *websocket.Conn) {
// 	for {
// 		messageType, p, err := conn.ReadMessage()
// 		if err != nil {
// 			log.Println(err)
// 			return
// 		}

// 		/* s := struct {
// 			T int64  `json:"timestamp"`
// 			M string `json:"metricname"`
// 		}{} */
// 		s := struct {
// 			T int    `json:"type"`
// 			B string `json:"body"`
// 		}{}

// 		if err := json.Unmarshal(p, &s); err != nil {
// 			log.Fatalf("failed unmarshalling: %v", err)
// 		}s
// 		fmt.Println(s)

// 		if err := conn.WriteMessage(messageType, p); err != nil {
// 			log.Println(err)
// 			return
// 		}
// 	}
// }

// // Writer .
// func Writer(conn *websocket.Conn) {
// 	for {
// 		fmt.Println("Sending")

// 		messageType, r, err := conn.NextReader()
// 		if err != nil {
// 			fmt.Println(err)
// 			return
// 		}

// 		w, err := conn.NextWriter(messageType)
// 		if err != nil {
// 			fmt.Println(err)
// 			return
// 		}

// 		if _, err := io.Copy(w, r); err != nil {
// 			fmt.Println(err)
// 			return
// 		}

// 		if err := w.Close(); err != nil {
// 			fmt.Println(err)
// 			return
// 		}
// 	}
// }
