package websocket

import (
	"fmt"
)

// Pool Pool结构，该结构将包含并发通信所需的所有渠道以及客户映射
type Pool struct {
	// Register 当有新客户端连接时，我们的注册频道将向该池中的所有客户端发送新用户加入
	Register chan *Client
	// Unregister 将取消注册用户，并在客户端断开连接时通知池
	Unregister chan *Client
	// Clients 客户到布尔值的映射。我们可以使用boolean值来指示活动/非活动状态，但不能根据浏览器的焦点进一步断开连接
	Clients map[*Client]bool
	// Broadcast 一个通道，该通道在传递消息时将循环通过池中的所有客户端，并通过套接字连接发送消息
	Broadcast chan Message
}

// NewPool .
func NewPool() *Pool {
	return &Pool{
		Register:   make(chan *Client),
		Unregister: make(chan *Client),
		Clients:    make(map[*Client]bool),
		Broadcast:  make(chan Message),
	}
}

// Start .
func (pool *Pool) Start() {
	for {
		select {
		case client := <-pool.Register:
			pool.Clients[client] = true
			fmt.Println("Size of Connection Pool: ", len(pool.Clients))
			for client := range pool.Clients {
				fmt.Println(client)
				client.Conn.WriteJSON(Message{Type: 1, Body: "New User Joined..."})
			}
			break
		case client := <-pool.Unregister:
			delete(pool.Clients, client)
			fmt.Println("Size of Connection Pool: ", len(pool.Clients))
			for client := range pool.Clients {
				client.Conn.WriteJSON(Message{Type: 1, Body: "User Disconnected..."})
			}
			break
		case message := <-pool.Broadcast:
			fmt.Println("Sending message to all clients in Pool")
			for client := range pool.Clients {
				if err := client.Conn.WriteJSON(message); err != nil {
					fmt.Println(err)
					return
				}
			}
		}
	}
}
