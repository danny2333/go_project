package auth

import (
	"errors"
	"goweb/app"
	"goweb/app/common/models/user"
	"net/http"
)

func _getUID(w http.ResponseWriter, r *http.Request) string {
	_uid := app.GlobalSession().SessionStart(w, r).Get("uid")
	uid, ok := _uid.(string)
	if ok && len(uid) > 0 {
		return uid
	}
	return ""
}

// User 获取登录用户信息
func User(w http.ResponseWriter, r *http.Request) user.User {
	uid := _getUID(w, r)
	if len(uid) > 0 {
		_user, err := user.Get(uid)
		if err == nil {
			return _user
		}
	}
	return user.User{}
}

// Attempt 尝试登录
func Attempt(email string, password string, w http.ResponseWriter, r *http.Request) error {
	// 1. 根据 Email 获取用户
	_user, err := user.GetByEmail(email)

	// 2. 如果出现错误
	if err != nil {
		// return errors.New("账号不存在或密码错误")
		// return errors.New("内部错误，请稍后尝试")
		return err
	}

	// 3. 匹配密码
	if !_user.ComparePassword(password) {
		return errors.New("账号不存在或密码错误")
	}

	// 4. 登录用户，保存会话
	app.GlobalSession().SessionStart(w, r).Set("uid", _user.GetStringID())

	return nil
}

// Login 登录指定用户
func Login(w http.ResponseWriter, r *http.Request, _user user.User) {
	app.GlobalSession().SessionStart(w, r).Set("uid", _user.GetStringID())
}

// Logout 退出用户
func Logout(w http.ResponseWriter, r *http.Request) {
	app.GlobalSession().SessionStart(w, r).Delete("uid")
}

// Check 检测是否登录
func Check(w http.ResponseWriter, r *http.Request) bool {
	return len(_getUID(w, r)) > 0
}
