package common

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// Message 结构体
type Message struct {
	Code    int64
	Message string
	Data    interface{}
}

// Success 成功
func (Message Message) Success(message string, data interface{}, w http.ResponseWriter) {
	Message.Code = 0
	Message.Message = message
	Message.Data = data
	jsonStr, err := json.Marshal(Message)
	if err != nil {
		log.Fatal("序列化json错误")
	}
	w.Header().Add("Access-Control-Allow-Origin", "*")
	fmt.Fprintf(w, "%s", string(jsonStr))
}

// Error 错误
func (Message Message) Error(message string, data interface{}, w http.ResponseWriter) {
	Message.Code = 1001
	Message.Message = message
	Message.Data = data
	jsonStr, err := json.Marshal(Message)
	if err != nil {
		log.Fatal("序列化json错误")
	}
	fmt.Fprintf(w, "%s", string(jsonStr))
}

func RespondWithError(w http.ResponseWriter, code int, msg string) {
	RespondWithJson(w, code, map[string]string{"error": msg})
}

func RespondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}
