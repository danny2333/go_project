package common

import (
	"encoding/json"
	"strconv"
)

func StringToInt64(e string) (int64, error) {
	return strconv.ParseInt(e, 10, 64)
}

func IntToString(e int) string {
	return strconv.Itoa(e)
}

func Float64ToString(e float64) string {
	return strconv.FormatFloat(e, 'E', -1, 64)
}

func Int64ToString(e int64) string {
	return strconv.FormatInt(e, 10)
}

func StructToJsonStr(e interface{}) (string, error) {
	if b, err := json.Marshal(e); err == nil {
		return string(b), err
	} else {
		return "", err
	}
}

func JsonStrToMap(e string) (map[string]interface{}, error) {
	var dict map[string]interface{}
	if err := json.Unmarshal([]byte(e), &dict); err == nil {
		return dict, err
	} else {
		return nil, err
	}
}
