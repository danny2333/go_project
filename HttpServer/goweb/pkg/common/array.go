package common

import "strconv"

type Data struct {
	Id    string  `json:"id"`
	Name  string  `json:"name"`
	PId   string  `json:"p_id"`
	Child []*Data `json:"child"`
}

type SliceData []*Data

func (s SliceData) Len() int {
	return len(s)
}

func (s SliceData) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s SliceData) Less(i, j int) bool {
	ii, err := strconv.Atoi(string([]byte(s[i].Id)[1:]))
	if err != nil {
		return false
	}
	jj, err := strconv.Atoi(string([]byte(s[j].Id)[1:]))
	if err != nil {
		return false
	}
	return ii < jj

}

// MakeTree 生成树
func MakeTree(Allnode []*Data, node *Data) {
	childs, _ := HaveChild(Allnode, node) //判断节点是否有子节点并返回
	if childs != nil {
		node.Child = append(node.Child, childs[0:]...) //添加子节点
		for _, v := range childs {                     //查询子节点的子节点，并添加到子节点
			_, has := HaveChild(Allnode, v)
			if has {
				MakeTree(Allnode, v) //递归添加节点
			}
		}
	}
}

// HaveChild 判断是否有子类
func HaveChild(Allnode []*Data, node *Data) (childs []*Data, yes bool) {
	for _, v := range Allnode {
		if v.PId == node.Id {
			childs = append(childs, v)
		}
	}
	if childs != nil {
		yes = true
	}
	return
}
