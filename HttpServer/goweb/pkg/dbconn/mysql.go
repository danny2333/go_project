package dbconn

import (
	"database/sql"
	"errors"
	"fmt"
	"goweb/config"
	"sync"

	_ "github.com/go-sql-driver/mysql"
)

type MySQLPool struct{}

var (
	instance *MySQLPool
	once     sync.Once
	DB       *sql.DB
)

// GetInstance 单例模式
func GetInstance() *MySQLPool {
	once.Do(func() {
		instance = &MySQLPool{}
	})

	return instance
}

// InitPool ...
func (pool *MySQLPool) InitPool() (db *sql.DB) {
	// dsn := "root:root@tcp(127.0.0.1:3306)/demo1?charset=utf8"
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s%s", config.Config.Database.User, config.Config.Database.Pwd, config.Config.Database.Host, config.Config.Database.Port, config.Config.Database.Dbname, config.Config.Database.Parameter)
	db, err := sql.Open(config.Config.Database.Type, dsn)
	if err != nil {
		panic(errors.New("MySQL连接失败"))
	}

	db.SetMaxIdleConns(50)
	db.SetMaxOpenConns(50)
	DB = db
	return db
}
