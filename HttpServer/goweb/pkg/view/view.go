package view

import (
	"goweb/app/blog/models/category"
	"goweb/app/common/models/user"
	"goweb/pkg/auth"
	"goweb/pkg/flash"
	"goweb/pkg/logger"
	"html/template"
	"net/http"
	"path/filepath"
	"strings"
)

// D 是 map[string]interface{} 的简写
type D map[string]interface{}

// Render 渲染通用视图
func Render(w http.ResponseWriter, r *http.Request, data D, tplFiles ...string) {
	RenderTemplate(w, r, "app", data, tplFiles...)
}

// RenderSimple 渲染简单的视图
func RenderSimple(w http.ResponseWriter, r *http.Request, data D, tplFiles ...string) {
	RenderTemplate(w, r, "simple", data, tplFiles...)
}

// RenderTemplate 渲染视图
func RenderTemplate(w http.ResponseWriter, r *http.Request, name string, data D, tplFiles ...string) {
	// 1. 通用模板数据
	var err error
	data["isLogined"] = auth.Check(w, r)
	data["user"] = auth.User(w, r)
	data["flash"] = flash.All(w, r)
	data["Categories"], _ = category.All()

	// 2. 生成模板文件
	allFiles := getTemplateFiles(tplFiles...)

	// 3. 解析所有模板文件
	tmpl, err := template.New("").
		Funcs(template.FuncMap{
			"RenderUnsafe":    RenderUnsafe,
			"strtime":         StrTime,
			"plus1":           selfPlus,
			"numplusplus":     numPlusPlus,
			"strip":           Long2IPString,
			"SubString":       SubString,
			"GetUserByID":     user.GetUserByID,
			"GetCateNameByID": category.GetCateNameByID,
		}).ParseFiles(allFiles...)
	logger.LogError(err)

	// 4. 渲染模板
	tmpl.ExecuteTemplate(w, name, data)
}

func getTemplateFiles(tplFiles ...string) []string {
	// 1 设置模板相对路径
	viewDir := "resources/views/"

	// 2. 遍历传参文件列表 Slice，设置正确的路径，支持 dir.filename 语法糖
	for i, f := range tplFiles {
		tplFiles[i] = viewDir + strings.Replace(f, ".", "/", -1) + ".gohtml"
	}

	// 3. 所有布局模板文件 Slice
	layoutFiles, err := filepath.Glob(viewDir + "layouts/*.gohtml")
	logger.LogError(err)

	// 4. 合并所有文件
	return append(layoutFiles, tplFiles...)
}
