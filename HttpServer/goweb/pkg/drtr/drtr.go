package drtr

import (
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
)

// simMux 路由器结构
type simMux struct {
	mu     sync.RWMutex               // 读写锁
	m      map[string][]HandlerStruct // 动态路由
	static map[string]string          // 静态路由
}

// HandlerStruct 路由结构
type HandlerStruct struct {
	regex   *regexp.Regexp // 正则对象
	params  map[int]string // 请求参数
	handler Handler
}

// Handler 精简
type Handler func(http.ResponseWriter, *http.Request)

// New 创建路由器
func New() *simMux {
	return &simMux{
		sync.RWMutex{},
		make(map[string][]HandlerStruct),
		make(map[string]string),
	}
}

// Static 注册静态路由
func (sMux *simMux) Static(prefix, path string) bool {
	if len(prefix) == 0 || len(path) == 0 {
		return false
	}

	_, exist := sMux.static[prefix]
	if exist {
		panic("simMux AddStatic: duplicate static prefix \"" + prefix + "\"")
	}
	sMux.static[prefix] = path
	return true
}

// add 注册动态路由
// 支持路由正则匹配，格式：/user/:id([0-9]+)/:name([a-z]+)
func (sMux *simMux) add(method, pattern string, handler Handler) bool {
	if len(pattern) == 0 || handler == nil {
		return false
	}

	sMux.mu.Lock()
	defer sMux.mu.Unlock()

	params := make(map[int]string) // 请求参数
	var patterns []string
	pos := 0
	arr := strings.Split(pattern, "/")
	for _, v := range arr {
		if strings.HasPrefix(v, ":") {
			index := strings.Index(v, "(")
			if index != -1 {
				patterns = append(patterns, v[index:])
				params[pos] = v[1:index]
				pos++
				continue
			}
		}
		patterns = append(patterns, v)
	}

	regex, err := regexp.Compile(strings.Join(patterns, "/"))
	if err != nil {
		panic("Sim add: wrong pattern \"" + pattern + "\"")
	}

	sMux.m[method] = append(sMux.m[method], HandlerStruct{regex: regex, params: params, handler: handler})
	return true
}

// Get 路由方法
func (sMux *simMux) Get(pattern string, handler Handler) bool {
	return sMux.add(http.MethodGet, pattern, handler)
}

// Post 路由方法
func (sMux *simMux) Post(pattern string, handler Handler) bool {
	return sMux.add(http.MethodPost, pattern, handler)
}

// Put 路由方法
func (sMux *simMux) Put(pattern string, handler Handler) bool {
	return sMux.add(http.MethodPut, pattern, handler)
}

// Delete 路由方法
func (sMux *simMux) Delete(pattern string, handler Handler) bool {
	return sMux.add(http.MethodDelete, pattern, handler)
}

// ServeHTTP 路由匹配与请求分发
func (sMux *simMux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// 静态路由解析
	for prefix, path := range sMux.static {
		if strings.HasPrefix(r.URL.Path, prefix) {
			file := path + r.URL.Path[len(prefix):]
			http.ServeFile(w, r, file)
			return
		}
	}

	// 动态路由解析
	if sMux.m[r.Method] == nil || len(sMux.m[r.Method]) == 0 {
		http.NotFound(w, r)
		return
	}

	path := r.URL.Path
	for _, HandlerStruct := range sMux.m[r.Method] {
		if !HandlerStruct.regex.MatchString(path) {
			continue
		}

		matches := HandlerStruct.regex.FindStringSubmatch(path)
		if len(matches[0]) != len(path) {
			continue
		}

		if len(HandlerStruct.params) > 0 {
			values := r.URL.Query()
			for i, val := range matches[1:] {
				values.Add(HandlerStruct.params[i], val)
			}
			r.URL.RawQuery = url.Values(values).Encode()
		}
		HandlerStruct.handler(w, r)
		return
	}
	http.NotFound(w, r)
}
