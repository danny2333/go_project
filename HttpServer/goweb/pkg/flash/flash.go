package flash

import (
	"encoding/gob"
	"goweb/app"
	"net/http"
)

// Flashes Flash 消息数组类型，用以在会话中存储 map
type Flashes map[string]interface{}

// 存入会话数据里的 key
var flashKey = "_flashes"

func init() {
	// 在 gorilla/sessions 中存储 map 和 struct 数据需
	// 要提前注册 gob，方便后续 gob 序列化编码、解码
	gob.Register(Flashes{})
}

// Info 添加 Info 类型的消息提示
func Info(w http.ResponseWriter, r *http.Request, message string) {
	addFlash(w, r, "info", message)
}

// Warning 添加 Warning 类型的消息提示
func Warning(w http.ResponseWriter, r *http.Request, message string) {
	addFlash(w, r, "warning", message)
}

// Success 添加 Success 类型的消息提示
func Success(w http.ResponseWriter, r *http.Request, message string) {
	addFlash(w, r, "success", message)
}

// Danger 添加 Danger 类型的消息提示
func Danger(w http.ResponseWriter, r *http.Request, message string) {
	addFlash(w, r, "danger", message)
}

// All 获取所有消息
func All(w http.ResponseWriter, r *http.Request) Flashes {
	val := app.GlobalSession().SessionStart(w, r).Get(flashKey)
	// 读取是必须做类型检测
	flashMessages, ok := val.(Flashes)
	if !ok {
		return nil
	}
	// 读取即销毁，直接删除
	app.GlobalSession().SessionStart(w, r).Delete(flashKey)
	return flashMessages
}

// 私有方法，新增一条提示
func addFlash(w http.ResponseWriter, r *http.Request, key string, message string) {
	flashes := Flashes{}
	flashes[key] = message
	app.GlobalSession().SessionStart(w, r).Set(flashKey, flashes)
}
