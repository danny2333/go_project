package cache

import (
	"github.com/coocood/freecache"
)

// 缓存
var cache = freecache.NewCache(100 * 1024 * 1024)

// Set 写入 时长
func Set(key, value []byte, expireSeconds int) error {
	return cache.Set(key, value, expireSeconds)
}

func Get(key []byte) ([]byte, error) {
	return cache.Get(key)
}

func Del(key []byte) bool {
	return cache.Del(key)
}
