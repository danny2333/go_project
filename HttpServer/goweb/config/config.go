package config

import (
	"encoding/json"
	"os"
)

type config struct {
	Static              string      `json:"static"`
	Template            string      `json:"template"`
	Address             string      `json:"address"`
	Port                string      `json:"port"`
	HandleTimeoutSecond int         `json:"handleTimeoutSecond"`
	Database            mysqlConfig `json:"database"`
}

type mysqlConfig struct {
	Type      string `json:"type"`
	User      string `json:"user"`
	Pwd       string `json:"pwd"`
	Dbname    string `json:"dbname"`
	Host      string `json:"host"`
	Port      string `json:"port"`
	Parameter string `json:"parameter"`
}

var Config config

func LoadConfig(configFile string) error {
	configData, err := os.ReadFile(configFile)
	if err != nil {
		return err
	}
	return json.Unmarshal(configData, &Config)
}

// 测试用方法
func exportDefaultConfig(configFile string) error {
	defaultConfig := config{}
	configData, err := json.MarshalIndent(&defaultConfig, "", "\t")
	if err != nil {
		return err
	}
	err = os.WriteFile(configFile, configData, 0666)
	if err != nil {
		return err
	}
	return nil
}
