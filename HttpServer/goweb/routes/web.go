package routes

import (
	"fmt"
	blog "goweb/app/blog/controllers"
	commonApis "goweb/app/common/apis"
	"goweb/app/common/controllers"
	"goweb/app/common/middlewares"
	pinduck "goweb/app/pinduck/controllers"
	webim "goweb/app/webim/controllers"
	"goweb/pkg/common"
	"goweb/pkg/drtr"
	"net/http"
)

// RegisterWebRoutes 注册网页相关路由
func RegisterWebRoutes() {

	// 静态页面
	pc := new(controllers.PagesController)
	// http.NotFoundHandler = http.HandlerFunc(pc.NotFound)
	// http.NotFoundHandler()
	// http.HandleFunc("/", pc.Index)
	// http.Handle("/", http.FileServer(http.Dir("app/templates")))
	http.HandleFunc("/count", pc.Count)

	// auth test
	http.HandleFunc("/auth_test", common.Use(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Authenticated!"))
	}, pc.BasicAuth))

	// 用户认证
	auc := new(controllers.AuthController)
	http.HandleFunc("/auth/register", middlewares.Guest(auc.Register))
	http.HandleFunc("/auth/doregister", middlewares.Guest(auc.DoRegister))
	http.HandleFunc("/auth/login", middlewares.Guest(auc.Login))
	http.HandleFunc("/auth/dologin", middlewares.Guest(auc.DoLogin))
	http.HandleFunc("/auth/logout", middlewares.Auth(auc.Logout))

	// 文章分类
	cc := new(blog.CategoriesController)
	http.HandleFunc("/blog/categories/create", middlewares.Auth(cc.Create))
	http.HandleFunc("/blog/categories/store", middlewares.Auth(cc.Store))
	http.HandleFunc("/blog/categories/show", cc.Show)
	http.HandleFunc("/blog/categories", cc.Categories)

	// 文章相关页面
	ac := new(blog.ArticlesController)
	http.HandleFunc("/blog/about", ac.About)
	http.HandleFunc("/blog/articles/show", ac.Show)
	http.HandleFunc("/blog/archives", ac.Archives)
	http.HandleFunc("/blog", ac.Index)
	http.HandleFunc("/blog/articles/create", middlewares.Auth(ac.Create))
	http.HandleFunc("/blog/articles/store", middlewares.Auth(ac.Store))
	http.HandleFunc("/blog/articles/edit", middlewares.Auth(ac.Edit))
	http.HandleFunc("/blog/articles/update", middlewares.Auth(ac.Update))
	http.HandleFunc("/blog/articles/delete", middlewares.Auth(ac.Delete))

	// 上传图片
	up := new(blog.UploadImgController)
	http.HandleFunc("/blog/uploadimg", up.UploadImg)
	http.HandleFunc("/blog/imglist", up.ImgList)
	http.HandleFunc("/blog/viewimg", up.ViewImg)

	// 用户信息
	uc := new(controllers.UserController)
	http.HandleFunc("/blog/author/show", uc.Show)
	http.HandleFunc("/blog/authors", uc.Authors)

	// 静态资源
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("resources/public/"))))

	// websocket mock test
	commonApis.RegisterRouter()

	// webim
	webim.RegisterRouter()

	// pinduck
	pinduck.RegisterRouter()

	// test drtr route
	// http://localhost:8000/user/666/shiajun
	r := drtr.New()
	r.Get("/", pc.Index)
	r.Get("/user/:id([0-9]+)/:name([a-z]+)", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, r.URL.Query())
	})
	http.Handle("/", r)
}
