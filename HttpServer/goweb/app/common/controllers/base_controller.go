package controllers

import (
	"goweb/pkg/flash"
	"net/http"
)

// BaseController 基础控制器
type BaseController struct {
}

// ResposeForUnauthorized 处理未授权的访问
func (bc BaseController) ResposeForUnauthorized(w http.ResponseWriter, r *http.Request) {
	flash.Warning(w, r, "未授权操作！")
	http.Redirect(w, r, "/", http.StatusFound)
}
