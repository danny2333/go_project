package controllers

import (
	"fmt"
	"goweb/app"
	"goweb/pkg/view"
	"net/http"
	"text/template"
)

// PagesController 处理静态页面
type PagesController struct {
	BaseController
}

// Index 首页
func (p *PagesController) Index(w http.ResponseWriter, r *http.Request) {
	view.RenderSimple(w, r, view.D{}, "base.home")
}

func (p *PagesController) Count(w http.ResponseWriter, r *http.Request) {
	sess := app.GlobalSession().SessionStart(w, r)
	ct := sess.Get("countnum")
	if ct == nil {
		sess.Set("countnum", 1)
	} else {
		sess.Set("countnum", (ct.(int) + 1))
	}
	t, _ := template.ParseFiles("resources/views/common/count.html")
	w.Header().Set("Content-Type", "text/html")
	t.Execute(w, sess.Get("countnum"))
}

// NotFound 404 页面
func (p *PagesController) NotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprint(w, "<h1>请求页面未找到 :(</h1><p>如有疑惑，请联系我们。</p>")
}

// basicAuth 权限
func (p *PagesController) BasicAuth(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)
		username, password, authOK := r.BasicAuth()
		if !authOK {
			http.Error(w, "Not authorized", http.StatusUnauthorized)
			return
		}

		// 可调用数据库查询
		if username != "username" || password != "password" {
			http.Error(w, "Not authorized", http.StatusUnauthorized)
			return
		}

		h.ServeHTTP(w, r)
	}
}
