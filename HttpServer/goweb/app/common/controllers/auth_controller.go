package controllers

import (
	"fmt"
	"goweb/app/common/middlewares"
	"goweb/app/common/models/user"
	"goweb/app/common/requests"
	"goweb/pkg/auth"
	"goweb/pkg/flash"
	"goweb/pkg/view"
	"net/http"
)

// AuthController 处理静态页面
type AuthController struct{}

func RegisterAuthRoutes() {
	// 用户认证
	auc := new(AuthController)
	http.HandleFunc("/auth/register", middlewares.Guest(auc.Register))
	http.HandleFunc("/auth/doregister", middlewares.Guest(auc.DoRegister))
	http.HandleFunc("/auth/login", middlewares.Guest(auc.Login))
	http.HandleFunc("/auth/dologin", middlewares.Guest(auc.DoLogin))
	http.HandleFunc("/auth/logout", middlewares.Auth(auc.Logout))
}

// Register 注册页面
func (*AuthController) Register(w http.ResponseWriter, r *http.Request) {
	view.RenderSimple(w, r, view.D{}, "auth.register")
}

// DoRegister 处理注册逻辑
func (*AuthController) DoRegister(w http.ResponseWriter, r *http.Request) {
	// 1. 初始化数据
	_user := user.User{
		Name:            r.PostFormValue("name"),
		Email:           r.PostFormValue("email"),
		Password:        r.PostFormValue("password"),
		PasswordComfirm: r.PostFormValue("password_comfirm"),
	}

	// 2. 表单规则
	if err := requests.ValidateRegistrationForm(_user); err != nil {
		// 3. 表单不通过 —— 重新显示表单
		fmt.Println(err)
		view.RenderSimple(w, r, view.D{
			"Errors": err,
			"User":   _user,
		}, "auth.register")
	} else {
		// flash.Success(w, r, "恭喜您注册成功！")
		// auth.Login(w, r, _user)
		// http.Redirect(w, r, "/", http.StatusFound)
		// 4. 验证成功，创建数据
		_user.Create()

		if _user.ID > 0 {
			// 登录用户并跳转到首页
			flash.Success(w, r, "恭喜您注册成功！")
			auth.Login(w, r, _user)
			http.Redirect(w, r, "/", http.StatusFound)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprint(w, "创建用户失败，请联系管理员")
		}
	}
}

// Login 显示登录表单
func (*AuthController) Login(w http.ResponseWriter, r *http.Request) {
	view.RenderSimple(w, r, view.D{}, "auth.login")
}

// DoLogin 处理登录表单提交
func (*AuthController) DoLogin(w http.ResponseWriter, r *http.Request) {
	email := r.PostFormValue("email")
	password := r.PostFormValue("password")

	if err := auth.Attempt(email, password, w, r); err == nil {
		flash.Success(w, r, "欢迎回来！")
		http.Redirect(w, r, "/", http.StatusFound)
	} else {
		view.RenderSimple(w, r, view.D{
			"Error":    err.Error(),
			"Email":    email,
			"Password": password,
		}, "auth.login")
	}
}

// Logout 退出登录
func (*AuthController) Logout(w http.ResponseWriter, r *http.Request) {
	auth.Logout(w, r)
	flash.Success(w, r, "您已退出登录")
	http.Redirect(w, r, "/", http.StatusFound)
}
