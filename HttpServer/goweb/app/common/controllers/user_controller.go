package controllers

import (
	"fmt"
	"goweb/app/blog/models/article"
	"goweb/app/common/models/user"
	"goweb/pkg/view"
	"net/http"
)

// UserController 用户控制器
type UserController struct {
	BaseController
}

// Show 获取用户所有文章
func (uc *UserController) Show(w http.ResponseWriter, r *http.Request) {

	// 1. 获取 URL 参数
	id := r.URL.Query().Get("id")

	// 2. 读取对应的文章数据
	_user, err := user.Get(id)

	// 3. 如果出现错误
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, "用户不存在")
	} else {
		// ---  4. 读取成功，显示用户文章列表 ---
		articles, err := article.GetByUserID(_user.GetStringID())
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprint(w, "404 文章未找到")
		} else {
			view.Render(w, r, view.D{
				"Articles": articles,
			}, "blog.articles.index", "blog.articles._article_meta")
		}
	}
}

// Authors 获取所有用户文章条数
func (uc *UserController) Authors(w http.ResponseWriter, r *http.Request) {
	Authors, _ := user.GetAllUserArtcileLength()
	view.Render(w, r, view.D{"Authors": Authors}, "blog.articles.authors")
}
