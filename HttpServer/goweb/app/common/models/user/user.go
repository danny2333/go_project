package user

import (
	"goweb/app/common/models"
	"goweb/pkg/password"
)

// User ...
type User struct {
	models.BaseModel

	Name     string
	Email    string
	Password string

	PasswordComfirm string
}

// ComparePassword 对比密码是否匹配
func (u User) ComparePassword(_password string) bool {
	return password.CheckHash(_password, u.Password)
}

// CreatedAtDate 创建日期
func (u User) CreatedAtDate() string {
	return u.CreatedAt.Format("2006-01-02")
}
