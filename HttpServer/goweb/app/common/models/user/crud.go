package user

import (
	"goweb/pkg/dbconn"
	"goweb/pkg/password"
	"goweb/pkg/types"
	"time"
)

// Create 创建用户，通过 User.ID 来判断是否创建成功
func (u *User) Create() (user User, err error) {
	statement := "insert into users (name, email, password, created_at) values (?, ?, ?, ?)"
	stmt, err := dbconn.DB.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	res, err := stmt.Exec(u.Name, u.Email, password.Hash(u.Password), time.Now())
	if err != nil {
		return
	}

	// 获得插入的ID
	id, err := res.LastInsertId()
	if err != nil {
		return
	}
	err = dbconn.DB.QueryRow("SELECT id, name, email, password, created_at, updated_at FROM users WHERE id = ?", id).Scan(&u.ID, &u.Name, &u.Email, &u.Password, &u.CreatedAt, &u.UpdatedAt)

	return
}

// GetByEmail 通过 Email 来获取用户
func GetByEmail(email string) (User, error) {
	user := User{}
	err := dbconn.DB.QueryRow("SELECT id, name, email, password, created_at FROM users WHERE email = ?", email).
		Scan(&user.ID, &user.Name, &user.Email, &user.Password, &user.CreatedAt)
	return user, err
}

// Get 通过 ID 获取用户
func Get(idstr string) (User, error) {
	user := User{}
	err := dbconn.DB.QueryRow("SELECT id, name, email, password, created_at FROM users WHERE id = ?", idstr).
		Scan(&user.ID, &user.Name, &user.Email, &user.Password, &user.CreatedAt)
	return user, err
}

// GetUserByID 通过id获取用户
func GetUserByID(id uint64) User {
	idstr := types.Uint64ToString(id)
	u, err := Get(idstr)
	if err != nil {
		return User{}
	}
	return u
}

// DeleteByID 通过 ID 删除用户
func (u *User) DeleteByID() (err error) {
	statement := "delete from users where id = ?"
	stmt, err := dbconn.DB.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(u.ID)
	return
}

// Update user information in the database
func (user *User) Update() (err error) {
	statement := "update users set name = ?, email = ? where id = ?"
	stmt, err := dbconn.DB.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(user.ID, user.Name, user.Email)
	return
}

// UserDeleteAll Delete all users from database
func UserDeleteAll() (err error) {
	statement := "delete from users"
	_, err = dbconn.DB.Exec(statement)
	return
}

// Users Get all users in the database and returns it
func Users() (users []User, err error) {
	rows, err := dbconn.DB.Query("SELECT id, name, email, password, created_at, updated_at FROM users")
	if err != nil {
		return
	}
	for rows.Next() {
		user := User{}
		if err = rows.Scan(&user.ID, &user.Name, &user.Email, &user.Password, &user.CreatedAt, &user.UpdatedAt); err != nil {
			return
		}
		users = append(users, user)
	}
	rows.Close()
	return
}

// GetAllUserArtcileLength 获取用户文章数据条数
func GetAllUserArtcileLength() ([]interface{}, error) {
	cates := []interface{}{}
	var id uint64
	var name string
	var count int
	sql := `
	SELECT
		a.user_id,
		u.name,
		COUNT( a.id ) AS count 
	FROM
		articles AS a
		INNER JOIN users AS u ON a.user_id = u.id 
	GROUP BY
		a.user_id	
	`
	rows, err := dbconn.DB.Query(sql)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		c := make(map[string]interface{})
		if err = rows.Scan(&id, &name, &count); err != nil {
			return nil, err
		}
		c["id"] = id
		c["name"] = name
		c["count"] = count
		cates = append(cates, c)
	}
	rows.Close()

	return cates, nil
}
