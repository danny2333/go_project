package models

import (
	"goweb/pkg/types"
	"time"
)

const TimeFormatStr string = "2006-01-02 15:04:05"

// BaseModel 模型基类
type BaseModel struct {
	ID        uint64
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (a BaseModel) GetStringID() string {
	return types.Uint64ToString(a.ID)
}
