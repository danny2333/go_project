package requests

import (
	"goweb/app/common/models/user"
	"goweb/pkg/validator"
)

func ValidateRegistrationForm(data user.User) error {
	if err := validator.New().
		Validate("Name", validator.StrLength(data.Name, 3, 20)).
		Validate("Name", validator.NotEmpty(data.Name)).
		Validate("Email", validator.NotEmpty(data.Email)).
		Validate("Email", validator.Email(data.Email)).
		Validate("Email", validator.StrLength(data.Email, 4, 30)).
		Validate("Password", validator.StrLength(data.Password, 6, 10)).
		Validate("Password", validator.NotEmpty(data.Password)).
		Validate("PasswordComfirm", validator.PasswordComfirm(data.Password, data.PasswordComfirm)).Err(); err != nil {
		return err
	}
	return nil
}
