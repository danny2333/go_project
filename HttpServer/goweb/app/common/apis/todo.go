package apis

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"
)

type Todo struct {
	ID      string `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

type todoHandlers struct {
	sync.Mutex
	store map[string]Todo
}

func NewTodoHandlers() *todoHandlers {
	return &todoHandlers{
		store: map[string]Todo{},
	}
}

func (h *todoHandlers) Todo(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.String(), "/")
	if len(parts) != 3 {
		w.WriteHeader(http.StatusFound)
		return
	}

	switch r.Method {
	case "GET":
		h.getTodo(w, r)
		return
	case "PUT":
		h.updateTodo(w, r)
		return
	case "DELETE":
		h.deleteTodo(w, r)
		return
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}

func (h *todoHandlers) getTodo(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.String(), "/")
	fmt.Printf("%#v\n", parts)
	fmt.Printf("len=%v\n", len(parts))
	if len(parts) != 3 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if parts[2] == "random" {
		h.getRandomTodo(w, r)
		return
	}

	h.Lock()
	todo, ok := h.store[parts[2]]
	h.Unlock()
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	jsonBytes, err := json.Marshal(todo)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Add("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonBytes)
}

func (h *todoHandlers) getRandomTodo(w http.ResponseWriter, r *http.Request) {
	ids := make([]string, len(h.store))
	h.Lock()
	i := 0
	for id := range h.store {
		ids[i] = id
		i++
	}
	defer h.Unlock()

	var target string
	if len(ids) == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if len(ids) == 1 {
		target = ids[0]
	} else {
		rand.Seed(time.Now().UTC().UnixNano())
		target = ids[rand.Intn(len(ids)-1)]
	}

	w.Header().Add("location", fmt.Sprintf("/todo/%s", target))
	w.WriteHeader(http.StatusFound)
}

func (h *todoHandlers) updateTodo(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.String(), "/")
	contentType := r.Header.Get("content-type")
	if contentType != "application/json" {
		w.WriteHeader(http.StatusUnsupportedMediaType)
		w.Write([]byte(fmt.Sprintf("required content-type 'application/json', but got '%s'", contentType)))
		return
	}

	bodyBytes, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	h.Lock()
	todo, ok := h.store[parts[2]]
	h.Unlock()
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	err = json.Unmarshal(bodyBytes, &todo)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	h.Lock()
	h.store[todo.ID] = todo
	defer h.Unlock()
}

func (h *todoHandlers) deleteTodo(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.String(), "/")

	h.Lock()
	delete(h.store, parts[2])
	h.Unlock()
}

func (h *todoHandlers) Todos(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		h.getTodos(w, r)
		return
	case "POST":
		h.createTodo(w, r)
		return
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("method not allowed"))
		return
	}
}

func (h *todoHandlers) getTodos(w http.ResponseWriter, r *http.Request) {
	todos := make([]Todo, len(h.store))

	h.Lock()
	i := 0
	for _, todo := range h.store {
		todos[i] = todo
		i++
	}
	h.Unlock()

	jsonBytes, err := json.Marshal(todos)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Add("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonBytes)

}

func (h *todoHandlers) createTodo(w http.ResponseWriter, r *http.Request) {
	bodyBytes, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	contentType := r.Header.Get("content-type")
	if contentType != "application/json" {
		w.WriteHeader(http.StatusUnsupportedMediaType)
		w.Write([]byte(fmt.Sprintf("required content-type 'application/json', but got '%s'", contentType)))
		return
	}

	var todo Todo
	err = json.Unmarshal(bodyBytes, &todo)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	todo.ID = fmt.Sprintf("%d", time.Now().UnixNano())

	h.Lock()
	h.store[todo.ID] = todo
	defer h.Unlock()

}
