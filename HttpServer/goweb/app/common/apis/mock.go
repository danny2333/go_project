package apis

import (
	"goweb/pkg/common"
	"io/ioutil"
	"net/http"
	"strings"
)

// 比如我们需要一个接口请求地址为 user/info
// 首先直接访问 http://localhost:8000/mock/user/info
// 在data目录下会生成一个api_user_info.json的文本，直接修改即可

// MockApi ...
func MockApi(w http.ResponseWriter, r *http.Request) {
	// fileName := "data/api" + strings.Replace(r.URL.Path, "/", "_", -1) + ".json"

	if len(strings.Split(r.URL.Path, "/")) != 4 {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error"))
		return
	}

	jsonBytes1 := []byte(`{
		"error": 200,
		"message": "success",
		"data": []
	}`)

	fileName := "resources/public/test/api_" + strings.Split(r.URL.Path, "/")[2] + "_" + strings.Split(r.URL.Path, "/")[3] + ".json"
	if common.CheckFileIsExist(fileName) {
		jsonBytes, err := ioutil.ReadFile(fileName)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		jsonBytes1 = jsonBytes
	} else {
		ioutil.WriteFile(fileName, jsonBytes1, 0644)
	}

	w.Header().Add("content-type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonBytes1)
}
