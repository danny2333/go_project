package apis

import (
	"fmt"
	"goweb/pkg/websocket"
	"net/http"
)

// serveWs2 .
func serveWs2(pool *websocket.Pool, w http.ResponseWriter, r *http.Request) {
	fmt.Println("WebSocket Endpoint Hit")

	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+V\n", err)
	}

	client := &websocket.Client{
		Conn: conn,
		Pool: pool,
	}

	pool.Register <- client
	client.Read()
}

// setupRoutes .
func setupRoutes() {
	// send {"type":1,"body":"{\"type\":1,\"body\":\"王尼玛\"}"}
	http.HandleFunc("/wschat", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Simple wschat Server")
	})

	pool := websocket.NewPool()
	go pool.Start()

	http.HandleFunc("/wschatws", func(w http.ResponseWriter, r *http.Request) {
		serveWs2(pool, w, r)
	})
}
