package apis

import "net/http"

func RegisterRouter() {
	http.HandleFunc("/wspage", serveHome)
	http.HandleFunc("/ws", serveWs)

	// mock
	http.HandleFunc("/mock/", MockApi)

	todoHandle := NewTodoHandlers()
	http.HandleFunc("/common/api/todo/", todoHandle.Todo)
	http.HandleFunc("/common/api/todo", todoHandle.Todos)

	// websocket chat
	setupRoutes()
}
