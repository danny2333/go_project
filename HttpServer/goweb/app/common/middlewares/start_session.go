package middlewares

import (
	"goweb/app"
	"net/http"
)

// StartSession 强制标头返回 HTML 内容类型
/* func StartSession(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// 1. 启动会话
		app.GlobalSession().SessionStart(w, r)

		// 2. 继续处理接下去的请求
		next.ServeHTTP(w, r)
	})
} */

type StartSessionMiddleWare struct {
	Next http.Handler
}

func (c StartSessionMiddleWare) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if c.Next == nil {
		c.Next = http.DefaultServeMux
	}

	// 1. 启动会话
	app.GlobalSession().SessionStart(w, r)

	c.Next.ServeHTTP(w, r)
}
