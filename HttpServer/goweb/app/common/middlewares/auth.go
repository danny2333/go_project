package middlewares

import (
	"goweb/pkg/auth"
	"goweb/pkg/flash"
	"net/http"
)

// Auth 登录用户才可访问
// 方法 http.HandleFunc("/thread/new", Guest(NewThread))
func Auth(next HttpHandlerFunc) HttpHandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		if !auth.Check(w, r) {
			flash.Warning(w, r, "登录用户才能访问此页面")
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		next(w, r)
	}
}
