package middlewares

import (
	"net/http"
	"strings"
)

// RemoveTrailingSlash 除首页以外，移除所有请求路径后面的斜杆
/* func RemoveTrailingSlash(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// 1. 除首页以外，移除所有请求路径后面的斜杆
		if r.URL.Path != "/" {
			r.URL.Path = strings.TrimSuffix(r.URL.Path, "/")
		}

		// 2. 将请求传递下去
		next.ServeHTTP(w, r)
	})
} */

// RemoveTrailingSlashMiddleWare 除首页以外，移除所有请求路径后面的斜杆
type RemoveTrailingSlashMiddleWare struct {
	Next http.Handler
}

func (c RemoveTrailingSlashMiddleWare) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if c.Next == nil {
		c.Next = http.DefaultServeMux
	}

	// 1. 除首页以外，移除所有请求路径后面的斜杆 内部
	if r.URL.Path != "/" {
		r.URL.Path = strings.TrimSuffix(r.URL.Path, "/")
	}

	c.Next.ServeHTTP(w, r)
}
