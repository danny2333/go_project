package middlewares

import (
	"context"
	"goweb/pkg/common"
	"net/http"
	"time"
)

type TimeoutMiddleWare struct {
	Next http.Handler
}

func (t *TimeoutMiddleWare) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if t.Next == nil {
		t.Next = http.DefaultServeMux
	}

	ctx, cancel := context.WithTimeout(r.Context(), time.Second*5)
	defer cancel()
	r.WithContext(ctx)
	ch := make(chan struct{})
	go func() {
		t.Next.ServeHTTP(w, r)
		ch <- struct{}{}
	}()
	select {
	case <-ch:
		return
	case <-ctx.Done():
		w.WriteHeader(http.StatusRequestTimeout)
		common.Info("[Request handle time out!]")
	}
	ctx.Done()
}
