package category

import "goweb/app/common/models"

// Category 文章分类模型
type Category struct {
	models.BaseModel

	Name string
}

// CreatedAtDate 创建日期
func (c Category) CreatedAtDate() string {
	return c.CreatedAt.Format("2006-01-02")
}