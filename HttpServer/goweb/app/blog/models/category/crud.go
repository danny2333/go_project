package category

import (
	"goweb/pkg/dbconn"
	"goweb/pkg/types"
	"time"
)

// Create 创建分类，通过 category.ID 来判断是否创建成功
func (category *Category) Create() (err error) {
	statement := "insert into categories (name, created_at, updated_at) values (?, ?, ?)"
	stmt, err := dbconn.DB.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	res, err := stmt.Exec(category.Name, time.Now(), time.Now())
	if err != nil {
		return
	}
	// 获得插入的ID
	_, err = res.LastInsertId()
	return err
}

func (category *Category) CheckIsExit() bool {
	var count int
	dbconn.DB.QueryRow("SELECT count(*) FROM categories WHERE name = ?", category.Name).
		Scan(&count)
	return count > 0
}

// All 获取分类数据
func All() ([]Category, error) {
	var categorys []Category

	rows, err := dbconn.DB.Query("SELECT id, name, created_at FROM categories")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		category := Category{}
		if err = rows.Scan(&category.ID, &category.Name, &category.CreatedAt); err != nil {
			return nil, err
		}
		categorys = append(categorys, category)
	}
	rows.Close()

	return categorys, nil
}

// GetAllCateArtcileLength 获取分类数据条数
func GetAllCateArtcileLength() ([]interface{}, error) {
	cates := []interface{}{}
	var id uint64
	var name string
	var count int

	rows, err := dbconn.DB.Query("SELECT a.category_id, c.`name`, COUNT(a.id) AS count FROM articles AS a INNER JOIN categories AS c ON a.category_id=c.id GROUP BY a.category_id")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		c := make(map[string]interface{})
		if err = rows.Scan(&id, &name, &count); err != nil {
			return nil, err
		}
		c["id"] = id
		c["name"] = name
		c["count"] = count
		cates = append(cates, c)
	}
	rows.Close()

	return cates, nil
}

// Get 通过 ID 获取分类
func Get(idstr string) (Category, error) {
	var category Category

	id := types.StringToInt(idstr)
	err := dbconn.DB.QueryRow("SELECT id, name, created_at FROM categories WHERE id = ?", id).
		Scan(&category.ID, &category.Name, &category.CreatedAt)
	return category, err

}

// GetCateByID 通过id获取分类
func GetCateNameByID(id uint64) string {
	idstr := types.Uint64ToString(id)
	c, err := Get(idstr)
	if err != nil {
		return idstr
	}
	return c.Name
}
