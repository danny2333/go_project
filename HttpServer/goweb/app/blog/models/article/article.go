package article

import (
	"goweb/app/common/models"
	"goweb/pkg/types"
)

// Article 文章模型
type Article struct {
	models.BaseModel

	Title      string
	Body       string
	UserID     uint64
	CategoryID uint64
}

// GetStringID 获取 ID 的字符串格式
func (a Article) GetStringID() string {
	return types.Uint64ToString(a.ID)
}

// CreatedAtDate 创建日期
func (a Article) CreatedAtDate() string {
	return a.CreatedAt.Format("2006-01-02")
}
