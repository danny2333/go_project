package article

import (
	"goweb/pkg/dbconn"
	"goweb/pkg/types"
	"net/http"
	"time"
)

// Get 通过 ID 获取文章
func Get(idstr string) (Article, error) {
	var article Article
	id := types.StringToInt(idstr)
	err := dbconn.DB.QueryRow("SELECT id, user_id, title, body, category_id, created_at, updated_at FROM articles WHERE id = ?", id).
		Scan(&article.ID, &article.UserID, &article.Title, &article.Body, &article.CategoryID, &article.CreatedAt, &article.UpdatedAt)
	return article, err
}

// GetAll 获取全部文章
func GetAll(r *http.Request, perPage int) ([]Article, error) {
	var articles []Article
	rows, err := dbconn.DB.Query("SELECT id, user_id, title, body, category_id, created_at, updated_at FROM articles LIMIT ?, 10", perPage)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		article := Article{}
		if err = rows.Scan(&article.ID, &article.UserID, &article.Title, &article.Body, &article.CategoryID, &article.CreatedAt, &article.UpdatedAt); err != nil {
			return nil, err
		}
		articles = append(articles, article)
	}
	rows.Close()
	return articles, nil
}

// Create 创建文章，通过 article.ID 来判断是否创建成功
func (article *Article) Create() (err error) {
	statement := "insert into articles (user_id, title, body, category_id, created_at, updated_at) values (?, ?, ?, ?, ?, ?)"
	stmt, err := dbconn.DB.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	res, err := stmt.Exec(article.UserID, article.Title, article.Body, article.CategoryID, time.Now(), time.Now())
	if err != nil {
		return
	}

	// 获得插入的ID
	_, err = res.LastInsertId()
	return err
}

// Update 更新文章
func (article *Article) Update() (rowsAffected int64, err error) {
	statement := "update articles set title = ?, body = ?, category_id = ? where id = ?"
	stmt, err := dbconn.DB.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	res, err := stmt.Exec(article.Title, article.Body, article.CategoryID, article.ID)
	if err != nil {
		return
	}
	rowsAffected, err = res.RowsAffected()
	return
}

// Delete 删除文章
func (article *Article) Delete() (rowsAffected int64, err error) {
	statement := "delete from articles where id = ?"
	stmt, err := dbconn.DB.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	res, err := stmt.Exec(article.ID)
	if err != nil {
		return
	}
	rowsAffected, err = res.RowsAffected()
	return
}

// GetByUserID 获取全部文章
func GetByUserID(uid string) ([]Article, error) {
	var articles []Article

	rows, err := dbconn.DB.Query("SELECT id, user_id, title, body, category_id, created_at, updated_at FROM articles WHERE user_id = ?", uid)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		article := Article{}
		if err = rows.Scan(&article.ID, &article.UserID, &article.Title, &article.Body, &article.CategoryID, &article.CreatedAt, &article.UpdatedAt); err != nil {
			return nil, err
		}
		articles = append(articles, article)
	}
	rows.Close()

	return articles, nil
}

// GetByCategoryID 获取分类相关的文章
func GetByCategoryID(cid string, perPage int) ([]Article, error) {
	var articles []Article

	rows, err := dbconn.DB.Query("SELECT id, user_id, title, body, category_id, created_at, updated_at FROM articles WHERE category_id = ? LIMIT ?, 10", cid, perPage)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		article := Article{}
		if err = rows.Scan(&article.ID, &article.UserID, &article.Title, &article.Body, &article.CategoryID, &article.CreatedAt, &article.UpdatedAt); err != nil {
			return nil, err
		}
		articles = append(articles, article)
	}
	rows.Close()

	return articles, nil
}
