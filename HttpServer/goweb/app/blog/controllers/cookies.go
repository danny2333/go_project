package controllers

import (
	"html/template"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

func Run() {
	http.HandleFunc("/", getCookie)
	http.HandleFunc("/login", login)
	http.HandleFunc("/1", cookie)
	http.HandleFunc("/2", cookie2)
	http.ListenAndServe(":8081", nil)
}

func login(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		const tmpl = `
		<div>
			<form action="/login" method="post">
				username:<br>
				<input type="text" name="username" value=""><br><br>
				password:<br>
				<input type="password" name="password" value=""><br><br>
				Description:<br>
				<input type="submit" value="Submit">
			</form>
		</div>`

		t, err := template.New("login").Parse(tmpl)
		if err != nil {
			log.Fatal(err)
		}

		t.Execute(w, nil)
	} else if r.Method == "POST" {
		r.ParseForm()
		username := r.FormValue("username")
		password := r.FormValue("password")

		if username == "admin" && password == "123456" {
			ck := &http.Cookie{
				Name:  "mycookie",
				Value: username,
				Path:  "/",
				// Domain: "localhost",
				// MaxAge: 120, // 120秒
				HttpOnly: true,
			}

			http.SetCookie(w, ck)
			http.Redirect(w, r, "/", 302)
		}
		http.Redirect(w, r, "/login", 302)
	}
}

func logout(w http.ResponseWriter, r *http.Request) {
	c, err := r.Cookie("mycookie")
	if err != nil {
		log.Fatal(err)
	}
	c.Name = "Deleted"
	c.Value = "Unuse"
	c.Expires = time.Unix(1414414788, 1414414788000)
	http.Redirect(w, r, "/login", http.StatusSeeOther)
}

func getCookie(w http.ResponseWriter, r *http.Request) {
	// 获取cookie值r.Cookie 刷新页面
	c, err := r.Cookie("mycookie")
	if err != nil {
		w.Write([]byte(err.Error()))
		log.Fatal(err)
		return
	}

	if time.Now().After(c.Expires) {
		log.Println("Cookie is expired.")
	}

	w.Write([]byte(c.Value))
}

func cookie(w http.ResponseWriter, r *http.Request) {
	ck := &http.Cookie{
		Name:   "mycookie",
		Value:  "Hello",
		Path:   "/",
		Domain: "localhost",
		MaxAge: 120, // 120秒
	}

	http.SetCookie(w, ck)

	// 获取cookie值r.Cookie 刷新页面
	ck2, err := r.Cookie("mycookie")
	if err != nil {
		w.Write([]byte(err.Error()))
		log.Fatal(err)
		return
	}

	w.Write([]byte(ck2.Value))
}

func cookie2(w http.ResponseWriter, r *http.Request) {
	ck := &http.Cookie{
		Name:   "mycookie",
		Value:  "hello world", //注意在这里空格是非法字符,常规处理替换空格
		Path:   "/2",
		Domain: "localhost",
		MaxAge: 120, //120秒
	}
	//ck.String -----ck 转化string
	// w.Header().Set("set-cookie", ck.String())//name,value中不含有空格
	w.Header().Set("Set-Cookie", strings.Replace(ck.String(), " ", "%20", -1)) //处理name，value中含有空格的办法。
	ck2, err := r.Cookie("mycookie")
	if err != nil {
		io.WriteString(w, err.Error())
		log.Fatal(err)
		return
	}
	io.WriteString(w, ck2.Value)
}
