package controllers

import (
	"goweb/pkg/common"
	"goweb/pkg/view"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"sort"
)

const (
	// UploadDir 上传文件目录
	UploadDir = "./resources/uploads/uploadimg"
)

// ByModTime type
type ByModTime []os.FileInfo

func (fis ByModTime) Len() int {
	return len(fis)
}

func (fis ByModTime) Swap(i, j int) {
	fis[i], fis[j] = fis[j], fis[i]
}

func (fis ByModTime) Less(i, j int) bool {
	return fis[i].ModTime().Before(fis[j].ModTime())
}

type UploadImgController struct{}

func (u *UploadImgController) UploadImg(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		view.Render(w, r, view.D{}, "blog.uploadimg.upload")
	}

	if r.Method == http.MethodPost {
		f, h, err := r.FormFile("image")
		common.CheckErr(err)

		// 获取文件后缀 包括点
		extStr := path.Ext(h.Filename)
		defer f.Close()

		// t, err := ioutil.TempFile(UploadDir, filename) //创建临时文件修改为 创建普通文件
		t, err := os.OpenFile(UploadDir+"/"+common.UniqueID()+extStr, os.O_WRONLY|os.O_CREATE, 0666)
		common.CheckErr(err)
		defer t.Close()

		_, err = io.Copy(t, f)
		common.CheckErr(err)

		// http.Redirect(w, r, "/blog/viewimg?id="+filename, http.StatusFound)
		http.Redirect(w, r, "/blog/imglist", http.StatusFound)
	}
}

func (u *UploadImgController) ViewImg(w http.ResponseWriter, r *http.Request) {
	imageID := r.FormValue("id")
	imagePath := UploadDir + "/" + imageID
	if exists := common.CheckFileIsExist(imagePath); !exists {
		http.NotFound(w, r)
		return
	}
	w.Header().Set("Content-Type", "image")
	http.ServeFile(w, r, imagePath)
}

func (u *UploadImgController) ImgList(w http.ResponseWriter, r *http.Request) {
	fileInfoArr, err := ioutil.ReadDir(UploadDir)
	common.CheckErr(err)

	// data := make(map[string]interface{})
	images := []string{}
	// sort.Reverse 按最新时间排序。
	sort.Sort(sort.Reverse(ByModTime(fileInfoArr)))
	for _, fileInfo := range fileInfoArr {
		images = append(images, fileInfo.Name())
	}

	// data["images"] = images
	// common.GenerateHTML(w, data, "resources/views/pinduck/", "layout", "imglist")
	view.Render(w, r, view.D{"images": images}, "blog.uploadimg.imglist")

}
