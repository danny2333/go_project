package controllers

import (
	"fmt"
	"goweb/app/blog/models/article"
	"goweb/app/blog/models/category"
	"goweb/app/blog/requests"
	"goweb/pkg/flash"
	"goweb/pkg/view"
	"net/http"
	"strconv"
)

// CategoriesController 文章分类控制器
type CategoriesController struct {
}

// Create 文章分类创建页面
func (*CategoriesController) Create(w http.ResponseWriter, r *http.Request) {
	view.Render(w, r, view.D{}, "blog.categories.create")
}

// Store 保存文章分类
func (*CategoriesController) Store(w http.ResponseWriter, r *http.Request) {
	_category := category.Category{
		Name: r.PostFormValue("name"),
	}

	if errors := requests.ValidateCategoryForm(_category); errors != nil {
		view.Render(w, r, view.D{
			"Category": _category,
			"Errors":   errors,
		}, "blog.categories.create")
	} else {
		err := _category.Create()
		if err == nil {
			// fmt.Fprint(w, "创建成功！")
			// http.Redirect(w, r, "/blog/categories/show?id="+_category.GetStringID(), http.StatusFound)
			flash.Success(w, r, "创建成功！")
			http.Redirect(w, r, "/blog", http.StatusFound)
		} else {
			// w.WriteHeader(http.StatusInternalServerError)
			// fmt.Fprint(w, "创建文章分类失败，请联系管理员")
			flash.Warning(w, r, "创建文章分类失败，请联系管理员")
			http.Redirect(w, r, "/blog", http.StatusFound)
		}
	}
}

// Show 显示分类下的文章列表
func (cc *CategoriesController) Show(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")

	page := r.URL.Query().Get("page")
	if page == "" {
		page = "0"
	}
	pageInt, _ := strconv.Atoi(page)

	_category, err := category.Get(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, "分类不存在")
	} else {
		articles, err := article.GetByCategoryID(_category.GetStringID(), pageInt)

		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprint(w, "404 文章未找到")
		} else {
			view.Render(w, r, view.D{
				"Articles":  articles,
				"PagerData": nil,
			}, "blog.articles.index", "blog.articles._article_meta")
		}
	}
}

// Categories 获取所有分类对应文章数
func (cc *CategoriesController) Categories(w http.ResponseWriter, r *http.Request) {
	categories, _ := category.GetAllCateArtcileLength()
	view.Render(w, r, view.D{"Cate": categories}, "blog.articles.categories")
}
