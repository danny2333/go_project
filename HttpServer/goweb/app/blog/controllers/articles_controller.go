package controllers

import (
	"fmt"
	"goweb/app/blog/models/article"
	"goweb/app/blog/models/category"
	"goweb/app/blog/requests"
	"goweb/pkg/auth"
	"goweb/pkg/flash"
	"goweb/pkg/view"
	"net/http"
	"strconv"
	"text/template"
)

// ArticlesController 处理静态页面
type ArticlesController struct {
}

// About 关于我们页面
func (ac *ArticlesController) About(w http.ResponseWriter, r *http.Request) {
	view.Render(w, r, view.D{"desc": "祝你也能诚实地面对自己的欲望与厌恶"}, "blog.articles.about")
}

// Show 文章详情页面
func (ac *ArticlesController) Show(w http.ResponseWriter, r *http.Request) {

	// 1. 获取 URL 参数
	id := r.URL.Query().Get("id")

	// 2. 读取对应的文章数据
	article, err := article.Get(id)

	// 3. 如果出现错误
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, "404 文章未找到")
	} else {
		// ---  4. 读取成功，显示文章 ---
		view.Render(w, r, view.D{
			"Article": article,
			"CanModifyArticle": func() bool {
				return auth.User(w, r).ID == article.UserID
			},
		}, "blog.articles.show", "blog.articles._article_meta")
	}
}

// Archives 文章归档
func (ac *ArticlesController) Archives(w http.ResponseWriter, r *http.Request) {
	// SELECT DATE_FORMAT(created_at,'%Y-%m-%d') as created_ats, COUNT(id) FROM articles GROUP BY created_ats
	articles, _ := article.GetAll(r, 0)
	view.Render(w, r, view.D{"Articles": articles}, "blog.articles.archives")
}

// Index 文章列表页
func (ac *ArticlesController) Index(w http.ResponseWriter, r *http.Request) {
	page := r.URL.Query().Get("page")
	if page == "" {
		page = "0"
	}
	pageInt, _ := strconv.Atoi(page)
	articles, err := article.GetAll(r, pageInt)

	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, "404 文章未找到")
	} else {
		view.Render(w, r, view.D{
			"Articles":  articles,
			"PagerData": nil,
		}, "blog.articles.index", "blog.articles._article_meta")
	}
}

// Create 文章创建页面
func (*ArticlesController) Create(w http.ResponseWriter, r *http.Request) {
	view.Render(w, r, view.D{}, "blog.articles.create", "blog.articles._form_field")
}

// Store 文章创建页面
func (*ArticlesController) Store(w http.ResponseWriter, r *http.Request) {
	currentUser := auth.User(w, r)
	category, err := category.Get(r.PostFormValue("category_id"))
	if err != nil {
		flash.Warning(w, r, "分类不存在")
		http.Redirect(w, r, "/blog/articles/create", http.StatusFound)
	}
	_article := article.Article{
		Title:      r.PostFormValue("title"),
		Body:       template.HTMLEscapeString(r.PostFormValue("body")),
		CategoryID: category.ID,
		UserID:     currentUser.ID,
	}

	errors := requests.ValidateArticleForm(_article)

	// if len(errors) == 0 {
	if errors == nil {
		// 创建文章
		err := _article.Create()
		if err == nil {
			flash.Success(w, r, "创建成功！")
			http.Redirect(w, r, "/blog", http.StatusFound)
			// http.Redirect(w, r, "/blog/articles/show?id="+_article.GetStringID(), http.StatusFound)
		} else {
			// w.WriteHeader(http.StatusInternalServerError)
			// fmt.Fprint(w, "创建文章失败，请联系管理员")
			flash.Warning(w, r, "创建文章失败，请联系管理员")
			http.Redirect(w, r, "/blog/articles/create", http.StatusFound)
		}
	} else {
		view.Render(w, r, view.D{
			"Article": _article,
			"Errors":  errors,
		}, "blog.articles.create", "blog.articles._form_field")
	}
}

// Edit 文章更新页面
func (ac *ArticlesController) Edit(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")

	_article, err := article.Get(id)

	if err != nil {
		w.WriteHeader(http.StatusFound)
		fmt.Fprint(w, "文章不存在")
		// flash.Warning(w, r, "文章不存在")
		// http.Redirect(w, r, "/blog", http.StatusFound)
	} else {
		// 检查权限
		if _article.UserID != (auth.User(w, r)).ID {
			flash.Warning(w, r, "未授权操作！")
			http.Redirect(w, r, "/blog/articles/edit?id="+_article.GetStringID(), http.StatusFound)
		} else {
			view.Render(w, r, view.D{
				"Article": _article,
				"Errors":  view.D{},
			}, "blog.articles.edit", "blog.articles._form_field")
		}
	}
}

// Update 更新文章
func (ac *ArticlesController) Update(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")

	category, err := category.Get(r.PostFormValue("category_id"))
	if err != nil {
		flash.Warning(w, r, "分类不存在")
		http.Redirect(w, r, "/blog", http.StatusFound)
	}

	_article, err := article.Get(id)

	if err != nil {
		// w.WriteHeader(http.StatusNotFound)
		// fmt.Fprint(w, "404 文章未找到")
		flash.Warning(w, r, "更新文章失败，请联系管理员")
		http.Redirect(w, r, "/blog", http.StatusFound)
	} else {
		// 检查权限
		if _article.UserID != (auth.User(w, r)).ID {
			flash.Warning(w, r, "未授权操作！")
			http.Redirect(w, r, "/blog", http.StatusFound)
		} else {
			_article.Title = r.PostFormValue("title")
			_article.Body = template.HTMLEscapeString(r.PostFormValue("body"))
			_article.CategoryID = category.ID

			errors := requests.ValidateArticleForm(_article)

			if errors == nil {
				rowsAffected, err := _article.Update()
				if err != nil {
					// 数据库错误
					// w.WriteHeader(http.StatusInternalServerError)
					// fmt.Fprint(w, "500 服务器内部错误")
					// return
					flash.Warning(w, r, "更新文章失败，请联系管理员")
					http.Redirect(w, r, "/blog", http.StatusFound)
				}

				// √ 更新成功，跳转到文章详情页
				if rowsAffected > 0 {
					flash.Success(w, r, "更新成功")
					http.Redirect(w, r, "/blog/articles/show?id="+_article.GetStringID(), http.StatusFound)
				} else {
					// fmt.Fprint(w, "您没有做任何更改！")
					http.Redirect(w, r, "/blog/articles/show?id="+_article.GetStringID(), http.StatusFound)
				}
			} else {
				view.Render(w, r, view.D{
					"Article": _article,
					"Errors":  errors,
				}, "blog.articles.edit", "blog.articles._form_field")
			}
		}
	}
}

// Delete 删除文章
func (ac *ArticlesController) Delete(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")

	_article, err := article.Get(id)

	if err != nil {
		// w.WriteHeader(http.StatusNotFound)
		// fmt.Fprint(w, "404 文章未找到")
		flash.Warning(w, r, "删除文章失败，请联系管理员")
		http.Redirect(w, r, "/blog", http.StatusFound)
	} else {
		// 检查权限
		if _article.UserID != (auth.User(w, r)).ID {
			flash.Warning(w, r, "未授权操作！")
			http.Redirect(w, r, "/blog", http.StatusFound)
		} else {
			rowsAffected, err := _article.Delete()
			if err != nil {
				// 应该是 SQL 报错了
				// w.WriteHeader(http.StatusInternalServerError)
				// fmt.Fprint(w, "500 服务器内部错误")
				flash.Warning(w, r, "删除文章失败，请联系管理员")
				http.Redirect(w, r, "/blog", http.StatusFound)
			} else {
				// 未发生错误
				if rowsAffected > 0 {
					// 重定向到文章列表页
					http.Redirect(w, r, "/blog", http.StatusFound)
				} else {
					// w.WriteHeader(http.StatusNotFound)
					// fmt.Fprint(w, "404 文章未找到")
					flash.Warning(w, r, "删除文章失败，请联系管理员")
					http.Redirect(w, r, "/blog", http.StatusFound)
				}
			}
		}
	}
}
