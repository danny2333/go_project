package requests

import (
	"goweb/app/blog/models/article"
	"goweb/pkg/validator"
)

func ValidateArticleForm(data article.Article) error {
	if err := validator.New().
		Validate("Title", validator.StrLength(data.Title, 3, 40)).
		Validate("Title", validator.NotEmpty(data.Title)).
		Validate("Body", validator.StrLength(data.Body, 10, 100000)).
		Validate("Body", validator.NotEmpty(data.Body)).Err(); err != nil {
		return err
	}
	return nil
}
