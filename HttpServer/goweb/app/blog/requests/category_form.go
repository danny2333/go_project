package requests

import (
	"fmt"
	"goweb/app/blog/models/category"
	"goweb/pkg/validator"
)

func ValidateCategoryForm(data category.Category) error {
	if err := validator.New().
		Validate("Name", validator.StrLength(data.Name, 2, 8), func() error {
			if data.CheckIsExit() {
				return fmt.Errorf("分类名已存在")
			}
			return nil
		}).
		Validate("Name", validator.NotEmpty(data.Name)).Err(); err != nil {
		return err
	}
	return nil
}
