package controllers

import (
	"goweb/pkg/common"
	"net/http"
)

type baseController struct{}

type AppController struct {
	baseController
}

func RegisterRouter() {
	a := new(AppController)
	http.HandleFunc("/webim", a.Home)
	http.HandleFunc("/webim/join", a.Join)

	l := new(LongPollingController)
	http.HandleFunc("/webim/lp", l.Join)
	http.HandleFunc("/webim/lp/post", l.Post)
	http.HandleFunc("/webim/lp/fetch", l.Fetch)

	w := new(WebSocketController)
	http.HandleFunc("/webim/ws", w.Get)
	http.HandleFunc("/webim/ws/join", w.Join)

	chatRoute()

	chatRoute2()
}

func (a *AppController) Home(w http.ResponseWriter, r *http.Request) {
	common.GenerateHTML(w, nil, "resources/views/webim/", "layout", "welcome")
}

func (a *AppController) Join(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		uname := r.PostFormValue("uname")
		tech := r.PostFormValue("tech")

		if len(uname) == 0 {
			http.Redirect(w, r, "/webim", http.StatusFound)
		}

		switch tech {
		case "longpolling":
			http.Redirect(w, r, "/webim/lp?uname="+uname, http.StatusFound)
		case "websocket":
			http.Redirect(w, r, "/webim/ws?uname="+uname, http.StatusFound)
		default:
			http.Redirect(w, r, "/webim", http.StatusFound)
		}

		return
	}
}
