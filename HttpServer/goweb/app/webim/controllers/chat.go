package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

//========== ChatMessage =============//

type ChatMessage struct {
	UserName  string `json:"userName"`
	Body      string `json:"body"`
	Timestamp string `json:"timestamp"`
}

func (self *ChatMessage) String() string {
	return self.UserName + " at " + self.Timestamp + " says " + self.Body
}

//============ ChatUser ================//

const channelBufSize = 100

var maxId int = 0

type ChatUser struct {
	id              int
	conn            *websocket.Conn
	server          *ChatServer
	outgoingMessage chan *ChatMessage
	doneCh          chan bool
}

func NewUser(conn *websocket.Conn, server *ChatServer) *ChatUser {
	if conn == nil {
		panic("connection cannot be nil")
	}
	if server == nil {
		panic(" Server cannot be nil")
	}

	maxId++
	ch := make(chan *ChatMessage, channelBufSize)
	doneCh := make(chan bool)
	log.Println("Done creating new User")
	return &ChatUser{maxId, conn, server, ch, doneCh}
}

func (user *ChatUser) Conn() *websocket.Conn {
	return user.conn
}

func (user *ChatUser) Write(message *ChatMessage) {
	select {
	case user.outgoingMessage <- message:
	default:
		user.server.RemoveUser(user)
		err := fmt.Errorf("User %d is disconnected.", user.id)
		user.server.Err(err)

	}
}

func (user *ChatUser) Done() {
	user.doneCh <- true
}

func (user *ChatUser) Listen() {
	go user.listenWrite()
	user.listenRead()
}

func (user *ChatUser) listenWrite() {
	log.Println("Listening to write to client")

	for {
		select {
		//send message to user
		case msg := <-user.outgoingMessage:
			//  log.Println("send in listenWrite for user :",user.id, msg)
			if err := user.conn.WriteJSON(&msg); err != nil {
				log.Println(err)
			}

			// receive done request
		case <-user.doneCh:
			log.Println("Done Channel for user:")
			user.server.RemoveUser(user)
			user.doneCh <- true
			return

		}
	}
}

func (user *ChatUser) listenRead() {
	//log.Println("Listening to Read to client")
	for {
		select {
		//receive Done request
		case <-user.doneCh:
			user.server.RemoveUser(user)
			user.doneCh <- true
			return

		// read data from websocket connection
		default:
			var messageObject ChatMessage
			err := user.conn.ReadJSON(&messageObject)

			if err != nil {
				user.doneCh <- true
				log.Println("Error while reading JSON from websocket ", err.Error())
				user.server.Err(err)
			} else {
				user.server.ProcessNewIncomingMessage(&messageObject)
			}

		}
	}
}

// =============== ChatServer =========== //
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type ChatServer struct {
	connectedUsers     map[int]*ChatUser
	Messages           []*ChatMessage `json:"messages"`
	addUser            chan *ChatUser
	removeUser         chan *ChatUser
	newIncomingMessage chan *ChatMessage
	errorChannel       chan error
	doneCh             chan bool
}

func NewChatServer() *ChatServer {
	Messages := []*ChatMessage{}
	connectedUsers := make(map[int]*ChatUser)
	addUser := make(chan *ChatUser)
	removeUser := make(chan *ChatUser)
	newIncomingMessage := make(chan *ChatMessage)
	errorChannel := make(chan error)
	doneCh := make(chan bool)

	return &ChatServer{
		connectedUsers,
		Messages,
		addUser,
		removeUser,
		newIncomingMessage,
		errorChannel,
		doneCh,
	}
}

func (server *ChatServer) AddUser(user *ChatUser) {
	log.Println("In AddUser")
	server.addUser <- user
}

func (server *ChatServer) RemoveUser(user *ChatUser) {
	log.Println("Removing user")
	server.removeUser <- user
}

func (server *ChatServer) ProcessNewIncomingMessage(message *ChatMessage) {
	// log.Println("In ProcessNewIncomingMessage ",message)
	server.newIncomingMessage <- message
}

func (server *ChatServer) Done() {
	server.doneCh <- true
}

func (server *ChatServer) sendPastMessages(user *ChatUser) {
	for _, msg := range server.Messages {
		//  log.Println("In sendPastMessages writing ",msg)
		user.Write(msg)
	}
}

func (server *ChatServer) Err(err error) {
	server.errorChannel <- err
}

func (server *ChatServer) sendAll(msg *ChatMessage) {
	log.Println("In Sending to all Connected users")
	for _, user := range server.connectedUsers {
		user.Write(msg)
	}
}

func chatRoute() {
	server := NewChatServer()
	go server.Listen()

	http.HandleFunc("/chat", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "resources/views/webim/chat.html")
	})
}

func (server *ChatServer) Listen() {
	log.Println("Server Listening .....")

	http.HandleFunc("/chat/ws", server.handleChat)
	http.HandleFunc("/chat/getAllMessages", server.handleGetAllMessages)

	for {
		select {
		// Adding a new user
		case user := <-server.addUser:
			log.Println("Added a new User")
			server.connectedUsers[user.id] = user
			log.Println("Now ", len(server.connectedUsers), " users are connected to chat room")
			server.sendPastMessages(user)

		case user := <-server.removeUser:
			log.Println("Removing user from chat room")
			delete(server.connectedUsers, user.id)

		case msg := <-server.newIncomingMessage:
			server.Messages = append(server.Messages, msg)
			server.sendAll(msg)
		case err := <-server.errorChannel:
			log.Println("Error : ", err)
		case <-server.doneCh:
			return
		}
	}

}

func (server *ChatServer) handleChat(responseWriter http.ResponseWriter, request *http.Request) {
	log.Println("HAndling chat request ")
	var messageObject ChatMessage
	conn, _ := upgrader.Upgrade(responseWriter, request, nil)
	//msgType, msg, err := conn.ReadMessage()
	err := conn.ReadJSON(&messageObject)
	log.Println("MEssage retireved when add user recieved", &messageObject)

	if err != nil {
		log.Println("Error while reading JSON from websocket ", err.Error())
	}
	user := NewUser(conn, server)

	log.Println("going to add user", user)
	server.AddUser(user)

	log.Println("user added successfully")
	server.ProcessNewIncomingMessage(&messageObject)
	user.Listen()

}

func (server *ChatServer) handleGetAllMessages(responseWriter http.ResponseWriter, request *http.Request) {

	if err := json.NewEncoder(responseWriter).Encode(server); err != nil {
		log.Println(err)
	}
}
