package controllers

import (
	"encoding/json"
	"fmt"
	"goweb/app/webim/models"
	"goweb/pkg/common"
	"log"
	"net/http"
	"strconv"
)

type LongPollingController struct {
	baseController
}

func (*LongPollingController) Join(w http.ResponseWriter, r *http.Request) {
	uname := r.URL.Query().Get("uname")
	if len(uname) == 0 {
		http.Redirect(w, r, "/webim", http.StatusFound)
	}

	// Join chat room.
	Join(uname, nil)

	data := make(map[string]interface{})
	data["IsLongPolling"] = true
	data["UserName"] = uname
	common.GenerateHTML(w, data, "resources/views/webim/", "layout", "longpolling")
}

func (*LongPollingController) Post(w http.ResponseWriter, r *http.Request) {
	uname := r.PostFormValue("uname")
	content := r.PostFormValue("content")
	if len(uname) == 0 || len(content) == 0 {
		// http.Redirect(w, r, "/webim", http.StatusFound)
		return
	}
	log.Println("sdsd: ", uname, content)

	publish <- newEvent(models.EVENT_MESSAGE, uname, content)
}

func (*LongPollingController) Fetch(w http.ResponseWriter, r *http.Request) {
	// lastReceivedStr := r.PostFormValue("lastReceived")
	lastReceivedStr := r.URL.Query().Get("lastReceived")
	lastReceivedInt, err := strconv.Atoi(lastReceivedStr)
	if err != nil {
		return
	}

	data := make(map[string]interface{})
	events := models.GetEvents(int(lastReceivedInt))
	if len(events) > 0 {
		data["json"] = events

		jsonStr, err := json.Marshal(data)
		if err != nil {
			log.Fatal("序列化json错误")
		}
		w.Header().Add("Access-Control-Allow-Origin", "*")
		fmt.Fprintf(w, "%s", string(jsonStr))

		return
	}

	// Wait for new message(s).
	ch := make(chan bool)
	waitingList.PushBack(ch)
	<-ch

	data["json"] = models.GetEvents(int(lastReceivedInt))

	jsonStr, err := json.Marshal(data)
	if err != nil {
		log.Fatal("序列化json错误")
	}
	w.Header().Add("Access-Control-Allow-Origin", "*")
	fmt.Fprintf(w, "%s", string(jsonStr))
}
