package controllers

import (
	"encoding/json"
	"goweb/app/webim/models"
	"goweb/pkg/common"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

type WebSocketController struct {
	baseController
}

func (*WebSocketController) Get(w http.ResponseWriter, r *http.Request) {
	uname := r.URL.Query().Get("uname")
	if len(uname) == 0 {
		http.Redirect(w, r, "/webim", http.StatusFound)
	}

	data := make(map[string]interface{})
	data["IsWebSocket"] = true
	data["UserName"] = uname
	common.GenerateHTML(w, data, "resources/views/webim/", "layout", "websocket")
}

func (*WebSocketController) Join(w http.ResponseWriter, r *http.Request) {
	uname := r.URL.Query().Get("uname")
	if len(uname) == 0 {
		http.Redirect(w, r, "/webim", http.StatusFound)
	}

	// Upgrade from http request to WebSocket.
	ws, err := websocket.Upgrade(w, r, nil, 1024, 1024)
	if _, ok := err.(websocket.HandshakeError); ok {
		http.Error(w, "Not a websocket handshake", 400)
		return
	} else if err != nil {
		common.Warning("Cannot setup WebSocket connection:", err)
		return
	}
	log.Println("aa ", uname)
	// Join chat room.
	Join(uname, ws)
	defer Leave(uname)

	// Message receive loop.
	for {
		_, p, err := ws.ReadMessage()
		if err != nil {
			return
		}
		publish <- newEvent(models.EVENT_MESSAGE, uname, string(p))
	}
}

// broadcastWebSocket broadcasts messages to WebSocket users.
func broadcastWebSocket(event models.Event) {
	data, err := json.Marshal(event)
	if err != nil {
		common.Warning("Fail to marshal event:", err)
		return
	}

	for sub := subscribers.Front(); sub != nil; sub = sub.Next() {
		// Immediately send event to WebSocket users.
		ws := sub.Value.(Subscriber).Conn
		if ws != nil {
			if ws.WriteMessage(websocket.TextMessage, data) != nil {
				// User disconnected.
				unsubscribe <- sub.Value.(Subscriber).Name
			}
		}
	}
}
