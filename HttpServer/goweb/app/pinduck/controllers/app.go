package controllers

import (
	"goweb/pkg/common"
	"net/http"
)

type baseController struct{}

type AppController struct {
	baseController
}

func RegisterRouter() {
	a := new(AppController)
	http.HandleFunc("/pinduck", a.Home)

}

func (a *AppController) Home(w http.ResponseWriter, r *http.Request) {
	common.GenerateHTML(w, nil, "resources/views/pinduck/", "layout", "home")
}
