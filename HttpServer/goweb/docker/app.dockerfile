# 打包依赖阶段使用golang作为基础镜像
FROM golang:1.15-alpine as builder

# 启用go module
ENV GO111MODULE=on \
  GOPROXY=https://goproxy.cn,direct

WORKDIR /app

ADD . .

RUN go mod download

# CGO_ENABLED禁用cgo 然后指定OS等，并go build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" -o chitchat

# 由于我不止依赖二进制文件，还依赖views文件夹下的html文件还有assets文件夹下的一些静态文件
# 所以我将这些文件放到了publish文件夹
RUN mkdir publish && cp chitchat publish && cp .env publish && \
  cp -r templates publish && cp -r public publish

# 运行阶段指定scratch作为基础镜像
FROM alpine

WORKDIR /app

# 将上一个阶段publish文件夹下的所有文件复制进来 
COPY --from=builder /app/publish .
# COPY --from=builder /app .

# 为了防止代码中请求https链接报错，我们需要将证书纳入到scratch中
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/cert

# 指定运行时环境变量
# ENV GIN_MODE=release \
  # PORT=80

EXPOSE 9091

ENTRYPOINT ["./chitchat"]