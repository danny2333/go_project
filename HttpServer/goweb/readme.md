docker build -t chitchat .
docker run -p 8082:9091 chitchat
docker run -p 8082:9091 chitchat -config=config.json

docker build -t chitchat -f Dockerfile.production .

docker 国内 pull 镜像太慢，可以用下面到命令来加速：

docker pull daocloud.io/daocloud/yourImageName:latest

启动容器，第一个 chitchat 是容器名字，第二个 chitchat 是镜像名称

-v 映射配置文件 本地路径：容器路径

docker run --name chitchat -p 8000:9091 -v ./config.json:/opt/app/config.json -d chitchat-server

指定文件
docker compose -f docker-compose.dev.yml build

docker compose -f docker-compose.dev.yml up

docker-compose ps

- 重点 docker compose 里面 mysql host go 里面填写

```yml
version: '3'

# docker-compose.dev.yml
services:
  web:
    build:
      context: .
      dockerfile: ./docker/app.dockerfile
    ports:
      - 8080:8080
    # volumes:
    # - .:/app
    container_name: golang-docker-web

# persisted storage
volumes:
  golang-docker-data:
```

```sh
# This scripts to stop the running server stop_run.sh
d=`ps -ef | grep GoWebPractic-linux | grep -v "grep" | awk '{print $2}'`
if [ -n "$d" ]
  then
    kill -9 $d
    echo "server stop"
  else
    echo "server not started"
fi
```

```go
func CreateMovieEndPoint(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var movie Movie
	if err := json.NewDecoder(r.Body).Decode(&movie); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	movie.ID = bson.NewObjectId()
	if err := dao.Insert(movie); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusCreated, movie)
}
```

### test

- goreddit
- go-forum
- WechatMsg
