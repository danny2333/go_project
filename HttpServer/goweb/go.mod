module goweb

go 1.18

require (
	github.com/coocood/freecache v1.2.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.5.0
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
)

require github.com/cespare/xxhash/v2 v2.1.2 // indirect
