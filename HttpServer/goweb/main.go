package main

import (
	"fmt"
	_ "goweb/app"
	"goweb/app/common/middlewares"
	"goweb/config"
	"goweb/pkg/dbconn"
	"goweb/routes"
	"net/http"
	"time"
)

func main() {
	// 加载 json 配置
	config.LoadConfig("./config/config.json")

	//数据库连接池初始化
	db := dbconn.GetInstance().InitPool()
	if db == nil {
		fmt.Println("init database pool failure...")
	}

	defer db.Close()

	// http中间件
	server := &http.Server{
		Addr: fmt.Sprintf("%s:%s", config.Config.Address, config.Config.Port),
		Handler: &middlewares.TimeoutMiddleWare{
			Next: &middlewares.LogMiddleWare{
				Next: &middlewares.CrossMiddleWare{
					Next: &middlewares.RemoveTrailingSlashMiddleWare{},
				},
			},
		},
		WriteTimeout: time.Duration(5) * time.Second,
		ReadTimeout:  time.Duration(10) * time.Second,
	}

	// 注册路由
	routes.RegisterWebRoutes()

	/*
		显示启动信息 终端文字带颜色
		https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
		\x1b[(文字装饰);(颜色代码):
		1、文字装饰
			0		1		4		3
			正常	加粗	下划线	背景
		2、颜色代码
			基本8色		基本高对比色	xterm 的 256 色
			30 ~ 37		90 ~ 97			0 ~ 256
	*/
	fmt.Printf("\x1b[1;32m* The Web Server was running on:\x1b[0m\t"+
		"http://%s:%s\n", "localhost", config.Config.Port)

	if err := server.ListenAndServe(); err != nil {
		fmt.Println(err)
	}
}
