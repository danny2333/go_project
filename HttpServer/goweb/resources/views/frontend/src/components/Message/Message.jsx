import React, { Component } from 'react'
import './Message.scss'

class Message extends Component {
  // 构造方法
  // 1 constructor必须用super()初始化this, 可以绑定事件到this

  // 2 如果你在constructor中要使用this.props, 就必须给super加参数, super(props)；

  // 3 无论有没有constructor, render中都可以使用this.props, 默认自带

  // 4 如果组件没有声明constructor, react会默认添加一个空的constructor

  // 5 ES6采用的是先创建父类的实例this（故要先调用 super( )方法），完后再用子类的构造函数修改this
  constructor(props) {
    super(props)
    let temp = JSON.parse(this.props.message)
    this.state = {
      message: temp
    }
  }
  render() {
    return <div className="Message">{this.state.message.body}</div>
  }
}

export default Message
