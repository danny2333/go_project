'use strict'

var currentLoc = document.location.href

function ajaxpost(url, data, func, args) {
  var r = new XMLHttpRequest()
  r.open('POST', url, true)
  r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
  r.onreadystatechange = function () {
    if (r.readyState != 4 || r.status < 200 || r.status > 400) {
      return
    }
    func = func || ''
    args = args || ''
    if (func.length > 0) {
      window[func](r.responseText, args)
    }
  }
  r.send(data)
}

function ajaxget(url, func, args) {
  var r = new XMLHttpRequest()
  r.open('GET', url, true)

  r.onreadystatechange = function () {
    if (r.readyState != 4 || r.status < 200 || r.status > 400) {
      return
    }
    func = func || ''
    args = args || ''
    if (func.length > 0) {
      window[func](r.responseText, args)
    }
  }

  r.send()
}

function ajaxgetns(url) {
  var r = new XMLHttpRequest()
  r.open('GET', url, false)
  r.send(null)
  return r.responseText
}

function isguest() {
  if (typeof guest !== 'undefined') {
    document.location.href =
      '/account/login/?back=' + encodeURIComponent(document.location.href)
    return false
  }
  return true
}

function comment(elems) {
  isguest()

  ajaxpost(
    '/comment/add/',
    'cbody=' +
      elems.cbody.value +
      '&pin=' +
      elems.pin.value +
      '&csrf=' +
      csrf +
      '&send=1',
    'comadd',
    ''
  )
}

function comadd(res) {
  var resarr = JSON.parse(res)

  if (resarr['result'] == '0') {
    document.getElementById('cform').getElementsByTagName('span')[0].innerHTML =
      resarr['content']
  } else {
    document.getElementById('cform').outerHTML = resarr['content']
  }
}

function like(pin, type) {
  isguest()

  ajaxpost(
    '/rate/like/',
    'pin=' + pin + '&type=' + type + '&csrf=' + csrf,
    'likechange',
    pin
  )
}

function likechange(like, pin) {
  var lc = document.getElementById('dislike-' + pin)
  if (1 == parseInt(like)) {
    lc = document.getElementById('like-' + pin)
  }

  lc.innerHTML = Math.abs(like) + parseInt(lc.innerHTML)
}

function follow(bname) {
  isguest()

  ajaxpost(
    '/rate/follow/',
    'bname=' + bname + '&csrf=' + csrf,
    'folchange',
    bname
  )
}

function folchange(cnt, bname) {
  var bc = document.getElementById('follow-' + bname)

  bc.innerHTML = parseInt(cnt) + parseInt(bc.innerHTML)
}

function repin(elems) {
  isguest()

  ajaxpost(
    '/rate/repin/',
    'bname=' +
      elems.bname.value +
      '&pin=' +
      elems.pin.value +
      '&csrf=' +
      csrf +
      '&send=1',
    'pinchange',
    elems.bname.value
  )
}

function pinchange(res, bname) {
  var pc = document.getElementById('repin')
  var json = JSON.parse(res)

  pc.innerHTML = json['html']

  /*
	if(res == '1')
	{
		pc.innerHTML = 'Repinned to <a href="/board/'+bname+'/">'+bname+'</a>';
	}
	else
	{
		pc.innerHTML = 'Repin exists in <a href="/board/'+bname+'/">'+bname+'</a>';
	}
	*/
}

function openhere(url) {
  window.history.pushState('page', 'Title', url)
  ajaxget(url + '?xhr=1', 'showhere', '')
}

function showhere(content) {
  var above = document.createElement('div')
  above.setAttribute('id', 'overpage')
  above.setAttribute('class', 'overpage')

  if (content.indexOf('s.imgur.com') != -1) {
    var script = document.createElement('script')
    script.setAttribute('src', '//s.imgur.com/min/embed.js')
    document.body.appendChild(script)
  }

  above.innerHTML =
    '<div class="overcontent"><div class="closebar" onclick="closehere();">close[x]</div>' +
    content +
    '</div><div class="closepage" onclick="closehere();"></div>'
  document.body.appendChild(above)
  document.body.classList.toggle('noscroll')
}

function closehere() {
  var beyond = document.getElementById('overpage')
  beyond.outerHTML = ''
  beyond.remove
  document.body.classList.toggle('noscroll')
  window.history.pushState('page', 'Title', currentLoc)
}

function load(url) {
  currentLoc = url
  window.history.pushState('page', 'Title', url)
  ajaxget(url + '&xhr=1', 'loadpage', '')
}

function loadpage(str) {
  var jsonarr = JSON.parse(str)
  document.getElementById('thumbnails').innerHTML += jsonarr['output']

  msnry.reloadItems()
  msnry.layout()

  document.getElementById('load').innerHTML = jsonarr['load']
  document.getElementById('nav').innerHTML = jsonarr['nav']
}

function resizer(elid, elact) {
  var resw = 640,
    resh = 480
  var elem = document.getElementById(elid)
  var natw = elem.naturalWidth
  var nath = elem.naturalHeight

  if (document.getElementById('overpage') !== null) {
    resw = document.getElementById('overpage').firstChild.clientWidth
  } else {
    resw = document.body.clientWidth /*window.innerWidth;*/
  }
  resh = (resw * nath) / natw

  if (resw > natw) {
    resw = natw
    resh = nath
  }

  elem.width = resw
  elem.height = resh
}
