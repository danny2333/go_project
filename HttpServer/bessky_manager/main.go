package main

import (
	"fmt"
	"os"
)

// 菜单系统（将菜单系统封装到函数中）
func showMenu() {
	fmt.Println("欢迎光临学生管理系统")
	fmt.Println(`
		1.查看所有学生
		2.新增学生
		3.删除学生
		4.退出
	`)
}

// 造一个存储学生数据的类型
type student struct {
	id   int64
	name string
}

// newStudent是一个student类型的构造函数
func newStudent(id int64, name string) *student {
	return &student{
		id:   id,
		name: name,
	}
}

// 造一个学生管理者(将学生数据存储到该类型中)
type sutdentManager struct {
	store_student map[int64]*student
}

func (s sutdentManager) showAllStudent() {
	// 显示所有学生信息(从学生管理者：sutdentManager存储的变量store_student获取到学生数据数据)
	for _, stu := range s.store_student {
		fmt.Printf("学号：%d 姓名：%s\n", stu.id, stu.name)
	}
}

func (s sutdentManager) addStudent() {
	// 添加学生
	// 1.请输入学生的id和姓名
	var (
		id   int64
		name string
	)
	fmt.Print("请输入学生id：")
	fmt.Scanln(&id)

	fmt.Print("请输入学生姓名：")
	fmt.Scanln(&name)

	// 2.判断学生是否存在
	if _, ok := s.store_student[id]; !ok {
		newStu := newStudent(id, name)
		s.store_student[id] = newStu
		fmt.Printf("学生信息 ID：%v、姓名：%v 添加成功\n", id, name)
	} else {
		fmt.Printf("学生信息 ID：%v、姓名：%v 已存在，请勿重复添加\n", id, name)
	}
	// 3.将学生信息添加到信息存储系统（store_student）
}

func (s sutdentManager) deleteStudent() {
	// 删除学生
	// 1.判断学生是否存在
	var id int64
	fmt.Print("请输入学生id：")
	fmt.Scanln(&id)

	// 2.根据学生id删除对应的学生信息
	if _, ok := s.store_student[id]; !ok {
		// 学生id不存在
		fmt.Printf("学生信息 ID：%v 不存在，删除失败\n", id)
	} else {
		// 学生id存在，删除成功
		delete(s.store_student, id)
		fmt.Printf("学生信息 ID：%v 存在，删除成功\n", id)
	}
}

func main() {
	// 初始化学生数据存储变量(开辟内存空间)
	stuManger := sutdentManager{
		store_student: make(map[int64]*student, 200),
	}
	for {
		// 1. 打印选项菜单

		showMenu()

		// 2.等待用户输入选项
		var choice int
		fmt.Print("选项：")
		fmt.Scanln(&choice)
		fmt.Printf("你选择了：%d选项\n", choice)
		// 3.执行对应的方法
		switch choice {
		case 1:
			stuManger.showAllStudent()
		case 2:
			stuManger.addStudent()
		case 3:
			stuManger.deleteStudent()
		case 4:
			os.Exit(1)
		default:
			fmt.Println("输入的选项不存在")
		}
	}
}
