package schema

import (
	"sync"
)

// WorkerHandle 工作处理接口
type WorkerHandle func() Worker

type FuncHandle func()

// Option 抓取选项
type Option struct {
	Mux    *sync.RWMutex `label:"读写锁"`
	Action map[string]FuncHandle
}

// Worker 内容抓取工作者
type Worker interface {
	Options() *Option
	Intro(category string) string
	Do(action string) (map[string]FuncHandle, error)
}
