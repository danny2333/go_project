package schema

import (
	"fmt"
	"os"
)

// Ge 内容抓取器
type Ge struct {
	Started   int64                   `label:"程序启动时间"`
	Workers   map[string]Worker       `label:"数据抓取器实例"`
	Providers map[string]WorkerHandle `label:"数据服务提供者"`
}

// Help 显示帮助信息
func (w *Ge) Help() {
	for k, wh := range w.Providers {
		var wh = wh()
		fmt.Fprintln(os.Stderr, "        ", k, " ", wh.Intro("label"))
	}
}

// Cli 启动内容抓取工作
func (w *Ge) Cli(provider, action string) {
	if wh, ok := w.Providers[provider]; ok {
		var worker = wh()
		w.Run(worker, provider, action)
	} else {
		fmt.Fprintln(os.Stderr, "未知的模块 [", provider, "]")
		w.Help()
	}
}

// Run 执行指定模块
func (w *Ge) Run(worker Worker, provider, action string) {
	funcMap, err := worker.Do(action)
	if nil != err {
		fmt.Println(err)
		for k := range funcMap {
			fmt.Fprintln(os.Stderr, "        ", k)
		}
	}
}
