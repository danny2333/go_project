package modules

import (
	"errors"
	"fmt"
	"goexample/schema"
	"os"
	"reflect"
	"regexp"
	"strings"
	"time"
)

func NewStructs() schema.Worker {
	return &Structs{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"struct1": struct1,
		}},
	}
}

type Structs struct {
	option *schema.Option
}

func (c *Structs) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Structs 样例"
	}

	return tip
}

func (c *Structs) Options() *schema.Option {
	return c.option
}

func (c *Structs) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]
	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

const tagName = "validate"

var mailRe = regexp.MustCompile(`\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z`)

//Generic data validator
type Validator interface {
	//Validate method performs validation and returns results and optional error.
	Validate(interface{}) (bool, error)
}

//DefaultValidator does not perform any validations
type DefaultValidator struct {
}

func (v DefaultValidator) Validate(val interface{}) (bool, error) {
	return true, nil
}

type NumberValidator struct {
	Min int
	Max int
}

func (v NumberValidator) Validate(val interface{}) (bool, error) {
	num := val.(int)

	if num < v.Min {
		return false, fmt.Errorf("should be greater than %v", v.Min)
	}

	if v.Max >= v.Min && num > v.Max {
		return false, fmt.Errorf("should be less than %v", v.Max)
	}

	return true, nil
}

//StringValidator validates string presence and/or its length
type StringValidator struct {
	Min int
	Max int
}

func (v StringValidator) Validate(val interface{}) (bool, error) {
	l := len(val.(string))

	if l == 0 {
		return false, fmt.Errorf("cannot be blank")
	}

	if l < v.Min {
		return false, fmt.Errorf("should be at least %v chars long", v.Min)
	}

	if v.Max >= v.Min && l > v.Max {
		return false, fmt.Errorf("should be less than %v chars long", v.Max)
	}

	return true, nil
}

type EmailValidator struct {
}

func (v EmailValidator) Validate(val interface{}) (bool, error) {
	if !mailRe.MatchString(val.(string)) {
		return false, fmt.Errorf("is not a valid email address")
	}

	return true, nil
}

//Returns validator struct corresponding to validation type
func getValidatorFromTag(tag string) Validator {
	args := strings.Split(tag, ",")

	switch args[0] {
	case "number":
		validator := NumberValidator{}
		fmt.Sscanf(strings.Join(args[1:], ","), "min=%d,max=%d", &validator.Min, &validator.Max)
		return validator
	case "string":
		validator := StringValidator{}
		fmt.Sscanf(strings.Join(args[1:], ","), "min=%d,max=%d", &validator.Min, &validator.Max)
		return validator
	case "email":
		return EmailValidator{}
	}

	return DefaultValidator{}
}

//Performs actual data validation using validator definitions on the struct
func validateStruct(s interface{}) []error {
	errs := []error{}

	//ValueOf returns a Value representing the run-time data
	v := reflect.ValueOf(s)

	for i := 0; i < v.NumField(); i++ {
		//Get the field tag value
		tag := v.Type().Field(i).Tag.Get(tagName)

		//Skip if tag is not defined or ignored
		if tag == "" || tag == "-" {
			continue
		}

		//Get a validator that corresponds to a tag
		validator := getValidatorFromTag(tag)

		//Perform validation
		valid, err := validator.Validate(v.Field(i).Interface())

		//Append error to results
		if !valid && err != nil {
			errs = append(errs, fmt.Errorf("%s %s", v.Type().Field(i).Name, err.Error()))
		}
	}

	return errs
}

// User 首字母小写是非导出的，外部的包无法引入
type User struct {
	ID       int    `validate:"-"`
	Username string `validate:"string,min=2,max=10" json:"username" db:"user_name"`
	Age      int    `validate:"number,min=1,max=1000"`
	Email    string `validate:"email,required"`
	address  Address
}

type Address struct {
	Province string `validate:"string"`
	City     string `validate:"string"`
}

func (u *User) SetUserName(name string) {
	u.Username = name
}

func valCheck(a interface{}) {
	v := reflect.ValueOf(a)
	switch v.Kind() {
	case reflect.Int64:
		fmt.Printf("a is int64, store value is:%d\n", v.Int())
	case reflect.Float64:
		fmt.Printf("a is float64, store value is:%f\n", v.Float())
	default:
		fmt.Println("valCheck: default")
	}

}

func struct1() {
	var user User
	user.Username = "user1"
	user.Age = 18
	user.Email = "user1example.com"
	user.address = Address{
		Province: "guangdong",
		City:     "shenzhen",
	}

	fmt.Printf("%+v\n\n", user)

	v := reflect.ValueOf(&user)
	v.Elem().FieldByName("Username").SetString("王尼玛")
	v.Elem().Field(2).SetInt(20000)

	fmt.Printf("%#v\n\n", user)

	user.SetUserName("托马斯")
	fmt.Printf("%#v\n\n", user)

	t := reflect.TypeOf(user)
	fmt.Println("Name: ", t.Name())
	fmt.Println("Type: ", t.Kind())

	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		tag := field.Tag.Get(tagName)
		fmt.Printf("%d. %v(%v), tag:'%v'\n", i+1, field.Name, field.Type.Name(), tag)
	}

	var x string = "aa"
	valCheck(x)

	fmt.Println("Errors: ")
	for i, err := range validateStruct(user) {
		fmt.Printf("\t%d. %s\n", i+1, err.Error())
	}

	// 在同一个 `case` 语句中，你可以使用逗号来分隔多个表达式。
	// 在这个例子中，我们还使用了可选的 `default` 分支。
	switch time.Now().Weekday() {
	case time.Saturday, time.Sunday:
		fmt.Println("It's the weekend")
	default:
		fmt.Println("It's a weekday")
	}

	// 不带表达式的 `switch` 是实现 if/else 逻辑的另一种方式。
	// 这里还展示了 `case` 表达式也可以不使用常量。
	tt := time.Now()
	switch {
	case tt.Hour() < 12:
		fmt.Println("It's before noon")
	default:
		fmt.Println("It's after noon")
	}

	// 类型开关 (`type switch`) 比较类型而非值。可以用来发现一个接口值的类型。
	// 在这个例子中，变量 `t` 在每个分支中会有相应的类型。
	whatAmI := func(i interface{}) {
		switch t := i.(type) {
		case bool:
			fmt.Println("I'm a bool")
		case int:
			fmt.Println("I'm an int")
		default:
			fmt.Printf("Don't know type %T\n", t)
		}
	}
	whatAmI(true)
	whatAmI(1)
	whatAmI("hey")

	fmt.Println("-------------可变参数---------------------")
	sum := func(nums ...int) {
		fmt.Print(nums, " ")
		total := 0
		for _, num := range nums {
			total += num
		}
		fmt.Println("总和：", total)
	}

	nums := []int{1, 2, 3, 4}
	// sum(1, 2, 3)
	sum(nums...)
	os.Exit(0)
}
