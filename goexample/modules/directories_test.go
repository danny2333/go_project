package modules

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func TestDemo1(t *testing.T) {
	// 想要把打印信息展示 加 -v
	fmt.Println("创建目录")
	// 在当前工作目录下，创建一个子目录。
	err := os.Mkdir("subdir", 0755)
	if err != nil {
		t.Log("创建目录失败：", err.Error())
	}

	t.Log("创建目录成功")
	// t.Error("创建目录成功")

	// 创建空文件
	d := []byte("")
	err = ioutil.WriteFile("subdir/file1.txt", d, 0644)
	if err != nil {
		t.Log(err)
	}

	// 创建有层级的目录
	err = os.MkdirAll("subdir/parent/file2", 0755)
	if err != nil {
		t.Log("创建层级目录失败：", err.Error())
	}

	// 创建空文件
	d2 := []byte("")
	err = ioutil.WriteFile("subdir/parent/file2.txt", d2, 0644)
	if err != nil {
		t.Log(err)
	}

	// 列出，目录数据 只能一级
	// c, err := ioutil.ReadDir("subdir")
	// if err != nil {
	// 	t.Log(err)
	// }
	// for _, entry := range c {
	// 	// Name=名称 IsDir=是否目录
	// 	fmt.Println(" ", entry.Name(), entry.IsDir())
	// }

	// `filepath.Walk` 遍历访问到每一个目录和文件后，都会调用 `visit`。
	volit := func(p string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		fmt.Println(" ", p, info.IsDir())
		return nil
	}

	// 遍历目录下所有层级
	err = filepath.Walk("subdir", volit)
	if err != nil {
		t.Log(err)
	}

	// 休眠
	time.Sleep(time.Minute)

	// 创建这个临时目录后，一个好习惯是：使用 `defer` 删除这个目录。
	// `os.RemoveAll` 会删除整个目录（类似于 `rm -rf`）。
	// defer os.RemoveAll("subdir")
}
