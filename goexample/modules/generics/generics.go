package generics

import (
	"errors"
	"fmt"
	"goexample/schema"
)

func NewGenerics() schema.Worker {
	return &Generics{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"generics": generics,
		}},
	}
}

type Generics struct {
	option *schema.Option
}

func (j *Generics) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Generics 样例"
	}

	return tip
}

func (j *Generics) Options() *schema.Option {
	return j.option
}

func (j *Generics) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := j.option.Action[action]
	if !ok {
		return j.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

func generics() {
	var m = map[int]string{1: "2", 2: "4", 4: "8"}
	var m1 = map[string]string{"1": "2", "2": "4", "4": "8"}
	fmt.Println("keys m:", MapKeys(m))
	fmt.Println("keys m1:", MapKeys(m1))

	// _ = MapKeys[int, string](m)
	_ = MapKeys(m)
	lst := List[int]{}
	lst.Push(10)
	lst.Push(13)
	lst.Push(23)
	lst.Push(10)
	fmt.Println("list:", lst.GetAll())
}
