package modules

import (
	"errors"
	"fmt"
	"goexample/schema"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

func NewGoroutine() schema.Worker {
	return &Goroutine{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"goroutine": goroutine,
		}},
	}
}

type Goroutine struct {
	option *schema.Option
}

func (c *Goroutine) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Goroutine 样例"
	}

	return tip
}

func (c *Goroutine) Options() *schema.Option {
	return c.option
}

func (c *Goroutine) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]
	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

func fetch(url string, wg *sync.WaitGroup) (int, error) {
	defer wg.Done()
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
		return 0, err
	}

	fmt.Println(url, " ", resp.StatusCode)
	return resp.StatusCode, nil
}

func pageSize(name, url string, output chan string) {
	resp, _ := http.Get(url)
	body, _ := ioutil.ReadAll(resp.Body)

	output <- fmt.Sprintf("%v: %v bytes", name, len(body))
}

func goroutine() {
	var urls = []string{
		"https://www.baidu.com/",
		"https://www.cnblogs.com/",
		"https://www.google.cn/",
		"http://www.github.com/",
		"http://httpbin.org/status/404",
	}

	var wg sync.WaitGroup
	for _, url := range urls {
		wg.Add(1)
		go fetch(url, &wg)
	}

	wg.Wait()

	fmt.Println("-----------------------------")
	start := time.Now()
	output := make(chan string)

	go pageSize("bilibili", "https://www.bilibili.com/", output)
	go pageSize("douyu", "https://www.douyu.com/", output)
	go pageSize("huya", "https://www.huya.com/", output)
	go pageSize("egame", "https://egame.qq.com/", output)

	fmt.Println(<-output)
	fmt.Println(<-output)
	fmt.Println(<-output)
	fmt.Println(<-output)

	fmt.Println("\nThis took us:", time.Since(start))
}
