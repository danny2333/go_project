package logger

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

type LOG struct {
	dir     string
	logFile string
	f       *os.File
}

var (
	MyLOG   LOG
	TimeNow = time.Now().Format("15:04:05")
)

func (this *LOG) Init(dir string) {
	// timeStr := time.Now().Format("2006-01-02 15:04:05")
	// fileTimeStr := strings.Replace(strings.Replace(timeStr, ":", "-", -1), " ", "-", -1)
	fileTimeStr := time.Now().Format("20060102")
	// this.logFile = fmt.Sprintf("./runtime/logs/%s/%s.log", dir, fileTimeStr)
	this.logFile = fmt.Sprintf("%s/%s.log", dir, fileTimeStr)

	if checkFileIsExist(this.logFile) {
		this.f, _ = os.OpenFile(this.logFile, os.O_APPEND, 0666)
		fmt.Printf("日志文件存在 %s\n", this.logFile)
	} else {
		this.f, _ = os.Create(this.logFile)
		this.f, _ = os.OpenFile(this.logFile, os.O_APPEND, 0666)
		fmt.Printf("打开日志文件 %s\n", this.logFile)
	}
}

func (this *LOG) End() {
	this.f.Close()
}

func (this *LOG) writeFile(writeString string) {
	io.WriteString(this.f, writeString)
}

func (this *LOG) Infos(formating string, args ...interface{}) {
	srcfilename, line, funcname := "???", 0, "???"
	pc, srcfilename, line, ok := runtime.Caller(1)
	if ok {
		funcname = runtime.FuncForPC(pc).Name()      // main.(*MyStruct).foo
		funcname = filepath.Ext(funcname)            // .foo
		funcname = strings.TrimPrefix(funcname, ".") // foo
		srcfilename = filepath.Base(srcfilename)     // /full/path/basename.go => basename.go
	}

	writeString := fmt.Sprintf("[INFOS] %s %s:%d(func %s): %s", TimeNow, srcfilename, line, funcname, fmt.Sprintf(formating, args...))
	fmt.Println(writeString)
	this.writeFile(writeString)
}

func (this *LOG) Error(formating string, args ...interface{}) {
	srcfilename, line, funcname := "???", 0, "???"
	pc, srcfilename, line, ok := runtime.Caller(1)
	if ok {
		funcname = runtime.FuncForPC(pc).Name()      // main.(*MyStruct).foo
		funcname = filepath.Ext(funcname)            // .foo
		funcname = strings.TrimPrefix(funcname, ".") // foo
		srcfilename = filepath.Base(srcfilename)     // /full/path/basename.go => basename.go
	}

	writeString := fmt.Sprintf("[ERROR] %s %s:%d(func %s): %s", TimeNow, srcfilename, line, funcname, fmt.Sprintf(formating, args...))
	fmt.Println(writeString)
	this.writeFile(writeString)
}

func (this *LOG) Panic(err error) {
	if err == nil {
		return
	}

	srcfilename, line, funcname := "???", 0, "???"
	pc, srcfilename, line, ok := runtime.Caller(2)
	if ok {
		funcname = runtime.FuncForPC(pc).Name()      // main.(*MyStruct).foo
		funcname = filepath.Ext(funcname)            // .foo
		funcname = strings.TrimPrefix(funcname, ".") // foo
		srcfilename = filepath.Base(srcfilename)     // /full/path/basename.go => basename.go
	}

	writeString := fmt.Sprintf("[PANIC] %s %s:%d(func %s): %s", TimeNow, srcfilename, line, funcname, fmt.Sprintf("%v", err))
	fmt.Println(writeString)
	this.writeFile(writeString)
	panic(err)
}

func checkFileIsExist(filename string) bool {
	var exist = true
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		exist = false
	}
	return exist
}
