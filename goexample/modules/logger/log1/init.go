package log1

var applog = new(FishLogger)

// 20201011: 使用Logs
func InitLogs(logpath string, amaxSize int64, amaxAge, alogCount int) {
	// [log]
	// # 日志文件的路径
	// LogPath = logs/app.log
	// # 日志文件的个数
	// LogCount = 6
	// # 日志文件的最大大小
	// #MaxSize = 1024*1024*256
	// # 默认256M
	// MaxSize = 268435456
	// # 日志文件的保存时间（天）
	// MaxAge = 3
	// log.InitLogs("logs/app.log", 268435456, 3, 6)

	maxSize = amaxSize // 单个文件最大大小
	maxAge = amaxAge   // 单个文件保存2天
	logCount = alogCount
	applog = NewLogger(logpath)
	defer applog.Flush()
	applog.SetLevel(DEBUG)
	// applog.SetLevel(FATAL)
	applog.SetCallInfo(true)
	applog.SetConsole(true)
	// applog.Info("test")
}
func Println(args ...interface{}) {
	// applog.Info(args)
	applog.println(INFO, args...)
}

func Printf(format string, args ...interface{}) {
	// applog.Infof(format, args...)
	applog.printf(INFO, format, args...)
}
