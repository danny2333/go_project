package modules

import (
	"errors"
	"fmt"
	"goexample/schema"
)

/*
[]T 是一个元素类型为 T 的 slice
[n]T 是一个有 n 个类型为 T 的值的数组
数组必须长度，长度是它的类型的一部分，因此数组不能改变长度
slice的长度是可以改变的，可以认为是动态数组
双引号用来创建可解析的字符串字面量(支持转义，但不能用来引用多行)

反引号用来创建原生的字符串字面量，
这些字符串可能由多行组成(不支持任何转义序列)，
原生的字符串字面量多用于书写多行消息、HTML以及正则表达式

而单引号则用于表示Golang的一个特殊类型：rune，类似其他语言的byte但又不完全一样，
是指：码点字面量（Unicode code point），不做任何转义的原始内容
*/

func NewArrays() schema.Worker {
	return &Arrays{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"arr1":   arr1,
			"arr2":   arr2,
			"slice1": slice1,
			"map1":   map1,
			"map2":   map2,
		}},
	}
}

type Arrays struct {
	option *schema.Option
}

func (a *Arrays) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Array 样例"
	}

	return tip
}

func (a *Arrays) Options() *schema.Option {
	return a.option
}

func (a *Arrays) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := a.option.Action[action]
	if !ok {
		return a.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

func arr1() {
	var arr [2]string
	arr[0] = "hello"
	arr[1] = "world"
	fmt.Printf("%s\n", arr)

	var arr1 = [2]int{1, 2}
	// 打印字节码
	var arr2 = [...]byte{'a', 'e', 'd', 'c', 'B'}
	// var arr2 = [...]byte{"a" ,"e" ,"d" ,"c" ,"B" }//使用双引号会报类型错
	fmt.Println("arr1", arr1)
	fmt.Println("arr2", arr2)

	var slice = []int{1, 2, 3, 4, 5}
	slice1 := slice
	slice1[0] = 11
	// slice是引用类型，slice会指向一个底层的array
	// slice1赋值后，slice也会变化
	fmt.Println("slice", slice)
	fmt.Println("slice1", slice1)
	fmt.Println("len(slice)", len(slice))
	fmt.Println("cap(slice)", cap(slice))

	// array不是引用类型，它的拷贝就是值的拷贝
	// 数组默认值是零值，对于 `int` 数组来说，元素的零值是 `0`。
	arra := [2]int{100, 101}
	arrb := arra
	arra[0] = 99
	fmt.Println("arra", arra)
	fmt.Println("arrb", arrb)

	slicem := make([]int, 10, 20)
	slicem[1] = 11
	// slicem [0 11 0 0 0 0 0 0 0 0] 10 20
	fmt.Println("slicem", slicem, len(slicem), cap(slicem))

	var str = "hello"
	fmt.Printf("str[0]=%c len(str)=%d\n", str[0], len(str))
	for index, val := range str {
		fmt.Printf("str[%d]=%c\n", index, val)
	}

	a := "啊"
	fmt.Println("一个中文字默认编码是utf-8占三个字节", len(a))

	var byteSlice []byte
	byteSlice = []byte(str)
	// 字节必须使用单引号包起来
	// 我们修改0的值  一个字节 所以只能一位 填写多位值报错
	// 中文字符在unicode下占2个字节,在utf-8编码下占3个字节,
	// 而golang默认编码是utf-8
	byteSlice[0] = 'a'
	fmt.Println(string(byteSlice))
}

func arr2() {
	TwoSum := func(item []int, target int) {
		for i := 0; i < len(item); i++ {
			other := target - item[i]
			for j := i + 1; j < len(item); j++ {
				if item[j] == other {
					fmt.Printf("(%d, %d)\n", i, j)
				}
			}
		}
	}

	item := []int{1, 3, 5, 6, 22, 7, 8}
	TwoSum(item, 8)
}

// append函数会改变slice所引用的数组的内容，从而影响到引用同一数组的其它slice。
// 但当slice中没有剩余空间（即(cap-len) == 0）时，此时将动态分配新的数组空间。
// 返回的slice数组指针将指向这个空间，而原数组的内容将保持不变；
// 其它引用此数组的slice则不受影响。

func slice1() {
	// 与数组不同，slice 的类型仅由它所包含的元素的类型决定（与元素个数无关）。
	// 要创建一个长度不为 0 的空 slice，需要使用内建函数 `make`。
	// 这里我们创建了一个长度为 3 的 `string` 类型的 slice（初始值为零值）。
	s := make([]string, 3)
	fmt.Println("s: ", s)

	// 当开始和结束位置都被忽略时，生成的切片将表示和原切片一致的切片，并且生成的切片与原切片在数据内容上也是一致的
	s2 := s[:]
	// 前面的三个空还存在，往后面累加
	// s2 = append(s2, "1", "2", "3")
	s2 = append(s2, []string{"1", "2", "3"}...)
	fmt.Println("s2", s2)

	// 我们可以和数组一样设置和得到值
	s[0] = "a"
	s[1] = "b"
	s[2] = "c"
	fmt.Println("set:", s)
	fmt.Println("get:", s[2])

	// `len` 返回 slice 的长度
	fmt.Println("len:", len(s))

	// 除了基本操作外，slice 支持比数组更丰富的操作。比如 slice 支持内建函数 `append`，
	// 该函数会返回一个包含了一个或者多个新值的 slice。
	// 注意由于 `append` 可能返回一个新的 slice，我们需要接收其返回值。
	s = append(s, "d")
	s = append(s, "e", "f")
	fmt.Println("apd:", s)

	// slice 还可以 `copy`。这里我们创建一个空的和 `s` 有相同长度的 slice——`c`，
	// 然后将 `s` 复制给 `c`。
	c := make([]string, len(s))
	copy(c, s)
	fmt.Println("cpy:", c)

	// slice 支持通过 `slice[low:high]` 语法进行“切片”操作。
	// 例如，右边的操作可以得到一个包含元素 `s[2]`、`s[3]` 和 `s[4]` 的 slice。
	l := s[2:5]
	fmt.Println("sl1:", l)

	// 这个 slice 包含从 `s[0]` 到 `s[5]`（不包含 5）的元素。
	l = s[:5]
	fmt.Println("sl2:", l)

	// 这个 slice 包含从 `s[2]`（包含 2）之后的元素。
	l = s[2:]
	fmt.Println("sl3:", l)

	// 我们可以在一行代码中声明并初始化一个 slice 变量。
	t := []string{"g", "h", "i"}
	fmt.Println("dcl:", t)

	// Slice 可以组成多维数据结构。内部的 slice 长度可以不一致，这一点和多维数组不同。
	twoD := make([][]int, 3)
	for i := 0; i < 3; i++ {
		innerLen := i + 1
		twoD[i] = make([]int, innerLen)
		for j := 0; j < innerLen; j++ {
			twoD[i][j] = i + j
		}
	}
	fmt.Println("2d: ", twoD)

	// 可变数量的参数
	multiParam := func(args ...string) {
		for _, v := range args {
			fmt.Println(v)
		}
	}

	multiParam("王尼玛", "张三")
	names := []string{"jerry", "herry"}
	multiParam(names...)

	// 下面这种复制不对 会把原数据也改变
	a := [...]int{1, 2, 3}
	a1 := a[:]
	a2 := a[:]
	a1[0] = 100
	fmt.Printf("a=%v a1=%v\n", a, a1)

	a2[1] = 100
	fmt.Printf("a=%v a2=%v\n", a, a2)
}

func map1() {
	// _map_ 是 Go 内建的[关联数据类型](http://zh.wikipedia.org/wiki/关联数组)
	// map 类似于 java 的 HashMap，Python的字典(dict)，是一种存储键值对(Key-Value)的数据解构。
	// （在一些其他的语言中也被称为 _哈希(hash)_ 或者 _字典(dict)_ ）。
	type Profile struct {
		Name    string // 名字
		Age     int    // 年龄
		Married bool   // 已婚
	}

	list := []Profile{
		{Name: "张三", Age: 30, Married: true},
		{Name: "李四", Age: 21},
		{Name: "王麻子", Age: 22},
	}

	fmt.Printf("%#v\n", list)

	// 要创建一个空 map，需要使用内建函数 `make`：`make(map[key-type]val-type)`。
	m := make(map[string]int)

	// 使用典型的 `make[key] = val` 语法来设置键值对。
	m["k1"] = 7
	m["k2"] = 13

	// 打印 map。例如，使用 `fmt.Println` 打印一个 map，会输出它所有的键值对。
	fmt.Println("map:", m)

	// 使用 `name[key]` 来获取一个键的值。
	v1 := m["k1"]
	fmt.Println("v1: ", v1)

	// 内建函数 `len` 可以返回一个 map 的键值对数量。
	fmt.Println("len:", len(m))

	// 内建函数 `delete` 可以从一个 map 中移除键值对。
	delete(m, "k2")
	fmt.Println("map:", m)

	// 当从一个 map 中取值时，还有可以选择是否接收的第二个返回值，该值表明了 map 中是否存在这个键。
	// 这可以用来消除 `键不存在` 和 `键的值为零值` 产生的歧义，
	// 例如 `0` 和 `""`。这里我们不需要值，所以用 _空白标识符(blank identifier)_ _ 将其忽略。
	_, prs := m["k2"]
	fmt.Println("prs:", prs)

	// 你也可以通过右边的语法在一行代码中声明并初始化一个新的 map。
	n := map[string]int{"foo": 1, "bar": 2}
	fmt.Println("map:", n)
}

type Dict struct {
	data map[interface{}]interface{}
}

func (d *Dict) Get(key interface{}) interface{} {
	return d.data[key]
}

func (d *Dict) Set(key, value interface{}) {
	d.data[key] = value
}

func (d *Dict) Visit(callback func(k, v interface{}) bool) {
	if callback == nil {
		return
	}

	for k, v := range d.data {
		if !callback(k, v) {
			return
		}
	}
}

func (d *Dict) Clear() {
	d.data = make(map[interface{}]interface{})
}

func NewDict() *Dict {
	d := &Dict{}

	d.Clear()
	return d
}

func map2() {
	dict := NewDict()

	dict.Set("My Factory", 60)
	dict.Set("Terra Craft", 36)
	dict.Set("Don't Hungry", 24)

	val := dict.Get("Terra Craft")
	fmt.Println("Terra Craft: ", val)

	dict.Visit(func(k, v interface{}) bool {
		if v.(int) > 40 {
			fmt.Printf("%d大于40\n", v)
			return true
		}

		fmt.Printf("%d小于40\n", v)
		return true
	})
}
