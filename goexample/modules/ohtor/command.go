package ohtor

import (
	"fmt"
	"os"
)

var (
	AllStudents []*Student
	studentMgr  = &StudentMgr{}
)

type Student struct {
	Username string
	Sex      int
	Score    float32
	Grade    string
}

type StudentMgr struct {
	allStudent []*Student
}

func (p *StudentMgr) AddStudent(stu *Student) (err error) {
	for index, v := range p.allStudent {
		if v.Username == stu.Username {
			fmt.Printf("user %s success update\n\n", stu.Username)
			p.allStudent[index] = stu
			return
		}
	}

	p.allStudent = append(p.allStudent, stu)
	fmt.Printf("user %s success insert\n\n", stu.Username)
	return
}

func (p *StudentMgr) ModifyStudent(stu *Student) (err error) {

	for index, v := range p.allStudent {
		if v.Username == stu.Username {
			p.allStudent[index] = stu
			fmt.Printf("user %s success update\n\n", stu.Username)
			return
		}
	}
	fmt.Printf("user %s is not found\n", stu.Username)
	err = fmt.Errorf("user %s is not exists", stu.Username)
	return
}

func (p *StudentMgr) ShowAllStudent() {

	for _, v := range p.allStudent {
		fmt.Printf("user:%s info:%#v\n", v.Username, v)
	}
	fmt.Println()
}

func NewStudent(username string,
	sex int,
	score float32,
	grade string) (stu *Student) {
	stu = &Student{
		Username: username,
		Sex:      sex,
		Score:    score,
		Grade:    grade,
	}
	return
}

func ShowMenu() {
	fmt.Println("1. add student")
	fmt.Println("2. modify student")
	fmt.Println("3. show all student")
	fmt.Println("4. exited")
	fmt.Println("")
}

func InputStudent() *Student {
	var (
		username string
		sex      int
		grade    string
		score    float32
	)

	fmt.Println("please input username:")
	fmt.Scanf("%s\n", &username)
	fmt.Println("please input sex:[0|1]")
	fmt.Scanf("%d\n", &sex)
	fmt.Println("please input grade:[0-6]")
	fmt.Scanf("%s\n", &grade)
	fmt.Println("please input score:[0-100]")
	fmt.Scanf("%f\n", &score)

	stu := NewStudent(username, sex, score, grade)
	return stu
}

func AddStudent() {
	stu := InputStudent()
	for index, v := range AllStudents {
		if v.Username == stu.Username {
			fmt.Printf("user %s success update\n\n", stu.Username)
			AllStudents[index] = stu
			return
		}
	}

	AllStudents = append(AllStudents, stu)
	fmt.Printf("user %s success insert\n\n", stu.Username)
}

func ModifyStudent() {
	stu := InputStudent()
	for index, v := range AllStudents {
		if v.Username == stu.Username {
			AllStudents[index] = stu
			fmt.Printf("user %s success update\n\n", stu.Username)
			return
		}
	}

	fmt.Printf("user %s is not found\n", stu.Username)
}

func ShowAllStudent() {
	for _, v := range AllStudents {
		fmt.Printf("user:%s info:%#v\n", v.Username, v)
	}

	fmt.Println()
}

func Run4() {
	for {
		ShowMenu()

		var sel int
		fmt.Scanf("%d\n", &sel)
		switch sel {
		case 1:
			// AddStudent()
			stu := InputStudent()
			studentMgr.AddStudent(stu)
		case 2:
			// ModifyStudent()
			stu := InputStudent()
			studentMgr.ModifyStudent(stu)
		case 3:
			// ShowAllStudent()
			studentMgr.ShowAllStudent()
		case 4:
			os.Exit(0)
		}
	}
}
