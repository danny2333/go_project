package ohtor

import (
	"context"
	"fmt"
	"sync"
	"time"
)

func FuncsRunningContext(funcs []func() error, ctx context.Context) (err error) {
	var wg sync.WaitGroup
	errChan := make(chan error, len(funcs))

	for _, f := range funcs {
		wg.Add(1)
		go func(f func() error) {
			defer wg.Done()
			err := f()
			if err != nil {
				errChan <- err
			}
		}(f)
	}

	go func() {
		wg.Wait()
		errChan <- nil
	}()

	select {
	case err = <-errChan:
		return err
	case <-ctx.Done():
		return fmt.Errorf("time out")
	}
}

func test1() error {
	fmt.Println("执行第一个")
	// time.Sleep(time.Second * 1)
	// fmt.Println("第一个执行完毕")
	return nil
}

func test2() error {
	fmt.Println("执行第二个")
	// time.Sleep(time.Second * 2)
	// fmt.Println("第二个执行失败")
	return fmt.Errorf("第二个执行失败")
}

func test3() error {
	fmt.Println("执行第三个")
	// time.Sleep(time.Second * 3)
	// fmt.Println("第三个执行完毕")
	return nil
}

func Run() {
	funcs := []func() error{test1, test2, test3}
	ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Second))
	defer cancel()
	fmt.Println(FuncsRunningContext(funcs, ctx))
}
