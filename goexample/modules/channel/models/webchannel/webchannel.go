package webchannel

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

// 实际的业务处理
func sayhelloName(w http.ResponseWriter, r *http.Request) {
	param := r.URL.Query()
	id := param.Get("id")
	idArr := strings.Split(id, ",")
	// 调用生产者
	go produceIdToChannel(idArr)
	// 由于消费者用了sleep去休眠造成处理业务的假象，所以这里用多协程的方式去处理，大家可以试着取消循环来起多个线程的方法，就用一个线程来跑，看看花费的时间是多少？
	for i := 0; i < len(idArr); i++ {
		go consumIdFromChannel()
	}

	fmt.Fprintf(w, "Hello first webchannel!") // 这个写入到w的是输出到客户端的
}

func MainRun() {
	// http://localhost:9090/sayhelloName?id=1,2,3,4,5,6,7,8,9,10
	http.HandleFunc("/sayhelloName", sayhelloName)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

// 声明一个channel模拟队列，如果这里用一个缓冲的chan呢，看看话费的时间又是多少呢？
var picIdChannel chan string = make(chan string)

func produceIdToChannel(id []string) {
	t := time.Now()
	for i := 0; i < len(id); i++ {
		picIdChannel <- id[i]
		fmt.Println("生产picId", id[i])
	}
	fmt.Println("花费时间", time.Since(t))
}

func consumIdFromChannel() {

	for {
		time.Sleep(time.Second * 10)
		e, _ := <-picIdChannel
		fmt.Println("消费picId", e)
	}

}
