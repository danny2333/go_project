package channel

import (
	"errors"
	"fmt"
	"goexample/modules/channel/models/webchannel"
	"goexample/schema"
	"math/rand"
	"os"
	"os/signal"
	"sync"
	"sync/atomic"
	"syscall"
	"time"
)

func NewChannel() schema.Worker {
	return &Channel{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"select1":     select1,
			"atomic1":     atomic1,
			"buffer1":     buffer1,
			"signal1":     signal1,
			"ticktsLock":  ticktsLock,
			"webchannel1": webchannel1,
		}},
	}
}

type Channel struct {
	option *schema.Option
}

func (c *Channel) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Channel 样例"
	}

	return tip
}

func (c *Channel) Options() *schema.Option {
	return c.option
}

func (c *Channel) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]
	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

// Go 的 _选择器（select）_  让你可以同时等待多个通道操作。
// 将协程、通道和选择器结合，是 Go 的一个强大特性。
func select1() {
	// 两个通道中选择
	c1 := make(chan string)
	c2 := make(chan string)

	// 各个通道将在一定时间后接收一个值
	// 通过这种方式来模拟并行的携程执行（例如RPC操作）时造成的阻塞（耗时）
	go func() {
		time.Sleep(1 * time.Second)
		c1 <- "one"
	}()

	go func() {
		time.Sleep(2 * time.Second)
		c2 <- "two"
	}()

	// 我们使用select关键字来同时等待这两个值
	// 并打印各自接收的值
	for i := 0; i < 2; i++ {
		select {
		case msg1 := <-c1:
			fmt.Println("received", msg1)
		case msg2 := <-c2:
			fmt.Println("received", msg2)
		}
	}
}

// atomic1
// 使用 `sync/atomic` 包在多个协程中进行 _原子计数_。
func atomic1() {
	// 我们将使用一个无符号整型（永远是正整数）变量来表示这个计数器。
	var ops uint64

	// WaitGroup 帮助我们等待所有协程完成它们的工作。
	var wg sync.WaitGroup

	// 我们会启动 50 个协程，并且每个协程会将计数器递增 1000 次。
	for i := 0; i < 50; i++ {
		wg.Add(1)

		go func() {
			for c := 0; c < 1000; c++ {
				// 使用 `AddUint64` 来让计数器自动增加，
				// 使用 `&` 语法给定 `ops` 的内存地址。
				atomic.AddUint64(&ops, 1)
			}
			wg.Done()
		}()
	}

	// 等待，直到所有协程完成。
	wg.Wait()

	// 现在可以安全的访问 `ops`，因为我们知道，此时没有协程写入 `ops`，
	// 此外，还可以使用 `atomic.LoadUint64` 之类的函数，在原子更新的同时安全地读取它们。
	fmt.Println("ops:", ops)

}

// buffer1
// 默认情况下，通道是 _无缓冲_ 的，这意味着只有对应的接收（`<- chan`）
// 通道准备好接收时，才允许进行发送（`chan <-`）。
// _有缓冲通道_ 允许在没有对应接收者的情况下，缓存一定数量的值。
func buffer1() {
	// 这里我们 `make` 了一个字符串通道，最多允许缓存 2 个值。
	messages := make(chan string, 4)
	code := make(chan int, 1)

	// 由于此通道是有缓冲的，
	// 因此我们可以将这些值发送到通道中，而无需并发的接收。
	messages <- "buffered"
	messages <- "channel"
	messages <- "channel2"

	code <- 233
	messages <- "channel3"

	// 然后我们可以正常接收这两个值。
	fmt.Println(<-messages)
	fmt.Println(<-messages)
	fmt.Println(<-messages)
	fmt.Println(<-code)
	fmt.Println(<-messages)

	write := func(ch chan int) {
		for i := 0; i < 10; i++ {
			ch <- i
			time.Sleep(time.Second)
		}
		close(ch)
	}

	ch := make(chan int)
	go write(ch)
	for v := range ch {
		fmt.Println("read value", v, "from ch")
	}

	fmt.Println("------------------------------------")
	// 当使用通道作为函数的参数时，你可以指定这个通道是否为只读或只写。
	// 该特性可以提升程序的类型安全。

	// `ping` 函数定义了一个只能发送数据的（只写）通道。
	// 尝试从这个通道接收数据会是一个编译时错误。
	ping := func(pings chan<- string, msg string) {
		pings <- msg
	}

	// `pong` 函数接收两个通道，`pings` 仅用于接收数据（只读），`pongs` 仅用于发送数据（只写）。
	pong := func(pings <-chan string, pongs chan<- string) {
		msg := <-pings
		pongs <- msg
	}

	pings := make(chan string, 1)
	pongs := make(chan string, 1)
	ping(pings, "passed message")
	pong(pings, pongs)
	fmt.Println(<-pongs)
}

// 有时候，我们希望 Go 可以智能的处理 [Unix 信号](http://en.wikipedia.org/wiki/Unix_signal)。
// 例如，我们希望当服务器接收到一个 `SIGTERM` 信号时，能够优雅退出，
// 或者一个命令行工具在接收到一个 `SIGINT` 信号时停止处理输入信息。
// 我们这里讲的就是在 Go 中如何使用通道来处理信号。
func signal1() {

	// Go 通过向一个通道发送 `os.Signal` 值来发送信号通知。
	// 我们将创建一个通道来接收这些通知（同时再创建一个在程序结束时发送通知的通道）。
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	// `signal.Notify` 注册给定的通道，用于接收特定信号。
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	// 这个协程执行一个阻塞的信号接收操作。
	// 当它接收到一个值时，它将打印这个值，然后通知程序可以退出。
	go func() {
		sig := <-sigs
		fmt.Println()
		fmt.Println(sig)
		done <- true
	}()

	// 程序将在这里进行等待，直到它得到了期望的信号
	// （也就是上面的协程发送的 `done` 值），然后退出。
	fmt.Println("awaiting signal")
	<-done
	fmt.Println("exiting")
}

// ticktsLock
// 模拟火车站买票
// 火车票100张，4个售票口出售(4个goroutine)
func ticktsLock() {
	var wg sync.WaitGroup
	var mutex sync.Mutex
	var tickts = 100

	saleTickts := func(name string) {
		rand.Seed(time.Now().UnixNano())
		for {
			mutex.Lock()
			if tickts > 0 {
				time.Sleep(time.Duration(rand.Intn(1000)))
				fmt.Println(name, ":", tickts)
				tickts--
			} else {
				fmt.Println(name, "结束买票...")
				mutex.Unlock()
				break
			}
			mutex.Unlock()
		}
		wg.Done()
	}

	wg.Add(4)
	go saleTickts("售票口1")
	go saleTickts("售票口2")
	go saleTickts("售票口3")
	go saleTickts("售票口4")
	wg.Wait()
	fmt.Println("该趟列车所有票卖光了....程序结束")
}

func webchannel1() {
	webchannel.MainRun()
}
