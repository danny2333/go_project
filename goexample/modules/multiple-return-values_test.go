// 方法名以Test开头
// -v，可以让测试时显示详细的流程
// go test指定文件时默认执行文件内的所有测试用例。可以使用-run参数选择需要的测试用例单独执行
// TestA 和 TestAK 的测试用例都被执行，原因是-run跟随的测试用例的名称支持正则表达式，使用-run TestA$即可只执行 TestA 测试用例
// 当需要终止当前测试用例时，可以使用 FailNow

// 如果调用了main文件里面额方法就不能指定文件

package modules

import "testing"

// `(int, int)` 在这个函数中标志着这个函数返回 2 个 `int`。
func vals() (int, int) {
	return 3, 7
}

func TestA(t *testing.T) {
	// 将断言重构为一个函数，减少重复并提高测试的可读性
	assertCorrectMessage := func(t *testing.T, got, want int) {
		// t.Helper() 表明这个方法是辅助函数，通过这样，当测试失败时所报告的行号将在函数调用中而不是辅助函数内部，这样容易跟踪问题
		t.Helper()
		if got != want {
			t.Errorf("got %d want %d", got, want)
		}
	}

	// 子测试
	t.Run("正确的案例", func(t *testing.T) {
		a, b := vals()
		got := a + b
		want := 10
		assertCorrectMessage(t, got, want)
	})

	t.Run("错误的案例", func(t *testing.T) {
		a, b := vals()
		got := a + b
		want := 11
		assertCorrectMessage(t, got, want)
	})
}
