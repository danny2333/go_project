package modules

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha1"
	"errors"
	"fmt"
	"goexample/schema"
)

func NewSha1hashes() schema.Worker {
	return &Sha1hashes{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"sha1hashes": sha1hashes,
		}},
	}
}

type Sha1hashes struct {
	option *schema.Option
}

func (a *Sha1hashes) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Sha1hashes 样例"
	}

	return tip
}

func (a *Sha1hashes) Options() *schema.Option {
	return a.option
}

func (a *Sha1hashes) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := a.option.Action[action]
	if !ok {
		return a.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

// [_SHA1 散列（hash）_](http://en.wikipedia.org/wiki/SHA-1)
// 经常用于生成二进制文件或者文本块的短标识。
// 例如，[git 版本控制系统](http://git-scm.com/) 大量的使用了 SHA1
// 来标识受版本控制的文件和目录。
// Go 在多个 `crypto/*` 包中实现了一系列散列函数。
// 这是 Go 中如何进行 SHA1 散列计算的例子。
func sha1hashes() {
	encrypt := func(plaintext, key, nonce []byte) []byte {
		block, err := aes.NewCipher(key)
		if err != nil {
			panic(err.Error())
		}
		aesgcm, err := cipher.NewGCM(block)
		if err != nil {
			panic(err.Error())
		}
		ciphertext := aesgcm.Seal(nil, nonce, plaintext, nil)
		return ciphertext
	}
	decrypt := func(ciphertext, key, nonce []byte) []byte {
		block, err := aes.NewCipher(key)
		if err != nil {
			panic(err.Error())
		}
		aesgcm, err := cipher.NewGCM(block)
		if err != nil {
			panic(err.Error())
		}
		plaintext, err := aesgcm.Open(nil, nonce, ciphertext, nil)
		if err != nil {
			panic(err.Error())
		}
		return plaintext
	}

	s := "sha1 this string"

	// 产生一个散列值的方式是 `sha1.New()`，`sha1.Write(bytes)`，
	// 然后 `sha1.Sum([]byte{})`。这里我们从一个新的散列开始。
	h := sha1.New()

	// 写入要处理的字节。如果是一个字符串，
	// 需要使用 `[]byte(s)` 将其强制转换成字节数组。
	h.Write([]byte(s))

	// `Sum` 得到最终的散列值的字符切片。`Sum` 接收一个参数，
	// 可以用来给现有的字符切片追加额外的字节切片：但是一般都不需要这样做。
	bs := h.Sum(nil)

	// SHA1 值经常以 16 进制输出，例如在 git commit 中。
	// 我们这里也使用 `%x` 来将散列结果格式化为 16 进制字符串。
	fmt.Println(s)
	fmt.Printf("%x\n", bs)

	fmt.Println()

	fmt.Println("AES encryption with GCM")
	plaintext := []byte("Some plain text")
	key := []byte("secretkey32bytessecretkey32bytes")
	nonce := make([]byte, 12)
	ciphertext := encrypt(plaintext, key, nonce)
	fmt.Printf("Ciphertext: %x\n", ciphertext)
	recoveredPt := decrypt(ciphertext, key, nonce)
	fmt.Printf("Recovered plaintext: %s\n", recoveredPt)
}
