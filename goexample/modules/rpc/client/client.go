package client

import (
	"errors"
	"fmt"
	"goexample/schema"
	"log"
	"net/rpc"
)

func NewRpcClient() schema.Worker {
	return &RpcClient{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"client1": client1,
		}},
	}
}

type RpcClient struct {
	option *schema.Option
}

func (c *RpcClient) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "RpcClient 样例"
	}

	return tip
}

func (c *RpcClient) Options() *schema.Option {
	return c.option
}

func (c *RpcClient) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]
	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

func client1() {
	client, err := rpc.Dial("tcp", "localhost:1234")
	if err != nil {
		log.Fatal("dialing:", err)
	}

	var reply string
	err = client.Call("HelloService.Hello", "王尼玛", &reply)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(reply)
}
