package server

import (
	"errors"
	"goexample/schema"
	"log"
	"net"
	"net/rpc"
)

func NewRpcServer() schema.Worker {
	return &RpcServer{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"server1": server1,
		}},
	}
}

type RpcServer struct {
	option *schema.Option
}

func (c *RpcServer) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "RpcServer 样例"
	}

	return tip
}

func (c *RpcServer) Options() *schema.Option {
	return c.option
}

func (c *RpcServer) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]
	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

const HelloServiceName = "HelloService"

type HelloServiceInterface = interface {
	Hello(request string, reply *string) error
}

func RegisterHelloService(svc HelloServiceInterface) error {
	return rpc.RegisterName(HelloServiceName, svc)
}

type HelloService struct{}

func (p *HelloService) Hello(request string, reply *string) error {
	*reply = "今天天气好好哟: " + request
	return nil
}

func server1() {
	rpc.RegisterName("HelloService", new(HelloService))

	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal("ListenTCP error:", err)
	}

	conn, err := listener.Accept()
	if err != nil {
		log.Fatal("Accept error:", err)
	}

	rpc.ServeConn(conn)
}
