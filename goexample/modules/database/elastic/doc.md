docker pull elasticsearch:6.4.3

-p 隐射端口
-e 设置参数，discovery.type=single-node，设置单节点，ES_JAVA_OPTS="-Xms256m -Xmx256m"，设置 JVM 参数
-d 后台运行
--name 节点名称

docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e ES_JAVA_OPTS="-Xms256m -Xmx256m" -d --name ES01 elasticsearch:6.4.3

```
docker run -d --name es -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" b0e9f9f047e6

-d：后台启动
--name：容器名称
-p：端口映射
-e：设置环境变量
discovery.type=single-node：单机运行
b0e9f9f047e6：镜像id
如果启动不了，可以加大内存设置：-e ES_JAVA_OPTS="-Xms512m -Xmx512m"
```


## 创建索引 elasticsearchtest 类似SQL中DB
```
curl -X PUT http://localhost:9200/test

PUT  http://localhost:9200/elasticsearchtest
{
  "acknowledged": true,
  "shards_acknowledged": true,
  "index": "elasticsearchtest"
}
```

## 创建Type elasticsearchtest/account 类似SQL中Table
```
POST  http://localhost:9200/elasticsearchtest/account
{
  "id": 1,
  "name": "dsds",
  "index": "elasticsearchtest"
}
```

## Docker部署ElasticSearch-Heard
```
docker pull  mobz/elasticsearch-head:5

docker create --name elasticsearch-head -p 9100:9100 mobz/elasticsearch-head:5

docker start elasticsearch-head

如果跨域进入容器修改elasticsearch.yml文件
docker exec -it ES01 /bin/bash

vi config/elasticsearch.yml

添加
http.cors.enabled: true
http.cors.allow-origin: "*"

退出重启
exit
docker restart es
```