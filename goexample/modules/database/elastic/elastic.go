package elastic

import (
	"errors"
	"fmt"
	olivere "goexample/modules/database/elastic/pkg/eorm"
	"goexample/schema"
)

func NewElastic() schema.Worker {
	return &Elastic{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"elastic1": elastic1,
		}},
	}
}

type Elastic struct {
	option *schema.Option
}

func (c *Elastic) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Elastic 样例"
	}

	return tip
}

func (c *Elastic) Options() *schema.Option {
	return c.option
}

func (c *Elastic) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]

	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

func elastic1() {
	olivere.Init("http://192.168.99.232:9200/")
	c := olivere.GetElasticDefault()
	v := c.GetVersion("http://192.168.99.232:9200/")
	fmt.Println(v)
}
