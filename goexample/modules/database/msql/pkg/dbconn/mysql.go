package dbconn

import (
	"database/sql"
	"errors"
	"fmt"
	"sync"

	_ "github.com/go-sql-driver/mysql"
)

type MySQLPool struct{}

var (
	instance *MySQLPool
	once     sync.Once
	DB       *sql.DB
)

// GetInstance 单例模式
func GetInstance() *MySQLPool {
	once.Do(func() {
		instance = &MySQLPool{}
	})

	return instance
}

// InitPool ...
func (pool *MySQLPool) InitPool() (db *sql.DB) {
	dsn := "root:root@tcp(127.0.0.1:3306)/demo1?charset=utf8"
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		panic(errors.New("MySQL连接失败"))
	}

	db.SetMaxIdleConns(50)
	db.SetMaxOpenConns(50)
	DB = db
	return db
}

// ----------------

// GetQueryResults 获取表数据及其对应字段
func GetQueryResults(database, query string, params []string) ([]string, [][]string, error) {
	db := DB

	if len(database) > 0 {
		_, err := db.Exec(fmt.Sprintf("USE %s;", database))
		if err != nil {
			return nil, nil, err
		}
	}

	var interfaceSlice = make([]interface{}, len(params))
	for i, d := range params {
		interfaceSlice[i] = d
	}

	rows, err := db.Query(query, interfaceSlice...)
	if err != nil {
		return nil, nil, err
	}

	columns, err := rows.Columns()
	if err != nil {
		return nil, nil, err
	}

	values := make([]sql.RawBytes, len(columns))

	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}

	resultRows := [][]string{}

	for rows.Next() {
		tableRow := make([]string, len(columns))

		err = rows.Scan(scanArgs...)
		if err != nil {
			return nil, nil, err
		}

		var value string
		for i, col := range values {
			if col == nil {
				value = "NULL"
			} else {
				value = string(col)
			}
			tableRow[i] = value
		}

		resultRows = append(resultRows, tableRow)
	}

	if err = rows.Err(); err != nil {
		return nil, nil, err
	}

	return columns, resultRows, nil
}
