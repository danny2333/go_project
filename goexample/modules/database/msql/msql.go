package msql

import (
	"encoding/json"
	"errors"
	"fmt"
	"goexample/modules/database/msql/pkg/dbconn"
	"goexample/modules/database/msql/pkg/dbconn2"
	"goexample/modules/database/msql/pkg/dbconn3"
	"goexample/schema"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func NewMsql() schema.Worker {
	return &Msql{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"mysql1": mysql1,
			"mysql2": mysql2,
			"mysql3": mysql3,
		}},
	}
}

type Msql struct {
	option *schema.Option
}

func (c *Msql) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Msql 样例"
	}

	return tip
}

func (c *Msql) Options() *schema.Option {
	return c.option
}

func (c *Msql) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]

	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

func mysql1() {
	//数据库连接池初始化
	orm := dbconn.GetInstance().InitPool()
	if orm == nil {
		fmt.Println("init database pool failure...")
	}
	defer orm.Close()

	var db = "demo1"
	var query = "SELECT * FROM `posts` WHERE id = ? AND title = ?"
	var params = []string{"1", "danny111@gmail.com"}
	columns, rows, err := dbconn.GetQueryResults(db, query, params)
	if err != nil {
		return
	}

	fmt.Printf("%#v\n", columns)

	fmt.Printf("%#v\n", rows)
}

type User struct {
	ID        string `json:"id"`
	Username  string `json:"username"`
	Password  string `json:"password"`
	CreatedAt string `json:"created_at"`
}

func mysql2() {
	db := dbconn2.Mysql{}

	// selectData
	selectData := func() {
		db.Sql = "select * from users where username='小明'"
		users, err := db.Query()
		if err != nil {
			fmt.Println(err.Error())
		}
		// fmt.Printf("%#v\n", users)

		/* b2, err := json.Marshal(users)
		if err != nil {
			fmt.Println(err.Error())
		}
		fmt.Println(string(b2)) */

		// /////////////////////////////////
		var userall []User
		// map转struct
		for _, v := range users {

			b3, err := json.Marshal(v)
			if err != nil {
				fmt.Println(err.Error())
			}
			user := User{}
			if err := json.Unmarshal(b3, &user); err != nil {
				fmt.Println(err.Error())
			}
			userall = append(userall, user)
		}
		// fmt.Println(userall)
		fmt.Printf("%v\n", userall)
	}
	selectData()

	// insertData
	_ = func() {
		data := make(map[string]interface{})
		data["table"] = "users"
		data["data"] = map[string]string{
			"username":   "如花3",
			"password":   "123456",
			"age":        "22",
			"created_at": time.Now().Format("2006/01/02 15:04:05"),
		}
		db.Data = data

		/*err := db.Insert()
		if err != nil {
			fmt.Println(err.Error())
		}*/

		id, err := db.InsertBegin()
		if err != nil {
			fmt.Println(err.Error())
		}
		fmt.Println("Insert ID: ", id)
	}
	// insertData()

	// updateData
	_ = func() {
		data := make(map[string]interface{})
		data["table"] = "users"
		data["set"] = map[string]string{
			"username":   "王尼玛",
			"password":   "66666",
			"created_at": time.Now().Format("2006/01/02 15:04:05"),
		}
		data["where"] = map[string]string{
			"id": "29",
		}
		db.Data = data

		err := db.UpdateBegin()
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		fmt.Println("更新数据成功")
	}
	// updateData()

	// deleteData
	_ = func() {
		data := make(map[string]interface{})
		data["table"] = "users"
		data["where"] = map[string]string{
			"id": "29",
		}

		db.Data = data

		err := db.DeleteBegin()
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		fmt.Println("删除数据成功")
	}
	// deleteData()
}

func mysql3() {
	M := dbconn3.NewModel("users")

	// selectData
	_ = func() {
		// res := M.Field("id,username").Order("id desc").Where("id = 1").Limit(1).Get()
		res := M.Field("id,username").Order("id desc").Where("id = 1").Get()
		// res := M.Field("id,username").Order("id desc").Get()

		fmt.Printf("%#v\n", res)
	}
	// selectData()

	// getOneData
	getOneData := func() {
		res := M.Field("*").Where("id = 1").GetOne()
		// fmt.Println(res.(map[int]map[string]string)[0]["username"])
		fmt.Println(res)
	}
	getOneData()

	// insertData
	_ = func() {
		data := make(map[string]interface{})
		data["username"] = "王尼玛"
		data["password"] = "123456"
		data["created_at"] = time.Now().Format("2006/01/02 15:04:05")

		res := M.Insert(data)
		fmt.Println(res)
	}
	// insertData()

	// updateData
	_ = func() {
		data := make(map[string]interface{})
		data["username"] = "王尼玛2"
		data["password"] = "654321"
		data["created_at"] = time.Now().Format("2006/01/02 15:04:05")

		res := M.Where("id = 30").Update(data)
		fmt.Println(res)
	}
	// updateData()

	// deleteData
	_ = func() {
		res := M.Delete(30)
		fmt.Println(res)
	}
	// deleteData()
}
