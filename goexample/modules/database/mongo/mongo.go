package mongo

import (
	"errors"
	"fmt"
	mongo "goexample/modules/database/mongo/pkg/gorm"
	"goexample/schema"
	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func NewMongo() schema.Worker {
	return &Mongo{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"mongo1": mongo1,
			"mongo2": mongo2,
		}},
	}
}

type Mongo struct {
	option *schema.Option
}

func (c *Mongo) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Mongo 样例"
	}

	return tip
}

func (c *Mongo) Options() *schema.Option {
	return c.option
}

func (c *Mongo) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]

	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

// ### \bin
// .\mongod.exe --dbpath ..\data\

type User struct {
	Id       bson.ObjectId `bson:"_id"`
	Name     string        `bson:"name"`
	PassWord string        `bson:"pass_word"`
	Age      int           `bson:"age"`
}

func mongo1() {
	// db, err := mgo.Dial("mongodb://192.168.2.28:27017,192.168.2.28:27018,192.168.2.28:27019/?replicaSet=howie")
	db, err := mgo.Dial("mongodb://127.0.0.1:27017?maxPoolSize=100")
	// db, err := mgo.Dial("mongodb://127.0.0.1:27017?replicaSet=howie")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	db.SetMode(mgo.Monotonic, true)
	c := db.DB("howie").C("person")

	//插入
	c.Insert(&User{
		Id:       bson.NewObjectId(),
		Name:     "王尼玛",
		PassWord: "123132",
		Age:      2,
	}, &User{
		Id:       bson.NewObjectId(),
		Name:     "李小明",
		PassWord: "qwer",
		Age:      5,
	}, &User{
		Id:       bson.NewObjectId(),
		Name:     "张大胆",
		PassWord: "6666",
		Age:      7,
	})

	var users []User

	c.Find(nil).All(&users) //查询全部数据
	log.Println(users)

	// c.FindId(users[0].Id).All(&users) //通过ID查询
	// log.Println(users)

	// c.Find(bson.M{"name": "JK_WEI"}).All(&users) //单条件查询(=)
	// log.Println(users)

	// c.Find(bson.M{"name": bson.M{"$ne": "JK_WEI"}}).All(&users) //单条件查询(!=)
	// log.Println(users)

	// c.Find(bson.M{"age": bson.M{"$gt": 5}}).All(&users) //单条件查询(>)
	// log.Println(users)

	// c.Find(bson.M{"age": bson.M{"$gte": 5}}).All(&users) //单条件查询(>=)
	// log.Println(users)

	// c.Find(bson.M{"age": bson.M{"$lt": 5}}).All(&users) //单条件查询(<)
	// log.Println(users)

	// c.Find(bson.M{"age": bson.M{"$lte": 5}}).All(&users) //单条件查询(<=)
	// log.Println(users)

	// c.Find(bson.M{"name": bson.M{"$in": []string{"JK_WEI", "JK_HE"}}}).All(&users) //单条件查询(in)
	// log.Println(users)

	// c.Find(bson.M{"$or": []bson.M{bson.M{"name": "JK_WEI"}, bson.M{"age": 7}}}).All(&users) //多条件查询(or)
	// log.Println(users)

	// c.Update(bson.M{"_id": users[0].Id}, bson.M{"$set": bson.M{"name": "JK_HOWIE", "age": 61}}) //修改字段的值($set)
	// c.FindId(users[0].Id).All(&users)
	// log.Println(users)

	// c.Find(bson.M{"name": "JK_CHENG", "age": 66}).All(&users) //多条件查询(and)
	// log.Println(users)

	// c.Update(bson.M{"_id": users[0].Id}, bson.M{"$inc": bson.M{"age": -6}}) //字段增加值($inc)
	// c.FindId(users[0].Id).All(&users)
	// log.Println(users)

	// //c.Update(bson.M{"_id": users[0].Id}, bson.M{"$push": bson.M{"interests": "PHP"}}) //从数组中增加一个元素($push)
	// c.Update(bson.M{"_id": users[0].Id}, bson.M{"$pull": bson.M{"interests": "PHP"}}) //从数组中删除一个元素($pull)
	// c.FindId(users[0].Id).All(&users)
	// log.Println(users)

	// c.Remove(bson.M{"name": "JK_CHENG"}) //删除
}

func initDb() {
	mongo.MustConnect("mongodb://localhost:27017", "usermanager")
}

func mongo2() {
	initDb()
	value := mongo.Instance.FindOne("agent", bson.M{
		"username": "gufeng",
	})
	fmt.Printf("%v", value)
}
