package gredis

import (
	"errors"
	"goexample/modules/database/gredis/handles"
	"goexample/modules/database/gredis/pkg/rorm"
	"goexample/schema"
	"log"
	"net/http"
	"time"
)

func NewGredis() schema.Worker {
	return &Gredis{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"redis1": redis1,
		}},
	}
}

type Gredis struct {
	option *schema.Option
}

func (c *Gredis) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Gredis 样例"
	}

	return tip
}

func (c *Gredis) Options() *schema.Option {
	return c.option
}

func (c *Gredis) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]

	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

type Author struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func test() {
	redisClient := rorm.Initialize()
	key1 := "sampleKey"
	value1 := &Author{Name: "danny ding", Age: 22}
	err := redisClient.SetKey(key1, value1, time.Minute*1)
	if err != nil {
		log.Fatalf("Error: %v", err.Error())
	}

	value2 := &Author{}
	err = redisClient.GetKey(key1, value2)
	if err != nil {
		log.Fatalf("Error: %v", err.Error())
	}

	log.Printf("Name: %s", value2.Name)
	log.Printf("Age: %d", value2.Age)
}

func redis1() {
	// test()
	http.HandleFunc("/database/redis/scores", handles.ScoresHandle)
	http.ListenAndServe(":8080", nil)
}
