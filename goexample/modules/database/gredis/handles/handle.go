package handles

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"goexample/modules/database/gredis/pkg/rorm"

	"github.com/go-redis/redis"
)

// curl -X GET "localhost:8080/database/redis/scores?start=0&stop=-1"
// curl -X POST localhost:8080/database/redis/scores -H "Content-Type: application/json" -d '{"nickname": "peter", "steps": 1456}'
func ScoresHandle(w http.ResponseWriter, r *http.Request) {
	redisClient := rorm.Initialize

	params := map[string]interface{}{}

	resp := map[string]interface{}{}

	var err error

	if r.Method == http.MethodGet {
		for k, v := range r.URL.Query() {
			params[k] = v[0]
		}

		resp, err = getScores(redisClient(), params)
	} else if r.Method == http.MethodPost {
		err = json.NewDecoder(r.Body).Decode(&params)
		resp, err = addScore(redisClient(), params)
	}

	enc := json.NewEncoder(w)
	enc.SetIndent("", "  ")

	/* if err != nil {
		log.Println("aaaaaaaaaaaaaa")
		resp = map[string]interface{}{
			"error": err.Error(),
		}
		fmt.Println(resp)
		// enc.Encode(resp)
	} else {
		if encodingErr := enc.Encode(resp); encodingErr != nil {
			fmt.Println("{ error: " + encodingErr.Error() + "}")
		}
	} */

	if err != nil {
		resp = map[string]interface{}{
			"error": err.Error(),
		}
	}
	if encodingErr := enc.Encode(resp); encodingErr != nil {
		fmt.Println("{ error: " + encodingErr.Error() + "}")
	}

}

func addScore(c *rorm.RedisClient, p map[string]interface{}) (map[string]interface{}, error) {
	// ctx := context.TODO()

	nickname := p["nickname"].(string)
	steps := p["steps"].(float64)

	err := c.C.ZAdd("app_users", redis.Z{
		Score:  steps,
		Member: nickname,
	}).Err()

	if err != nil {
		return nil, err
	}

	rank := c.C.ZRank("app_users", p["nickname"].(string))

	if err != nil {
		return nil, err
	}

	resp := map[string]interface{}{
		"data": map[string]interface{}{
			"nickname": p["nickname"].(string),
			"rank":     rank.Val(),
		},
	}

	return resp, nil
}

func getScores(c *rorm.RedisClient, p map[string]interface{}) (map[string]interface{}, error) {
	// return nil, errors.New("自定错误")
	start, err := strconv.ParseInt(fmt.Sprint(p["start"]), 10, 64)
	if err != nil {
		return nil, err
	}

	stop, err := strconv.ParseInt(fmt.Sprint(p["stop"]), 10, 64)
	if err != nil {
		return nil, err
	}

	// ZCount 统计某个分数范围内的元素个数
	total, err := c.C.ZCount("app_users", "-inf", "+inf").Result()
	if err != nil {
		return nil, err
	}

	// 返回有序集 key 中， score 值介于 max 和 min 之间(默认包括等于 max 或 min )的所有的成员。
	// 有序集成员按 score 值递减(从大到小)的次序排列。
	scores, err := c.C.ZRevRangeWithScores("app_users", start, stop).Result()
	if err != nil {
		return nil, err
	}

	data := []map[string]interface{}{}

	for _, z := range scores {
		record := map[string]interface{}{}
		rank := c.C.ZRank("app_users", z.Member.(string))

		if err != nil {
			return nil, err
		}

		record["nickname"] = z.Member.(string)
		record["score"] = z.Score
		record["rank"] = rank.Val()
		data = append(data, record)
	}

	countPerRequest := stop - start + 1
	if stop == -1 {
		countPerRequest = total
	}

	resp := map[string]interface{}{
		"data": data,
		"meta": map[string]interface{}{
			"start":      start,
			"stop":       stop,
			"per_requet": countPerRequest,
			"total":      total,
		},
	}
	return resp, nil
}
