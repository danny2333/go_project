package modules

import (
	"bufio"
	"errors"
	"fmt"
	"goexample/schema"
	"net"
	"os"
	"strings"
)

func NewTelnet() schema.Worker {
	return &Telnet{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"telnet1": telnet1,
		}},
	}
}

type Telnet struct {
	option *schema.Option
}

func (c *Telnet) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Telnet 样例"
	}

	return tip
}

func (c *Telnet) Options() *schema.Option {
	return c.option
}

func (c *Telnet) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]
	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

// telnet1 Go语言Telnet回音服务器 http://c.biancheng.net/view/vip_7352.html
func telnet1() {
	// 创建一个程序结束码的通道
	exitChan := make(chan int)

	// 将服务器并发运行
	go server("127.0.0.1:7001", exitChan)

	// 通道阻塞，等待接收返回值
	code := <-exitChan
	fmt.Printf("code: %d\n", code)

	// 标记程序返回值并退出
	os.Exit(code)
}

func server(address string, exitChan chan int) {
	// 根据地址进行侦听
	l, err := net.Listen("tcp", address)

	if err != nil {
		fmt.Println(err.Error())
		exitChan <- 1
	}

	fmt.Println("Listen: " + address)

	defer l.Close()

	for {
		conn, err := l.Accept()

		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		go handleSession(conn, exitChan)
	}
}

func handleSession(conn net.Conn, exitChan chan int) {
	fmt.Println("Session strated:")

	// 创建一个网络连接数据的读取器
	reader := bufio.NewReader(conn)

	// 接收循环的数据
	for {
		// 读取字符串，直到碰到回车返回
		str, err := reader.ReadString('\n')

		// 读取数据正确
		if err == nil {
			// 去掉字符串尾部的回车
			str = strings.TrimSpace(str)

			// 处理Telnet指令
			if !processTelnetCommand(str, exitChan) {
				conn.Close()
				break
			}

			// Echo逻辑，发什么数据，原样返回
			conn.Write([]byte(str + "\r\n"))
		} else {
			// 发生错误
			fmt.Println("Session closed")
			conn.Close()
			break
		}
	}
}

func processTelnetCommand(str string, exitChan chan int) bool {
	// @close指令表示终止本次会话
	if strings.HasPrefix(str, "@close") {
		fmt.Println("Session closed")

		// 告诉外部需要断开连接
		return false
	} else if strings.HasPrefix(str, "@shutdown") {
		// @shutdown指令表示终止服务进程

		fmt.Println("Server shutdown")

		// 往通道中写入0，阻塞等待接收方处理
		exitChan <- 0

		// 告诉外部需要断开连接
		return false
	}

	fmt.Println(str)
	return true
}
