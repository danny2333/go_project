# struct 结构体
```go
package main

import "fmt"

// Movie 首先使用关键字 type 声明结构体
type Movie struct {
	Name   string
	Rating float32
}

func Run() {

	// 然后创建实例化结构体 : 每个元素都需要使用逗号 ，结尾
	// 简短变量创建
	m := Movie{
		Name:   "www.ydook.com",
		Rating: 10,
	}
	fmt.Println(m)

	// 显式创建
	var M Movie
	M.Name = "MyName"
	M.Rating = 9
	fmt.Println(M)

	// new() 简短变量创建
	movi := new(Movie)
	movi.Name = "New() struct"
	movi.Rating = 8
	fmt.Println(movi)

}
```

## 匿名结构体
```golang

// 在函数外部定义匿名结构体并赋值给 
var config struct {
  APIKey string
  OAuthConfig oauth.Config
}

// 定义并初始化并赋值给 
datadata := struct {
  Title string
  Users []*User
}{
  title,
  users
}
```