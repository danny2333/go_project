package modules

import (
	"bufio"
	b64 "encoding/base64"
	"errors"
	"fmt"
	"goexample/schema"
	"io/ioutil"
	"math"
	"math/rand"
	"os"
	"os/exec"
	"sort"
	"strconv"
	"strings"
	"time"
)

// 状态协程 Stateful
// https://learnku.com/docs/gobyexample/2020/go-state-process-stateful/6290

func NewDEMO() schema.Worker {
	return &DEMO{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"for":                  fors,
			"base64":               base64,
			"dtype":                dtype,
			"closures":             closures,
			"constants":            constants,
			"environmentvariables": environmentvariables,
			"execingprocesses":     execingprocesses,
			"strings1":             strings1,
			"strings2":             strings2,
			"fmtstring":            fmtstring,
			"fmtstring2":           fmtstring2,
			"init":                 golanginit,
			"linefilters":          linefilters,
			"numberparsing":        numberparsing,
			"pointers":             pointers,
			"randomnumbers":        randomnumbers,
			"sort":                 sort1,
			"sort2":                sort2,
		}},
	}
}

type DEMO struct {
	option *schema.Option
}

func (d *DEMO) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "DEMO 样例"
	}

	return tip
}

func (d *DEMO) Options() *schema.Option {
	return d.option
}

func (d *DEMO) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := d.option.Action[action]
	if !ok {
		return d.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	// map[string]interface{}
	// fu.(func())()
	fu()

	return nil, nil
}

func fors() {
	// `for` 是 Go 中唯一的循环结构。这里会展示 `for` 循环的三种基本使用方式。
	// 最基础的方式，单个循环条件。
	i := 1
	for i <= 3 {
		fmt.Println(i)
		i = i + 1
	}

	// 经典的初始/条件/后续 `for` 循环。
	for j := 7; j <= 9; j++ {
		fmt.Println(j)
	}

	// 不带条件的 `for` 循环将一直重复执行，
	// 直到在循环体内使用了 `break` 或者 `return` 跳出循环。
	for {
		fmt.Println("loop")
		break
	}

	// 你也可以使用 `continue` 直接进入下一次循环。
	for n := 0; n <= 5; n++ {
		if n%2 == 0 {
			continue
		}
		fmt.Println(n)
	}
}

func base64() {
	// 这是要编解码的字符串。
	data := "abc123!?$*&()'-=@~"

	// Go 同时支持标准 base64 以及 URL 兼容 base64。
	// 这是使用标准编码器进行编码的方法。
	// 编码器需要一个 `[]byte`，因此我们将 string 转换为该类型。
	sEnc := b64.StdEncoding.EncodeToString([]byte(data))
	fmt.Println(sEnc)

	// 解码可能会返回错误，如果不确定输入信息格式是否正确，
	// 那么，你就需要进行错误检查了。
	sDec, _ := b64.StdEncoding.DecodeString(sEnc)
	fmt.Println(string(sDec))
	fmt.Println()

	// 使用 URL base64 格式进行编解码。
	uEnc := b64.URLEncoding.EncodeToString([]byte(data))
	fmt.Println(uEnc)
	uDec, _ := b64.URLEncoding.DecodeString(uEnc)
	fmt.Println(string(uDec))
}

/*
bool

string

int  int8  int16  int32  int64
uint uint8 uint16 uint32 uint64 uintptr

byte // uint8 的别名

rune // int32 的别名
     // 代表一个Unicode码

float32 float64

complex64 complex128
*/
func dtype() {
	// todo
}

// Go 支持[_匿名函数_](http://zh.wikipedia.org/wiki/%E5%8C%BF%E5%90%8D%E5%87%BD%E6%95%B0)，
// 并能用其构造 <a href="http://zh.wikipedia.org/wiki/%E9%97%AD%E5%8C%85_(%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%A7%91%E5%AD%A6)"><em>闭包</em></a>。
// 匿名函数在你想定义一个不需要命名的内联函数时是很实用的。

func closures() {
	// 闭包
	// `intSeq` 函数返回一个在其函数体内定义的匿名函数。
	// 返回的函数使用闭包的方式 _隐藏_ 变量 `i`。
	// 返回的函数 _隐藏_ 变量 `i` 以形成闭包。
	intSeq := func() func() int {
		i := 0
		return func() int {
			i++
			return i
		}
	}

	// 我们调用 `intSeq` 函数，将返回值（一个函数）赋给 `nextInt`。
	// 这个函数的值包含了自己的值 `i`，这样在每次调用 `nextInt` 时，都会更新 `i` 的值。
	nextInt := intSeq()

	// 通过多次调用 `nextInt` 来看看闭包的效果。
	fmt.Println(nextInt())
	fmt.Println(nextInt())
	fmt.Println(nextInt())

	// 为了确认这个状态对于这个特定的函数是唯一的，我们重新创建并测试一下。
	newInts := intSeq()
	fmt.Println(newInts())

	f1 := func(f func()) {
		fmt.Println("f1")
		f()
	}

	f2 := func(x, y int) {
		fmt.Println("f2", x+y)
	}

	f3 := func(f func(int, int), m, n int) func() {
		tmp := func() {
			f(m, n)
		}
		fmt.Println("f3")
		return tmp
	}

	ret := f3(f2, 1, 2)
	f1(ret)

}

func constants() {
	// `const` 用于声明一个常量。
	const s string = "constant"
	// `const` 语句可以出现在任何 `var` 语句可以出现的地方
	const n = 500000000

	// 常数表达式可以执行任意精度的运算
	const ss = 3e20 / n
	fmt.Println(ss)

	// 数值型常量没有确定的类型，直到被给定某个类型，比如显式类型转化。
	fmt.Println(int64(ss))

	// 一个数字可以根据上下文的需要（比如变量赋值、函数调用）自动确定类型。
	// 举个例子，这里的 `math.Sin` 函数需要一个 `float64` 的参数，`n` 会自动确定类型。
	fmt.Println(math.Sin(n))

	// a=100 b=100 c=200 d=200
	const (
		a int = 100
		b
		c int = 200
		d
	)

	fmt.Printf("a=%d b=%d c=%d d=%d\n", a, b, c, d)

	// 0 1 2 8 8
	const (
		e = iota
		f
		g
		h = 8
		i
	)

	// <<表示左移操作
	// 1 << iota 表示将1的二进制表示向左移iota位
	// 1 2 4
	const (
		a1 = 1 << iota
		a2 = 1 << iota
		a3 = 1 << iota
	)
	fmt.Printf("e=%d f=%d g=%d h=%d i=%d\n", e, f, g, h, i)
	fmt.Printf("a1=%d a2=%d a3=%d\n", a1, a2, a3)
	defer fmt.Println("end...")
}

// [_环境变量_](http://zh.wikipedia.org/wiki/%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F)
// 是一种[向 Unix 程序传递配置信息](http://www.12factor.net/config)的常见方式。
// 让我们来看看如何设置、获取以及列出环境变量。

func environmentvariables() {
	// isEnvExist 检查环境变量是否存在
	isEnvExist := func(key string) bool {
		if _, ok := os.LookupEnv(key); ok {
			return true
		}

		return false
	}

	// 使用 `os.Setenv` 来设置一个键值对。
	// 使用 `os.Getenv`获取一个键对应的值。
	// 如果键不存在，将会返回一个空字符串。
	os.Setenv("FOO", "1")
	fmt.Println("FOO:", os.Getenv("FOO"))
	fmt.Println("BAR:", os.Getenv("BAR"))

	fmt.Println(isEnvExist("a"))
	// 使用 `os.Environ` 来列出所有环境变量键值对。
	// 这个函数会返回一个 `KEY=value` 形式的字符串切片。
	// 你可以使用 `strings.SplitN` 来得到键和值。这里我们打印所有的键。
	for _, e := range os.Environ() {
		fmt.Printf("env: %v\n", e)
		pair := strings.SplitN(e, "=", 2)
		fmt.Println(pair[0])
	}

}

func execingprocesses() {
	// sysType := runtime.GOOS

	// 在这个例子中，我们将执行 `ls` 命令。
	// Go 要求我们提供想要执行的可执行文件的绝对路径，
	// 所以我们将使用 `exec.LookPath` 找到它（应该是 `/bin/ls`）。
	/* binary, lookErr := exec.LookPath("ls")
	if lookErr != nil {
		panic(lookErr)
	}

	// `Exec` 需要的参数是切片的形式的（不是放在一起的一个大字符串）。
	// 我们给 `ls` 一些基本的参数。注意，第一个参数需要是程序名。
	args := []string{"ls", "-a", "-l", "-h"}

	// `Exec` 同样需要使用[环境变量](environment-variables)。
	// 这里我们仅提供当前的环境变量。
	env := os.Environ()

	// 这里是真正的 `syscall.Exec` 调用。
	// 如果这个调用成功，那么我们的进程将在这里结束，并被 `/bin/ls -a -l -h` 进程代替。
	// 如果存在错误，那么我们将会得到一个返回值。
	execErr := syscall.Exec(binary, args, env)
	if execErr != nil {
		panic(execErr)
	} */

	// 我们将从一个简单的命令开始，没有参数或者输入，仅打印一些信息到标准输出流。
	// `exec.Command` 可以帮助我们创建一个对象，来表示这个外部进程。
	dateCmd := exec.Command("date")

	// `.Output` 是另一个帮助函数，常用于处理运行命令、等待命令完成并收集其输出。
	// 如果没有错误，`dateOut` 将保存带有日期信息的字节。
	dateOut, err := dateCmd.Output()
	if err != nil {
		panic(err)
	}
	fmt.Println(string(dateOut))

	// 下面我们将看看一个稍复杂的例子，
	// 我们将从外部进程的 `stdin` 输入数据并从 `stdout` 收集结果。
	grepCmd := exec.Command("grep", "hello")

	// 这里我们明确的获取输入/输出管道，运行这个进程，
	// 写入一些输入数据、读取输出结果，最后等待程序运行结束。
	grepIn, _ := grepCmd.StdinPipe()
	grepOut, _ := grepCmd.StdoutPipe()
	grepCmd.Start()
	grepIn.Write([]byte("hello grep\ngoodbye grep"))
	grepIn.Close()
	grepBytes, _ := ioutil.ReadAll(grepOut)
	grepCmd.Wait()

	// 上面的例子中，我们忽略了错误检测，
	// 当然，你也可以使用常见的 `if err != nil` 方式来进行错误检查。
	// 我们只收集了 `StdoutPipe` 的结果，
	// 但是你可以使用相同的方法收集 `StderrPipe` 的结果。
	fmt.Println("> grep hello")
	fmt.Println(string(grepBytes))

	// 注意，在生成命令时，我们需要提供一个明确描述命令和参数的数组，而不能只传递一个命令行字符串。
	// 如果你想使用一个字符串生成一个完整的命令，那么你可以使用 `bash` 命令的 `-c` 选项：
	lsCmd := exec.Command("bash", "-c", "ls -a -l -h")
	lsOut, err := lsCmd.Output()
	if err != nil {
		panic(err)
	}
	fmt.Println("> ls -a -l -h")
	fmt.Println(string(lsOut))
}

// 标准库的 `strings` 包提供了很多有用的字符串相关的函数。
// 这儿有一些用来让你对 `strings` 包有一个初步了解的例子。
func strings1() {
	// 我们给 `fmt.Println` 一个较短的别名，
	// 因为我们随后会大量的使用它。
	var p = fmt.Println

	// 这是一些 `strings` 中有用的函数例子。
	// 由于它们都是包的函数，而不是字符串对象自身的方法，
	// 这意味着我们需要在调用函数时，将字符串作为第一个参数进行传递。
	// 你可以在 [`strings`](http://golang.org/pkg/strings/) 包文档中找到更多的函数。
	p("Contains:  ", strings.Contains("test", "es"))
	p("Count:     ", strings.Count("test", "t"))
	p("HasPrefix: ", strings.HasPrefix("test", "te"))
	p("HasSuffix: ", strings.HasSuffix("test", "st"))
	p("Index:     ", strings.Index("test", "e"))
	p("Join:      ", strings.Join([]string{"a", "b"}, "-"))
	p("Repeat:    ", strings.Repeat("a", 5))
	p("Replace:   ", strings.Replace("foo", "o", "0", -1))
	p("Replace:   ", strings.Replace("foo", "o", "0", 1))
	p("Split:     ", strings.Split("a-b-c-d-e", "-"))
	p("ToLower:   ", strings.ToLower("TEST"))
	p("ToUpper:   ", strings.ToUpper("test"))
	p()

	// 虽然不是 `strings` 的函数，但仍然值得一提的是，
	// 获取字符串长度（以字节为单位）以及通过索引获取一个字节的机制。
	p("Len: ", len("hello"))
	p("Char:", "hello"[1])
}

func strings2() {

}

func fmtstring() {
	//fmt占位符
	//fmt.Printf()格式化输出
	//fmt.Print()连续不换行输出
	//fmt.Println()换行输出

	//打印内存地址
	ss := []int{1, 2, 3}
	fmt.Printf("%p\n", &ss) //使用%p  传的值得是内存地址（指针）
	fmt.Printf("%p\n", &ss[0])
	fmt.Printf("%p\n", &ss[1])
	fmt.Printf("%p\n", &ss[2])

	n := 100
	//打印类型
	fmt.Printf("%T\n", n)
	//打印值
	fmt.Printf("%v\n", n)
	//打印二进制
	fmt.Printf("%b\n", n)
	//打印十进制
	fmt.Printf("%d\n", n)
	//打印八进制
	fmt.Printf("%o\n", n)
	//打印十六进制
	fmt.Printf("%x\n", n)

	f := 12.34
	fmt.Printf("浮点数:%T\n", f)
	fmt.Printf("%b\n", f) //无小数部分、二进制指数的科学计数法，如-123456p-78
	fmt.Printf("%e\n", f) //科学计数法，如-1234.456e+78
	fmt.Printf("%E\n", f) //科学计数法，如-1234.456E+78
	fmt.Printf("%f\n", f) //有小数部分但无指数部分，如123.456
	fmt.Printf("%F\n", f) //同%f
	fmt.Printf("%g\n", f) //根据实际情况采用%e或%f格式（以获得更简洁、准确的输出）
	fmt.Printf("%G\n", f) //根据实际情况采用%E或%F格式（以获得更简洁、准确的输出）

	s := "hello/wzb"
	//打印字符串
	fmt.Printf("string:%s\n", s)
	fmt.Printf("value:%v\n", s)
	o := struct{ name string }{"teizhu"}
	fmt.Printf("%+v\n", o) //输出结构体时会添加字段名
	fmt.Printf("%v\n", o)
	fmt.Printf("%#v\n", o) //值的Go语法表示
	sc := 'c'
	fmt.Printf("字符:%c\n", sc)    //打印字符
	fmt.Println(len(s))          //字符串长度
	ret := strings.Split(s, "/") //字符串分割
	fmt.Println(ret)
	fmt.Printf("%T\n", ret)
	fmt.Println(strings.Contains(s, "wzb")) //包含
	fmt.Println(strings.HasPrefix(s, "h"))  //前缀
	fmt.Println(strings.HasSuffix(s, "h"))  //后缀

	sIndex := "adfadfwett"
	fmt.Println(strings.Index(sIndex, "a")) //查找index
	fmt.Println(strings.Join(ret, "+"))     //拼接

	//字符串中拿出具体的字符
	strRune := "hello铁柱你好안녕하세요."
	for _, c := range strRune {
		fmt.Printf("%c\n", c)
	}
	//字符串修改
	s1 := "白萝卜"
	s2 := []rune(s1) //1.将字符串转换为一个rune切片  rune的类型即为int32
	s2[0] = '红'      //2。修改的话必须用单引号 因为装换位rune后所有的选择都是字符
	fmt.Println(s2)
	fmt.Println(string(s2)) //把切片强制转换为字符串

	//Fprint
	// 向标准输出写入内容
	fmt.Fprintln(os.Stdout, "向标准输出写入内容")
	fileObj, err := os.OpenFile("./test2.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("打开文件出错，err:", err)
		return
	}
	name := "阿无的吴"
	//向打开的文件句柄中写入内容
	fmt.Fprintf(fileObj, "往文件中写如信息：%s", name)

	ss1 := fmt.Sprint("沙河小王子")
	name1 := "沙河小王子"
	age := 18
	ss2 := fmt.Sprintf("name:%s,age:%d", name1, age)
	ss3 := fmt.Sprintln("沙河小王子")
	fmt.Println(ss1, ss2, ss3)

	//Errorf
	err3 := fmt.Errorf("这是一个错误") //定义一个error
	fmt.Println(err3)
	e := errors.New("原始错误e")
	fmt.Println(e)
	w := fmt.Errorf("Wrap了一个错误%w", e)
	fmt.Println(w)
	/* var (
		name3   string
		age3    int
		married bool
	)
	fmt.Scan(&name3, &age3, &married)
	fmt.Printf("扫描结果 name:%s age:%d married:%t \n", name3, age3, married)
	*/
	reader := bufio.NewReader(os.Stdin) // 从标准输入生成读对象
	fmt.Print("请输入内容：")
	text, _ := reader.ReadString('\n') // 读到换行
	text = strings.TrimSpace(text)
	fmt.Printf("%#v\n", text)
}

func fmtstring2() {
	type point struct {
		x, y int
	}
	// Go 提供了一些用于格式化常规值的打印“动词”。
	// 例如，这样打印 `point` 结构体的实例。
	p := point{1, 2}
	fmt.Printf("%v\n", p)

	// 如果值是一个结构体，`%+v` 的格式化输出内容将包括结构体的字段名。
	fmt.Printf("%+v\n", p)

	// `%#v` 根据 Go 语法输出值，即会产生该值的源码片段。
	fmt.Printf("%#v\n", p)

	// 需要打印值的类型，使用 `%T`。
	fmt.Printf("%T\n", p)

	// 格式化布尔值很简单。
	fmt.Printf("%t\n", true)

	// 格式化整型数有多种方式，使用 `%d` 进行标准的十进制格式化。
	fmt.Printf("%d\n", 123)

	// 这个输出二进制表示形式。
	fmt.Printf("%b\n", 14)

	// 输出给定整数的对应字符。
	fmt.Printf("%c\n", 33)

	// `%x` 提供了十六进制编码。
	fmt.Printf("%x\n", 456)

	// 同样的，也为浮点型提供了多种格式化选项。
	// 使用 `%f` 进行最基本的十进制格式化。
	fmt.Printf("%f\n", 78.9)

	// `%e` 和 `%E` 将浮点型格式化为（稍微有一点不同的）科学记数法表示形式。
	fmt.Printf("%e\n", 123400000.0)
	fmt.Printf("%E\n", 123400000.0)

	// 使用 `%s` 进行基本的字符串输出。
	fmt.Printf("%s\n", "\"string\"")

	// 像 Go 源代码中那样带有双引号的输出，使用 `%q`。
	fmt.Printf("%q\n", "\"string\"")

	// 和上面的整型数一样，`%x` 输出使用 base-16 编码的字符串，
	// 每个字节使用 2 个字符表示。
	fmt.Printf("%x\n", "hex this")

	// 要输出一个指针的值，使用 `%p`。
	fmt.Printf("%p\n", &p)

	// 格式化数字时，您经常会希望控制输出结果的宽度和精度。
	// 要指定整数的宽度，请在动词 "%" 之后使用数字。
	// 默认情况下，结果会右对齐并用空格填充。
	fmt.Printf("|%6d|%6d|\n", 12, 345)

	// 你也可以指定浮点型的输出宽度，同时也可以通过 `宽度.精度` 的语法来指定输出的精度。
	fmt.Printf("|%6.2f|%6.2f|\n", 1.2, 3.45)

	// 要左对齐，使用 `-` 标志。
	fmt.Printf("|%-6.2f|%-6.2f|\n", 1.2, 3.45)

	// 你也许也想控制字符串输出时的宽度，特别是要确保他们在类表格输出时的对齐。
	// 这是基本的宽度右对齐方法。
	fmt.Printf("|%6s|%6s|\n", "foo", "b")

	// 要左对齐，和数字一样，使用 `-` 标志。
	fmt.Printf("|%-6s|%-6s|\n", "foo", "b")

	// 到目前为止，我们已经看过 `Printf` 了，
	// 它通过 `os.Stdout` 输出格式化的字符串。
	// `Sprintf` 则格式化并返回一个字符串而没有任何输出。
	s := fmt.Sprintf("a %s", "string")
	fmt.Println(s)

	// 你可以使用 `Fprintf` 来格式化并输出到 `io.Writers` 而不是 `os.Stdout`。
	fmt.Fprintf(os.Stderr, "an %s\n", "error")
}

// 1 先执行全局变量初始化
// 2 然后再执行init函数
// 3 最后执行main函数

// "_" 可以简单理解为赋值但以后不再使用
/* var _ int64 = s()

func init() {
	fmt.Println("开始执行init函数")
}

func s() int64 {
	fmt.Println("开始初始化const/var")
	return 1
} */
func golanginit() {
	fmt.Println("开始执行main函数")
}

func linefilters() {
	// _行过滤器（line filter）_ 是一种常见的程序类型，
	// 它读取 stdin 上的输入，对其进行处理，然后将处理结果打印到 stdout。
	// var buf [16]byte
	// os.Stdin.Read(buf[:])
	// // fmt.Println(string(buf[:]))
	// os.Stdout.WriteString(string(buf[:]))

	//只读的方式打开
	/* file, err := os.Open("./modules/init.md")
	if err != nil {
		fmt.Println("open file failed, err:", err)
		return
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println("read file failed, err:", err)
			return
		}
		fmt.Println(line)
	} */

	// 用带缓冲的 scanner 包装无缓冲的 `os.Stdin`，
	// 这为我们提供了一种方便的 `Scan` 方法，
	// 将 scanner 前进到下一个 `令牌`（默认为：下一行）。
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		// `Text` 返回当前的 token，这里指的是输入的下一行。
		ucl := strings.ToUpper(scanner.Text())

		// 输出转换为大写后的行。
		fmt.Println(ucl)
	}

	// 检查 `Scan` 的错误。
	// 文件结束符（EOF）是可以接受的，它不会被 `Scan` 当作一个错误。
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "error:", err)
		os.Exit(1)
	}

	/* file, err := os.OpenFile("./modules/test.dat", os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Println("open file failed, err:", err)
		return
	}

	defer file.Close()
	writer := bufio.NewWriter(file)
	// for i := 0; i < 10; i++ {
	// 	writer.WriteString("hello world\n")
	// }
	writer.WriteString("王尼玛\n")
	writer.Flush() */
}

func numberparsing() {
	// 内建的 `strconv` 包提供了数字解析能力。
	// 使用 `ParseFloat`，这里的 `64` 表示解析的数的位数。
	f, _ := strconv.ParseFloat("1.234", 64)
	fmt.Println(f)

	// 在使用 `ParseInt` 解析整型数时，
	// 例子中的参数 `0` 表示自动推断字符串所表示的数字的进制。
	// `64` 表示返回的整型数是以 64 位存储的。
	i, _ := strconv.ParseInt("123", 0, 64)
	fmt.Println(i)

	// `ParseInt` 会自动识别出字符串是十六进制数。
	d, _ := strconv.ParseInt("0x1c8", 0, 64)
	fmt.Println(d)

	// `ParseUint` 也是可用的。
	u, _ := strconv.ParseUint("789", 0, 64)
	fmt.Println(u)

	// `Atoi` 是一个基础的 10 进制整型数转换函数。
	k, _ := strconv.Atoi("135")
	fmt.Println(k)

	// 在输入错误时，解析函数会返回一个错误。
	_, e := strconv.Atoi("wat")
	fmt.Println(e)
}

func pointers() {
	// Go 支持 <em><a href="http://zh.wikipedia.org/wiki/%E6%8C%87%E6%A8%99_(%E9%9B%BB%E8%85%A6%E7%A7%91%E5%AD%B8)">指针</a></em>，
	// 允许在程序中通过 `引用传递` 来传递值和数据结构。

	// 我们将通过两个函数：`zeroval` 和 `zeroptr` 来比较 `指针` 和 `值`。
	// `zeroval` 有一个 `int` 型参数，所以使用值传递。
	// `zeroval` 将从调用它的那个函数中得到一个实参的拷贝：ival。
	zeroval := func(ival int) {
		ival = 0
	}

	// `zeroptr` 有一个和上面不同的参数：`*int`，这意味着它使用了 `int` 指针。
	// 紧接着，函数体内的 `*iptr` 会 _解引用_ 这个指针，从它的内存地址得到这个地址当前对应的值。
	// 对解引用的指针赋值，会改变这个指针引用的真实地址的值。
	zeroptr := func(iptr *int) {
		*iptr = 0
	}

	i := 1
	fmt.Println("initial:", i)

	zeroval(i)
	fmt.Println("zeroval:", i)

	// 通过 `&i` 语法来取得 `i` 的内存地址，即指向 `i` 的指针。
	zeroptr(&i)
	fmt.Println("zeroptr:", i)

	// 指针也是可以被打印的。
	fmt.Println("pointer:", &i)

	// ------------------------------------
	// 指针即某个值的地址，类型定义时使用符号*，
	// 对一个已经存在的变量，使用&获取改变量的地址

	// str := "王尼玛"
	// var p *string = &str // p是指向str的指针
	// *p = "如花"
	// fmt.Println(str) // Hello修改了p，str的值也发生了改变

	// 指针通常在函数传递参数，或者给某个类型定义新的方法时使用。
	// Go语言中，参数是按值传递的，如果不使用指针，函数内部将会拷贝一份参数的副本，
	// 对参数的修改并不会影响到外部变量的值。如果参数使用指针，
	// 对参数的传递将会影响到外部变量。
	num := 100
	add := func(num int) {
		num += 1
	}

	realAdd := func(num *int) {
		*num += 1
	}

	add(num)
	fmt.Println(num) // 100 num没有变化

	realAdd(&num)
	fmt.Println(num) // 101，指针传递，num被修改

}

func randomnumbers() {
	// Go 的 `math/rand` 包提供了[伪随机数](http://en.wikipedia.org/wiki/Pseudorandom_number_generator)生成器。

	// 例如，`rand.Intn` 返回一个随机的整数 n，且 `0 <= n < 100`。
	fmt.Println(rand.Intn(100))
	fmt.Println(rand.Intn(100))
	fmt.Println()

	// `rand.Float64` 返回一个64位浮点数 `f`，且 `0.0 <= f < 1.0`。
	fmt.Println(rand.Float64())

	// 这个技巧可以用来生成其他范围的随机浮点数，
	// 例如，`5.0 <= f < 10.0`
	fmt.Println((rand.Float64() * 5) + 5)
	fmt.Println((rand.Float64() * 5) + 5)
	fmt.Println()

	// 默认情况下，给定的种子是确定的，每次都会产生相同的随机数数字序列。
	// 要产生不同的数字序列，需要给定一个不同的种子。
	// 注意，对于想要加密的随机数，使用此方法并不安全，
	// 应该使用 `crypto/rand`。
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)

	// 调用上面返回的 `rand.Rand`，就像调用 `rand` 包中函数一样。
	fmt.Print(r1.Intn(100), ",")
	fmt.Print(r1.Intn(100))
	fmt.Println()

	// 如果使用相同种子生成的随机数生成器，会生成相同的随机数序列。
	s2 := rand.NewSource(42)
	r2 := rand.New(s2)
	fmt.Print(r2.Intn(100), ",")
	fmt.Print(r2.Intn(100))
	fmt.Println()
	s3 := rand.NewSource(42)
	r3 := rand.New(s3)
	fmt.Print(r3.Intn(100), ",")
	fmt.Print(r3.Intn(100))

	// Go 支持 <a href="http://zh.wikipedia.org/wiki/%E9%80%92%E5%BD%92_(%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%A7%91%E5%AD%A6)"><em>递归</em></a>。
	// 这里是一个经典的阶乘示例。
	// `fact` 函数在到达 `fact(0)` 前一直调用自身。

}

// Go 的 `sort` 包实现了内建及用户自定义数据类型的排序功能。
// 我们先来看看内建数据类型的排序。
func sort1() {
	// 排序方法是针对内置数据类型的；
	// 这是一个字符串排序的例子。
	// 注意，它是原地排序的，所以他会直接改变给定的切片，而不是返回一个新切片。
	strs := []string{"c", "a", "b"}
	sort.Strings(strs)
	fmt.Println("Strings:", strs)

	// 一个 `int` 排序的例子。
	ints := []int{7, 2, 4}
	sort.Ints(ints)
	fmt.Println("Ints:   ", ints)

	// 我们也可以使用 `sort` 来检查一个切片是否为有序的。
	s := sort.IntsAreSorted(ints)
	fmt.Println("Sorted: ", s)

	// 插入排序
	// 8, 3, 2, 9, 4, 6,10, 0
	// 2, 3, 8,
	insert_sort := func(a [8]int) [8]int {
		for i := 1; i < len(a); i++ {
			for j := i; j > 0; j-- {
				if a[j] < a[j-1] {
					a[j], a[j-1] = a[j-1], a[j]
				} else {
					break
				}
			}
		}
		return a
	}

	// 选择排序
	// 8, 3, 2, 9, 4, 6,10, 0
	// 0, 2, 3
	select_sort := func(a [8]int) [8]int {
		for i := 0; i < len(a); i++ {
			for j := i + 1; j < len(a); j++ {
				if a[j] < a[i] {
					a[i], a[j] = a[j], a[i]
				}
			}
		}
		return a
	}

	// 冒泡排序
	bubble_sort := func(a [8]int) [8]int {
		for i := 0; i < len(a); i++ {
			for j := 0; j < len(a)-i-1; j++ {
				if a[j] > a[j+1] {
					a[j], a[j+1] = a[j+1], a[j]
				}
			}
		}
		return a
	}

	var a [8]int = [8]int{8, 3, 2, 9, 4, 6, 10, 0}
	var b [5]string = [5]string{"ac", "ec", "be", "fa", "ii"}
	var c [5]float64 = [5]float64{29.38, 22.32, 0.8, 99191.2}
	fmt.Println(insert_sort(a))
	fmt.Println(select_sort(a))
	fmt.Println(bubble_sort(a))

	sortdemo := func(a [8]int, b [5]string, c [5]float64) {
		sort.Ints(a[:])
		fmt.Println("a:", a)

		sort.Strings(b[:])
		fmt.Println("b:", b)

		sort.Float64s(c[:])
		fmt.Println("c:", c)
	}

	sortdemo(a, b, c)
}

// 有时候，我们可能想根据自然顺序以外的方式来对集合进行排序。
// 例如，假设我们要按字符串的长度而不是按字母顺序对它们进行排序。
// 这儿有一个在 Go 中自定义排序的示例。

// 为了在 Go 中使用自定义函数进行排序，我们需要一个对应的类型。
// 我们在这里创建了一个 `byLength` 类型，它只是内建类型 `[]string` 的别名。
type byLength []string

// 我们为该类型实现了 `sort.Interface` 接口的 `Len`、`Less` 和 `Swap` 方法，
// 这样我们就可以使用 `sort` 包的通用 `Sort` 方法了，
// `Len` 和 `Swap` 在各个类型中的实现都差不多，
// `Less` 将控制实际的自定义排序逻辑。
// 在这个的例子中，我们想按字符串长度递增的顺序来排序，
// 所以这里使用了 `len(s[i])` 和 `len(s[j])` 来实现 `Less`。
func (s byLength) Len() int {
	return len(s)
}
func (s byLength) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byLength) Less(i, j int) bool {
	return len(s[i]) < len(s[j])
}

// sort2 自定义排序
func sort2() {
	// 一切准备就绪后，我们就可以通过将切片 `fruits` 强转为 `byLength` 类型的切片，
	// 然后对该切片使用 `sort.Sort` 来实现自定义排序。
	fruits := []string{"peach", "banana", "kiwi"}
	sort.Sort(byLength(fruits))
	fmt.Println(fruits)
}
