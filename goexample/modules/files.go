package modules

import (
	"archive/zip"
	"bufio"
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"encoding/csv"
	"encoding/gob"
	"encoding/xml"
	"errors"
	"flag"
	"fmt"
	"goexample/schema"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"
)

func NewFiles() schema.Worker {
	return &Files{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"xml1":      xml1,
			"zip1":      zip1,
			"bin1":      bin1,
			"csv1":      csv1,
			"file1":     file1,
			"gob1":      gob1,
			"dir1":      dir1,
			"tmpfile1":  tmpfile1,
			"readfile1": readfile1,
		}},
	}
}

type Files struct {
	option *schema.Option
}

func (c *Files) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Files 样例"
	}

	return tip
}

func (c *Files) Options() *schema.Option {
	return c.option
}

func (c *Files) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]
	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

type document struct {
	Title string `xml:"title"`
	URL   string `xml:"url"`
	Text  string `xml:"abstract"`
	ID    int
}

func loadDocuments(path string) ([]document, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	dec := xml.NewDecoder(f)
	dump := struct {
		Documents []document `xml:"doc"`
	}{}
	if err := dec.Decode(&dump); err != nil {
		return nil, err
	}

	docs := dump.Documents
	for i := range docs {
		docs[i].ID = i
	}

	return docs, nil
}

func search(docs []document, term string) []document {
	var r []document
	for _, doc := range docs {
		if strings.Contains(doc.Text, term) {
			r = append(r, doc)
		}
	}

	return r
}

func searchReg(docs []document, term string) []document {
	re := regexp.MustCompile(`(?i)\b` + term + `\b`)
	var r []document
	for _, doc := range docs {
		if re.MatchString(doc.Text) {
			r = append(r, doc)
		}
	}

	return r
}

func xml1() {
	t1 := time.Now()
	// enwiki-latest-abstract1.xml
	// fmt.Println("Read Local File")

	filePath := flag.String("path", "./enwiki-latest-abstract1.xml", "请输入文件地址")

	flag.Parse()

	docs, err := loadDocuments(*filePath)
	if err != nil {
		fmt.Println("文件打开错误：", err.Error())
	}

	var keyword string

	fmt.Println("请输入关键词：")
	fmt.Scanln(&keyword)

	res := search(docs, keyword)
	// res := searchReg(docs, "cat")

	elapsed := time.Since(t1)
	fmt.Println("搜索结果：", res)
	fmt.Println("用时：", elapsed)
}

func zip1() {
	/* // 创建一个缓冲区用来保存压缩文件
	buf := new(bytes.Buffer)

	// 创建压缩文档
	zf := zip.NewWriter(buf)

	var files = []struct {
		Name string
		Body []string
	}{
		{Name: "Golang.txt", Body: []string{"aa", "bb", "cc"}},
	}

	for _, file := range files {
		f, err := zf.Create(file.Name)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println("type", reflect.TypeOf(file.Body))

		for _, b := range file.Body {
			_, err = f.Write([]byte(b + "\n"))
			if err != nil {
				fmt.Println(err)
			}
		}
	}

	// 关闭压缩文档
	err := zf.Close()
	if err != nil {
		fmt.Println(err)
	}

	// 将压缩文档内容写入文件
	f, err := os.OpenFile("file.zip", os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Println(err)
	}

	buf.WriteTo(f) */

	// 打开zip
	zf, err := zip.OpenReader("file.zip")
	if err != nil {
		fmt.Println(err.Error())
	}

	defer zf.Close()

	for _, f := range zf.File {
		fmt.Println("文件名：", f.Name)
		rc, err := f.Open()
		if err != nil {
			fmt.Println(err.Error())
		}
		_, err = io.CopyN(os.Stdout, rc, int64(f.UncompressedSize64))
		if err != nil {
			fmt.Println(err.Error())
		}

		rc.Close()
	}
}

// bin1 二进制文件
func bin1() {
	type Website = struct {
		URL int32
	}

	// 编码
	/* file, err := os.Create("output.bin")
	for i := 1; i <= 10; i++ {
		info := Website{int32(i)}
		if err != nil {
			fmt.Println("文件创建失败：", err.Error())
			return
		}
		defer file.Close()

		var binBuf bytes.Buffer
		binary.Write(&binBuf, binary.LittleEndian, info)
		b := binBuf.Bytes()
		_, err := file.Write(b)
		if err != nil {
			fmt.Println("编码失败：", err.Error())
			return
		}
	}
	fmt.Println("编码成功") */

	// 解码

	readNextBytes := func(file *os.File, number int) []byte {
		bytes := make([]byte, number)

		_, err := file.Read(bytes)
		if err != nil {
			fmt.Println("解码失败")
		}

		return bytes
	}

	file, err := os.Open("output.bin")
	if err != nil {
		fmt.Println("文件打开失败：", err.Error())
		return
	}

	defer file.Close()

	m := Website{}

	for i := 1; i <= 10; i++ {
		data := readNextBytes(file, 4)
		buffer := bytes.NewBuffer(data)
		err = binary.Read(buffer, binary.LittleEndian, &m)
		if err != nil {
			fmt.Println("二进制文件读取失败", err.Error())
			return
		}
		fmt.Println("第", i, "个值为：", m)
	}

}

func csv1() {
	f, err := os.Create("People.csv")
	if err != nil {
		fmt.Println("文件创建失败：", err.Error())
		return
	}

	writer := csv.NewWriter(f)
	var data = [][]string{
		{"Name", "Age", "Occupation"},
		{"Sally", "22", "Nurse"},
		{"Joe", "43", "Sportsman"},
		{"Louis", "39", "Author"},
	}

	err = writer.WriteAll(data)
	if err != nil {
		fmt.Println("写入失败：", err.Error())
		return
	}
	fmt.Println("成功写入csv")

}

func file1() {
	/* // 创建一个新文件，写入内容
	filePath := "./output.txt"
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("打开文件错误", err.Error())
		return
	}

	// 及时关闭
	defer file.Close()
	// 写入内容
	str := "https://www.baidu.com/\n" // \n\r标识换行 txt文件要看到换行效果要用\r\n
	// 写入时，使用带缓存的 *Writer
	writer := bufio.NewWriter(file)
	for i := 0; i < 3; i++ {
		writer.WriteString(str)
	}
	// 因为 writer 是带缓存的，因此在调用 WriteString 方法时，内容是先写入缓存的
	// 所以要调用flush方法，将缓存的数据真正写入到文件中
	writer.Flush() */

	// 读取文件
	file, err := os.Open("./output.txt")
	if err != nil {
		fmt.Println("文件打开失败", err.Error())
		return
	}
	// 关闭file句柄，防止内存泄漏
	defer file.Close()

	// 创建一个 *Reader，是带缓冲的
	reader := bufio.NewReader(file)
	for {
		str, err := reader.ReadString('\n') //读取到一个换行就结束
		if err == io.EOF {                  //io.EOF 表示文件的末尾
			break
		}
		fmt.Print(str)
	}

	fmt.Println("文件读取结束...")
}

func gob1() {
	/* // 创建gob文件
	info := map[string]string{
		"name":    "今天天气很坏",
		"website": "https://baidu.com",
	}

	name := "demo.gob"
	File, _ := os.OpenFile(name, os.O_RDWR|os.O_CREATE, 0777)
	defer File.Close()

	enc := gob.NewEncoder(File)
	if err := enc.Encode(info); err != nil {
		fmt.Println(err)
	} */

	// 读取gob文件
	var M map[string]string
	File, _ := os.Open("demo.gob")
	D := gob.NewDecoder(File)
	D.Decode(&M)
	fmt.Println(M)
}

/*
!---listen16
|    |----employee
|    |    |----employee.exe
|    |    |----main.go
|    |----empty_interface
|    |    |----empty_interface.exe
|    |    |----main.go
|    |----homework
|    |    |----tree
|    |    |    |----main.go
|    |    |    |----tree.exe
|    |----interface_nest
|    |    |----interface_nest.exe
|    |    |----main.go
|    |----interface_test
|    |    |----interface_test.exe
|    |    |----main.go
|    |----multi_interface
|    |    |----main.go
|    |    |----multi_interface.exe
|    |----pointer_interface
|    |    |----main.go
|    |    |----pointer_interface.exe
|    |----type_assert
|    |    |----main.go
|    |    |----type_assert.exe
*/
func Tree(dirPath string, deep int) (err error) {
	dir, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return err
	}
	if deep == 1 {
		fmt.Printf("!---%s\n", filepath.Base(dirPath))
	}

	// window的目录分隔符是 \
	// linux 的目录分隔符是 /
	sep := string(os.PathSeparator)
	for _, fi := range dir {
		//如果是目录，继续调用ListDir进行遍历
		if fi.IsDir() {
			fmt.Printf("|")
			for i := 0; i < deep; i++ {
				fmt.Printf("    |")
			}
			fmt.Printf("----%s\n", fi.Name())
			Tree(dirPath+sep+fi.Name(), deep+1)
			continue
		}

		fmt.Printf("|")
		for i := 0; i < deep; i++ {
			fmt.Printf("    |")
		}
		fmt.Printf("----%s\n", fi.Name())

	}
	return nil
}

// dir1 抓取本地数据例子
// https://github.com/gocn/news
func dir1() {
	if len(os.Args) > 1 {
		// Tree(os.Args[1], 1)
		Tree("./modules", 1)
	}

	// 应使用 `Join` 来构建可移植(跨操作系统)的路径。
	// 它接收任意数量的参数，并参照传入顺序构造一个对应层次结构的路径。
	p := filepath.Join("dir1", "dir2", "filename")
	fmt.Println("p:", p)

	// 您应该总是使用 `Join` 代替手动拼接 `/` 和 `\`。
	// 除了可移植性，`Join` 还会删除多余的分隔符和重复目录，使得路径更加规范。
	fmt.Println(filepath.Join("dir1//", "filename"))
	fmt.Println(filepath.Join("dir1/../dir1", "filename.jpg"))

	// `Dir` 和 `Base` 可以被用于分割路径中的目录和文件。
	// 此外，`Split` 可以一次调用返回上面两个函数的结果。
	fmt.Println("Dir(p):", filepath.Dir(p))
	fmt.Println("Base(p):", filepath.Base(p))

	// 判断路径是否为绝对路径。
	fmt.Println(filepath.IsAbs("dir/file"))
	fmt.Println(filepath.IsAbs("E:\\phpstudy_pro\\WWW"))

	// 某些文件名包含了扩展名（文件类型）。
	// 我们可以用 `Ext` 将扩展名分割出来。
	filename := "config.json"
	ext := filepath.Ext(filename)
	fmt.Println(ext)
	// 想获取文件名清除扩展名后的值，请使用 `strings.TrmSuffix`。
	fmt.Println(strings.TrimSuffix(filename, ext))

	// `Rel` 寻找 `basepath` 与 `targpath` 之间的相对路径。
	// 如果相对路径不存在，则返回错误。
	fmt.Println(filepath.Rel("/home/polaris/studygolang", "/home/polaris/studygolang/src/logic/topic.go"))
	fmt.Println(filepath.Rel("/home/polaris/studygolang", "/data/studygolang"))

	// Output:
	// src/logic/topic.go <nil>
	// ../../../data/studygolang <nil>
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

// 在程序运行时，我们经常创建一些运行时用到，程序结束后就不再使用的数据。
// *临时目录和文件* 对于上面的情况很有用，因为它不会随着时间的推移而污染文件系统。
func tmpfile1() {

	// 创建临时文件最简单的方法是调用 `ioutil.TempFile` 函数。
	// 它会创建并打开文件，我们可以对文件进行读写。
	// 函数的第一个参数传 `""`，`ioutil.TempFile` 会在操作系统的默认位置下创建该文件。
	f, err := ioutil.TempFile("", "sample")
	check(err)

	// 打印临时文件的名称。
	// 文件名以 `ioutil.TempFile` 函数的第二个参数作为前缀，
	// 剩余的部分会自动生成，以确保并发调用时，生成不重复的文件名。
	// 在类 Unix 操作系统下，临时目录一般是 `/tmp`。
	fmt.Println("Temp file name:", f.Name())

	// defer 删除该文件。
	// 尽管操作系统会自动在某个时间清理临时文件，但手动清理是一个好习惯。
	defer os.Remove(f.Name())

	// 我们可以向文件写入一些数据。
	_, err = f.Write([]byte{1, 2, 3, 4})
	check(err)

	// 如果需要写入多个临时文件，最好是为其创建一个临时 *目录* 。
	// `ioutil.TempDir` 的参数与 `TempFile` 相同，
	// 但是它返回的是一个 *目录名* ，而不是一个打开的文件。
	dname, err := ioutil.TempDir("", "sampledir")
	fmt.Println("Temp dir name:", dname)

	defer os.RemoveAll(dname)

	// 现在，我们可以通过拼接临时目录和临时文件合成完整的临时文件路径，并写入数据。
	fname := filepath.Join(dname, "file1")
	err = ioutil.WriteFile(fname, []byte{1, 2}, 0666)
	check(err)
}

// news 数据结构体
// https://github.com/gocn/news
type news struct {
	ID    int64
	Title string
	Link  string
	Ctime time.Time
}

// getFileList 遍历文件夹
func getFileList(dir string) []string {
	var files []string
	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		if f == nil {
			return err
		}

		if strings.Contains(path, ".git") {
			return nil
		}

		if f.IsDir() {
			return nil
		}

		baseName := filepath.Base(path)
		if !strings.Contains(baseName, "-") {
			return nil
		}

		ext := filepath.Ext(path)
		if ext != ".md" {
			return nil
		}

		files = append(files, path)
		return nil
	})

	if err != nil {
		return nil
	}

	return files
}

// getFileHash 获取文件哈希
func getFileHash(path string) string {
	file, err := os.Open(path)
	if err != nil {
		return ""
	}
	defer file.Close()

	h := md5.New()
	_, err = io.Copy(h, file)
	if err != nil {
		return ""
	}

	return fmt.Sprintf("%x", h.Sum(nil))
}

// getNews 获取新闻数据
func getNews(path string) []news {
	if runtime.GOOS == "windows" {
		path = strings.TrimSuffix(path, `"`)
		path = strings.Replace(path, "\\", "/", -1)
	}

	file, err := os.Open(path)
	if err != nil {
		return nil
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return nil
	}

	var newsList []news
	reg := regexp.MustCompile(`(?i)#{0,3}\s*GoCN每日新闻\(([\d-]*)\)\n+\D*((.*\n)+?\n)`)
	all := reg.FindAllSubmatch(data, -1)

	for _, item := range all {
		singleNews := news{}
		loc, _ := time.LoadLocation("Local")
		ctime, err := time.ParseInLocation("2006-01-02", string(item[1]), loc)
		if err == nil {
			singleNews.Ctime = ctime
		}
		sreg := regexp.MustCompile(`(\d)\.\s*(.*)\s+(http.*)\n?`)
		sall := sreg.FindAllSubmatch(item[2], -1)
		for _, sitem := range sall {
			id, err := strconv.Atoi(string(sitem[1]))
			if err == nil {
				singleNews.ID = int64(id)
			}
			singleNews.Title = strings.TrimSpace(string(sitem[2]))
			surl, err := url.Parse(string(sitem[3]))
			if err == nil {
				singleNews.Link = surl.String()
			}
			newsList = append(newsList, singleNews)
		}
	}

	return newsList

}

func readfile1() {
	files := getFileList("G:\\tempCode\\news")
	fmt.Println(files[0])

	hash := getFileHash(files[0])
	fmt.Println(hash)

	news := getNews(files[0])
	fmt.Printf("%#v\n", news[0])
}
