package web

import (
	"encoding/json"
	"errors"
	"fmt"
	"goexample/modules/web/pkg/pagination"
	"goexample/schema"
	"time"
)

func NewWeb() schema.Worker {
	return &Web1{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"web1": web1,
		}},
	}
}

type Web1 struct {
	option *schema.Option
}

func (c *Web1) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Web1 样例"
	}

	return tip
}

func (c *Web1) Options() *schema.Option {
	return c.option
}

func (c *Web1) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]
	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

type Article struct {
	Title       string    `json:"title"`
	CreatedTime time.Time `json:"created_time"`
	Description string    `json:"description"`
	Tags        []string  `json:"tags"`
	Author      string    `json:"author"`
	MusicId     string    `json:"music_id"`
	Category    string    `json:"category"`
}

type Articles []Article

func web1() {
	result := []map[string]interface{}{}

	for i := 0; i < 100; i++ {
		item := map[string]interface{}{}
		item["Title"] = fmt.Sprintf("title_%d", i)
		item["CreatedTime"] = time.Now()
		item["Description"] = fmt.Sprintf("description_%d", i)
		item["Tags"] = []string{"aff", "5fdf"}
		item["Author"] = fmt.Sprintf("author_%d", i)
		item["MusicId"] = fmt.Sprintf("music_id_%d", i)
		item["Category"] = fmt.Sprintf("category_%d", i)
		result = append(result, item)
	}

	pageData := pagination.Pagination(result, 22, 5)
	// fmt.Printf("%#v\n", pageData)

	jsonData, _ := json.Marshal(pageData)
	fmt.Println(string(jsonData))
}
