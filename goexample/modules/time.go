package modules

import (
	"errors"
	"fmt"
	"goexample/schema"
	"time"
)

func NewTimes() schema.Worker {
	return &Times{
		option: &schema.Option{Action: map[string]schema.FuncHandle{
			"time1": time1,
		}},
	}
}

type Times struct {
	option *schema.Option
}

func (c *Times) Intro(category string) string {
	var tip string

	switch category {
	case "label":
		tip = "Times 样例"
	}

	return tip
}

func (c *Times) Options() *schema.Option {
	return c.option
}

func (c *Times) Do(action string) (map[string]schema.FuncHandle, error) {
	fu, ok := c.option.Action[action]
	if !ok {
		return c.option.Action, errors.New("找不到指定方法 [ " + action + " ]")
	}
	fu()

	return nil, nil
}

func time1() {
	// 分别使用 `time.Now` 的 `Unix` 和 `UnixNano`，
	// 来获取从 Unix 纪元起，到现在经过的秒数和纳秒数。
	/* now := time.Now()
	secs := now.Unix()
	nanos := now.UnixNano()

	// 注意 `UnixMillis` 是不存在的，所以要得到毫秒数的话，
	// 你需要手动的从纳秒转化一下。
	millis := nanos / 1000000
	fmt.Println(secs)
	fmt.Println(nanos)
	fmt.Println(millis)
	fmt.Println(now.Format("2006-01-02 15:04:05"))

	// 你也可以将 Unix 纪元起的整数秒或者纳秒转化到相应的时间。
	fmt.Println(time.Unix(secs, 0))
	fmt.Println(time.Unix(0, nanos)) */

	// 定时器表示在未来某一时刻的独立事件。
	// 你告诉定时器需要等待的时间，然后它将提供一个用于通知的通道。
	// 这里的定时器将等待 2 秒。
	timer1 := time.NewTimer(2 * time.Second)

	// `<-timer1.C` 会一直阻塞，
	// 直到定时器的通道 `C` 明确的发送了定时器失效的值。
	<-timer1.C
	fmt.Println("Timer 1 fired")

	// 如果你需要的仅仅是单纯的等待，使用 `time.Sleep` 就够了。
	// 使用定时器的原因之一就是，你可以在定时器触发之前将其取消。
	// 例如这样。
	timer2 := time.NewTimer(time.Second)
	go func() {
		<-timer2.C
		fmt.Println("Timer 2 fired")
	}()
	stop2 := timer2.Stop()
	if stop2 {
		fmt.Println("Timer 2 stopped")
	}

	// 给 `timer2` 足够的时间来触发它，以证明它实际上已经停止了。
	time.Sleep(2 * time.Second)
}
