package main

import (
	"flag"
	"fmt"
	"goexample/modules"
	"goexample/modules/channel"
	"goexample/modules/database/elastic"
	"goexample/modules/database/gredis"
	"goexample/modules/database/mongo"
	"goexample/modules/database/msql"
	"goexample/modules/generics"
	rpcclient "goexample/modules/rpc/client"
	rpcjson "goexample/modules/rpc/jsonrpc"
	rpcserver "goexample/modules/rpc/server"
	web1 "goexample/modules/web"
	"goexample/schema"
	"os"
	"time"
)

func main() {
	// 命令行 布尔类型的参数必须使用等号的方式指定
	var worker = flag.String("w", "", "指定要执行的模块名")
	var action = flag.String("a", "", "指定运行模块的方法名")
	var showList = flag.Bool("l", false, "显示支持的数据抓取模块并退出")

	var ge = &schema.Ge{
		Started: time.Now().Unix(),
		Providers: map[string]schema.WorkerHandle{
			"demo":       modules.NewDEMO,
			"array":      modules.NewArrays,
			"json":       modules.NewJSON,
			"channel":    channel.NewChannel,
			"sha1hashes": modules.NewSha1hashes,
			"struct":     modules.NewStructs,
			"file":       modules.NewFiles,
			"time":       modules.NewTimes,
			"regexp":     modules.NewRegexps,
			"goroutine":  modules.NewGoroutine,
			"telnet":     modules.NewTelnet,
			"rpcclient":  rpcclient.NewRpcClient,
			"rpcserver":  rpcserver.NewRpcServer,
			"rpcjson":    rpcjson.NewRpcJson,
			"mysql":      msql.NewMsql,
			"redis":      gredis.NewGredis,
			"mongo":      mongo.NewMongo,
			"elastic":    elastic.NewElastic,
			"web":        web1.NewWeb,
			"generics":   generics.NewGenerics,
		},
	}

	flag.Parse()

	if *showList {
		fmt.Fprintln(os.Stderr, "模块列表:")

		ge.Help()

		return
	}

	if "" == *worker {
		flag.Usage()
	} else {
		ge.Cli(*worker, *action)
	}

}
